# 10. Open Source Hardware

## References

### Wool Testlab, Raw Color

[Goldberd machines](https://www.rawcolor.nl/project/?id=430&type=ownProduction) from Raw Color around wool.

::: vimeo 86242837

### Crafting Fashion with Robots

Crochet and basket weaving are the two textile technique that don’t have an automated mechanical device. This project uses a robotic arm to automatise basket weaving.

::: youtube ydU9XTSRfd8



### Pleating loom

améliorer métier à plisser de Dewi → dégradé gravure et/ou CNC

[Dewi Brunet](https://dewiorigami.com/) is an origami artist, he folds paper but also pleats textile. For his practice, he developed an open source [pleating loom](https://github.com/DewiBrunet/Plissage/blob/master/Pleating%20loom.md) for embossing paper. This loom is a super interesting tool, but for the moment, it is designed only as plain lines. The engraving is done by lowering the power of the lasercutter. Therefore the grooves are really fine and the embossing tool has a hard time to go in. I wanted to make modifications this week to make the plain lines as gradient lines to be engraved as a valley and be able to have thicker engraved lines. But I didn't have the time to do that…

![](https://user-images.githubusercontent.com/25649502/208509984-26848474-48e2-4549-8ccb-729eaed76837.JPG "Dewi Brunet's pleating loom.")

![](https://user-images.githubusercontent.com/25649502/208255089-ca2f8ed4-26e3-49fa-9319-2cabe27e912c.JPG "Dewi Brunet's pleating loom variations.")



### Hilo spinning machine

In the frame of the [SheMakes](https://shemakes.eu) project, we constructed at Green Fabric the Hilo spinning machine. It was quite challenging to build, but we finally managed to complete it. We figured that we need a drafting device to prepare a fiber top with a constant thickness, otherwise we would get only very random thicknesses. It worked much better with strips of paper which had all the same width as they were cut with a paper shredder.

PICTURES HILO


### Live Loom, Alex Mc Lean

In 2020–21, I tried to build Alex Mc Lean's Live Loom. It is an open source machine to make jacquard weaving with patterns sent by a computer. Although Alex Mc Lean helped me with debugging it, the solenoids were not selecting the right threads. Meanwhile I had found another way to make jacquard weaving in my lab (see next section).

![](https://lh6.googleusercontent.com/i0hnX9M0766WrgRuAUEXs0Xr2xiW1Kk_9g0le9KvAQTXlp1d1F1tKlUThQglwwwz2Sqv55HNEKUN4jxOIGWydsUxWFE0u7_vxLYYaCkOZEawQ_iSLnEy7BwGa31cKLtGdt1Tb1WagiQ "Alex Mc Lean's live loom.")

![](https://gitlab.com/svilayphiou/knit-tink/-/raw/master/doc/liveloom/20220528_123115.jpg "Live loom building attempt. Due to a software issue, I've never succeeded in connecting it to my computer.")

## Previous work

### knit-tink, a DIY jacquard loom on a knitting machine



In 2023, I worked on a way to have a jacquard loom in a lab. After failing with the Live Loom, I just used my hacked knitting machine to use its patterning function, and transform it into a warp-weighted weaving loom. [The research is fully documented here.](https://gitlab.com/svilayphiou/knit-tink)

![](https://gitlab.com/svilayphiou/knit-tink/-/raw/master/doc/tissage%20sur%20machine%20%C3%A0%20tricoter/20220902_152421.jpg)

![](https://gitlab.com/svilayphiou/knit-tink/-/raw/master/doc/tissage%20sur%20machine%20%C3%A0%20tricoter/20220914_171233.jpg)

::: instagram Cifjg4wDGHy



### DIY speedweve

Repairing clothes is one of my favourite activity: it's useful and it repairs my soul at the same time… The Speedweve looms are really interesting tools to repair quickly with a weaving structure. I've never had one myself and I wanted to make one, rather than buying one, to have a very fine warp density as I love repairing with very fine threads.


I had bought the [Lemon loom instructions](https://lemonloom.com/products/make-your-own-darning-loom-end-2-end-hook-and-assembly-instructions-w-video) but I found quite difficult to make the hooks. Miranda from [Little Lemon Loom](https://www.instagram.com/littlelemonloom/) showed her process once in an Instagram story, but it's not available anymore. She has a wooden board with nails to be able to bend her hooks always the same way. It's indeed very difficult to do without that. 

Although I find very elegant and comfy that the loom is made of fabric, I found it difficult to sew precise intervals and that everything stays right in place. 

![](https://lemonloom.com/cdn/shop/products/image_216a1ac4-b74e-46df-8898-429309566d52_1200x1200.jpg?v=1645936443 "Lemon loom, a modern Speedweve-like loom")

In conclusion, it's a good idea to buy it directly from the e-shop, or to make a digital fabricated loom.

I made a structure in 3mm plywood with the lasercutter, and 3D printed hooks. The wooden layers have ridges to help the hooks to stay aligned.



::: instagram CoX0llDvJiO



What I missed so far is the darning disc to hold the fabric with the loom. I found this [3D file for the disc](https://www.printables.com/cs/model/344366-speedweve-darning-disc/files). I’d need to adapt the loom to fit the disc.

![](https://media.printables.com/media/prints/344366/images/2934415_623eb6d0-839f-4993-80fa-50a1df0f9c88/thumbs/inside/1280x960/jpg/large_display_img_1236-1_344366.webp "3D printed disc to stretch the fabric.")

In the meantime, I also stumbled upon this 3D printed speedweve from [That Darn Machine](https://www.instagram.com/thatdarnmachine/), but I find the hooks too thick to my liking, and the project seems down anyways:

::: instagram CBB_aCYpfC4

And there’s this one from Dye Mad Yarns, but it’s also a bit too thick to my liking:

![](https://dyemadyarns.com/cdn/shop/products/20220502_105029_1280x.jpg?v=1651505457 "Speedweve-like loom from Dye Mad Yarns")







## Circular Sock Machine (CSM)

You probably know by now that I am obsessed with knitting. I’ve been wanting to build a CSM for a while, now is the perfect opportunity! Although you can knit socks on a flatbed domestic machine, it's not as smooth as a circular one. I make a pair of socks in 2 hours on a flatbed machine; you could make one pair in 40 minutes with a vintage metallic CSM.



### Existing projects

There are [many CSM](https://www.stlfinder.com/3dmodels/circular-knitting-machine/) on the Internet, it was a bit difficult to choose one project from another…

#### Circular Knitic, 2014
A project by artists Varvara Guljajeva & Mar Canet. It follows their project of a flatbed open-source knitting machine. This project is fully automated with a motor. I  wanted to have a machine which you could manipulate with your hand to have more control on what can be done.
Source files: <https://www.instructables.com/Circular-Knitic/>

![](https://content.instructables.com/FB2/P4H0/IAHPANVO/FB2P4H0IAHPANVO.jpg?auto=webp&frame=1&width=350&height=1024&fit=bounds&md=4e853a151fa6ae8e800743149a5d698f "Circular Knitic by Varvara Guljajeva & Mar Canet")


#### Idda, 2014/2016
It is a very big gauge, but the files were made with openSCAD, so all the parameters should be "easy" to change. The big advantage of this project is that the knitting needles are also 3D printed. They are based on this [1976 patent](https://patents.google.com/patent/US3983719). I've never seen needles without latches before. It looks super interesting as we don't know for how long metallic knitting needles will be produced…  

Source files: https://github.com/jonnor/idda-knitting-machine

![](https://github.com/jonnor/idda-knitting-machine/raw/master/doc/automated-machine-mod-small.png)




#### CSM plus ribber
This project is replicating a vintage metallic CSM (Legare, Auto Knitter, etc.). Later on, the author added a ribber attachment. I haven't chose this project because it is using the knitting needles from CSM which are more difficult to find, therefore more expensive. But the ribbing attachment is super interesting to have…

Source files: 

- <https://www.thingiverse.com/thing:4695746> 
- <https://www.thingiverse.com/thing:4786810>





### CSM 2023 from RKstar42
There are many Circular Sock Machine projects on the Internet. I’ve always been sceptical about 3D printed machines as I imagined they would not be very smooth to activate. But after visiting [Fablab Zurich](https://zurich.fablab.ch/home/digitale-werkstatt/), I was quite convinced. I haven’t tried the machine with yarn, but I was convinced enough by the fluidity of the plastic mechanisms.



![](../images/week10/20230408_154055.jpg "CSM from Fablab Zurich. They have an electronic row counter.")



![](../images/week10/20230408_154122.jpg "CSM from Fablab Zurich.")

![](../images/week10/20230408_154811.jpg "It looks like they have a second one, and this one seems to have a ribber! I noticed that only now…")

![](../images/week10/20230408_154845.jpg "My first encoutner with a caston bonnet. A device you need to build to start a knit on a CSM (see «How to use a CSM» Section).")

![](../images/week10/20230408_154849.jpg "Cast-on bonnet close-up.")

![](../images/week10/20230408_153341.jpg "Extra: they seem to have a DIY(?) electric bobbin winder. Interesting!")

![](../images/week10/20230408_153003.jpg "Extra: they highly customized their hacked knitting machine: a case to avoid dust, a wifi connection with a Raspberry Pi, a better beeper, and they managed to keep/replace the row counter. Super inspiring.")

### RKStar42's CSM

But I haven’t been able to find the plans of their specific machine. And they didn’t answer when I asked for it… So I found this quite new project from RKStar42 on Thingiverse. What pleased me at first sight was that the author made some specific modifications compared to what was existing so far (we'll talk about that in the conclusion):

> «There are 2 push-up cams, leaving the needles normally in the up position. They can be pushed down to do maintenance; eg. to pick up dropped stitches. This eliminates having to use mechanical "flippers" to push the needles up. (Usually the needles are pushed up (to clear the needle latches) just before the stitch is made. Here the needles are also pushed up right after the stitch is made. This makes no difference for the knitting, it just looks differently)»

Source files:

- <https://www.thingiverse.com/thing:6273014>
- More explanations from the author: <https://www.knittingparadise.com/threads/new-diy-circular-knitting-machine-on-thingiverse.764261/>

When you download the source files, there are more details on the spare parts to prepare and on the process of mounting than what you can read on Thingiverse. Nonetheless the assembly steps are not really described and there are not enough pictures to see the machine fully (no overview, no view from the back) which makes it a bit difficult to understand how to mount the machine. The author is quite available if you ask questions on Thingiverse, but it seems unlikely that he would add more pictures or a video of the machine running.
We (with my Fabricademy fellows [Charlotte](https://class.textile-academy.org/2024/charlotte-jacob/assignments/week10/) and [Eileen](https://class.textile-academy.org/2024/eileen-schreyers/assignments/week10/)) hope that our documentations will help others to build the machine.



#### Materials

| Qty |  Description    |  Price  |           Shop           |
|-----|-----------------|---------|--------------------------|
| 64 | KR830-KR850 machine knitting needles. | 40ct/needle | https://stecker.be |
| 1   | 114x3mm o-ring ( or a thick rubber bands) | 0 | in stock |
| 1   | 760x6mm 2GT timing belt. (760mm is the minimum length) | ? |    |
| 1 | 30cm aluminium tubing (6mm) for the yarn brake/heel spring stand. → we used a 5.5mm knitting needle as the holes came out too small |  | in stock |
|  | about 1m piano wire (between 0.8/1mm)**→ we used 0.7mm as the holes came out too small** | given | neighbour |
| 106 | 4.5mm steel Ball Bearings (cal .177) (52 + 36 + 18 for each part) |  |  |
| 1 | 10cm smooth rod (3mm) (drill rod, styrene, etc) for yarn brake pivots **→ we used a 2nd hand 2.5mm knitting needle as the holes came out too small** |  | deadstock |
| 2 | 1.5mm nails or pins to secure yarn brake stand |  | in stock |
| 1 | one ballpoint style spring to push the center cam up. |  | sacrifice a bic pen |
| 1 | 8mm x 2mm screw hook to hang the heel spring weight |  | la Pièce Manquante |
| 10 | M3x10mm flathead |  | la Pièce Manquante |
| 6 | M3x16mm cylinder head |  | la Pièce Manquante |
| 1 | M3x30mm |  | in stock |
| 4 | M3 nuts |  | in stock |
| 1 | M4x25mm + washer and 5mm bushing |  | in stock |

 

![](../images/week10/20231117_183041.jpg "Piano chords given by a neighbour of Green Fabric who used to be an organ manfacturer. <3")



#### 3D models



#### 3D printing


All the parts can be 3D-printed without any supports, the 3D models are already including bridges that you sometimes need to break after the printing. This is really time saving.

This CSM version is a bit different from existing 3D printed CSM:

- it uses a timing belt and 106 ball bearings instead of 3D printed gears and less than 20 ball bearings: this allows less friction
- the cam leaves the needles in an upper position. According to the original maker, it is easier for stitch maintenance (on a flatbed knitting machine, this would be equivalent as the needles going back to D position)

??? info "3D printing settings for most parts"
    - if you're using metrics system, print the regular files and not the UNC versions
    - **NO SUPPORTS NEEDED**: The 3D models are already designed with the mandatory supports which you need to remove afterwards. It is much appreciated!
    - 0.4mm nozzle
    - 0.2mm layer height (Quality preset from Prusa Slicer)
    - top layers: 3
    - bottom layers: 3
    - infill: Adaptative Cubic 15%
    - Combine infill every: 2 layers (reduces printing time, especially for tall parts)

??? info "3D printing settings for «main cylinder» and «main base»"
    - These parts are very long to print, the author suggests to increase the layer height to reduce printing time.
    - 0.3mm layer height
    - Combine infill every: 2 layers (reduces printing time)

![](../images/week10/20231128_145717.jpg "Cylinders.")

![](../images/week10/20231128_145737.jpg "The 3rd cylinder was a bit too fragile.")

![](../images/week10/20231127_113501.jpg "We printed it again with thicker walls. Here you can also see that some parts need to be removed after printing. The mandatory supports are already in the 3D model.")

On this video, we can see how the push cam (black) can move up/downwards to be able to change stitch size:

<iframe title="push cam-mute" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/d73945c8-1cc0-431e-b414-1b09450c54e4" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

![](../images/week10/20231125_203116.jpg "The hardest part to print: because of its printing time, we had to leave it print overnight. But at Green Fabric, it's too cold during the night. I had to bring the 3D printer to my house to print it and have a look at it.")

![](../images/week10/20231126_102109.jpg "The next morning, I was happy to find a well-printed part!")

![](../images/week10/20231126_105434.jpg "The different parts are quite nicely design as the models already integrate supports that you need to break apart, like here. We used a large rubber band from our favourite sandwich place in Brussels to hold the needles, instead of an o-ring spring.")







#### Assembly

See [Charlotte’s documentation](https://class.textile-academy.org/2024/charlotte-jacob/assignments/week10/) for an extensive viewing of where each part is going where. 

!!! "Main process"
    - Cut and sand the needles (they should look like an "L")
    - Sand the 3D printed components
    - Assemble each main part
    - Assemble all the main parts together
    - Make a 10cm hole in a wooden board
    - Screw the machine on the board

![](../images/week10/20231127_105219.jpg "Eileen is cutting the knitting needles with a dremel.")

Adding needles is super easy:

<iframe title="adding-needle-mute" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/1ad3770c-fcad-4107-ab82-da9287267b60" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>


![](../images/week10/20231124_165821.jpg "Screw the main part on a board.")

![](../images/week10/20231124_170042.jpg " Screwing the crank.")

![](../images/week10/20231124_172713.jpg " To screw the crank, we put the timing belt and put the crank as far as could. Therefore, the exact length of the timing belt is not very important.")



First test of the circular knitting machine after assembly of the base parts: 

<iframe title="turning crank" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/5ec8b7a7-4625-4641-b712-5a19305215d2" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>



![](../images/week10/20231128_144808.jpg "The hole in the wooden board is necessary for the knit to come down.")





![](../images/week10/20231123_142325.jpg "**Yarn feeder**
The part "guide_top.stl" is quite fragile and broke, so it would be a good idea in a future to make the curved part thicker.")

![](../images/week10/20231123_154539.jpg "**Piano chord holes** The holes were a bit clogged by the 3D printing blunders. We had to open the holes again by heating needles.")

##### Specific assembly parts

**Bearing balls**
This step was the most cumbersome. The assembly notes recommend to put all the bearings between the bearing shells, to spread them around and then to add the bearing cages with the holes pointing down. But this was a nightmare to do and we actually never succeeded this way. Instead, we put the ballcage between the bearing shells first, with the holes pointing up, and then inserted the bearings in each hole. 

![](../images/week10/20231123_153218.jpg "Big tip: use a closed workspace so that the bearings don't fall on the floor. They tend to escape easily. This box top was doing the job quite fine.")

![](../images/week10/20231124_141618.jpg "Putting all ball bearings in and then inserting the ballcage. IT DOES NOT WORK.")

![](../images/week10/20231124_141816.jpg "INSTEAD DO THIS: Putting the ballcage first and then inserting the ball bearings in.")







#### How to use a CSM





![](../images/week10/20231127_151748.jpg "Assembled machine!.")



Now that the machine is ready, we need to learn how to use it. This CSM seller made a lot of video tutorials to learn how to use such a machine. Even if it's not the same machine, the principle remains the same:

- [Dean and Bean](https://www.deanandbean.com/basic-csm-skills)
- [Bear Valley Fibres](https://www.instagram.com/bearvalleyfibres/)
- [4 ways to knit heels](https://www.youtube.com/watch?v=5b3gTIcstq8), this channel has a lot of pretty advanced CSM videos

##### Cast on

- [Cast on the first time without a setup bonnet](https://www.deanandbean.com/casting-on-without-a-setup-bonnet)
- [Cast on the first time with an existing knitted tube](https://yewtu.be/watch?v=Co2-TNcprBw)
- [Cast on with a setup bonnet](https://www.deanandbean.com/casting-on-with-a-setup-bonnet)

* * *

I decided to make the second version of these cast on, by making a temporary setup bonnet with my flatbed knitting machine. I knitted a tube, and at the base of the tube, I put a string through to tighten it up.

![](../images/week10/20231127_170128.jpg "A temporary setup bonnet made with a flatbed knitting machine.")

![](../images/week10/20231127_170132.jpg "Using a flatbed knitting machine weight to hold the stitches down.")



![](../images/week10/20231127_153336.jpg "Finding the good system for weight is difficult. It has to be the right amount but also to spread out in the circumference of the machine…")

First test of knitting. The yarn feeder is too close to the needles. I had to hold it with my hand so that it doesn't collide with the needles head. 

<iframe title="knitting" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/0e7dd597-5d0d-4af8-bc18-2ef49e18efa9" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

* * *

We re-printed the «guide_base.stl» and it seems to fix the yarn feeder getting too close to the needle heads. 

<iframe title="CSM knitting smooth" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/369405c8-405c-4612-9d46-1fbbbfa2f46e" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>



![](../images/week10/20240429_152736.jpg "Yarn tension tests. You can unscrew the two black pulleys to lift up/down the black pushcam: cam up makes smaller stitches, cam down makes bigger stitches. The screw on top of the cam is supposed to help lifting up/down the cam but it does not seem to do anything. Maybe the push cam got damaged at this spot…")