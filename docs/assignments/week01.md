
# 1. State of the art & documentation

## Research & Ideation

I have not a precise idea on what to do during the Fabricademy course, I'm mostly interested in discovering new techniques which will be triggers to ideas to develop. I do plan though for the open source hardware week to build a circular knitting machine.


## References & Inspiration

### Basket Club

![Basket Club](https://basketclub.world/site/assets/files/1389/snapbasket-gif.gif "✉️ Brief 6 - Nicolas Baker")


![Basket Club](https://basketclub.world/site/assets/files/1252/simone_post-brief_9_money_4.jpg "Brief 9 - Simone Post")

![Basket Club](https://basketclub.world/site/assets/files/1598/earnest_studio_candle_3.jpg "Brief 24 - Earnest Studio")



### Unstable Design Lab

<iframe width="560" height="315" src="https://www.youtube.com/embed/LZy9ME3Tr54?si=DY9gaCXeZpTk0c0S" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/8YxW3YQudqQ?si=bEIX_R8fvX6gBt24" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


### Soft Connection Lab

<video  src="https://softconnectionlab.be/braiding.mp4" controls></video>



---


## Documentation workflow

Being a former graphic designer, what I like to change first are colors and fonts. It was not that obvious to have a custom font which is not from Google. There is a strange thing happening with paths… The fonts worked locally on my computer but not online… So I started a custom theme as documented [here](https://www.mkdocs.org/dev-guide/themes/).
I “cheated” a little bit… I started from an existing theme that my partner is working on. I then started to play modifying it and discovering CSS new features like `repeating-conic-gradient`. All html and css files are available in the `vvvvv` folder of [my gitlab folder](https://gitlab.fabcloud.org/academany/fabricademy/2024/students/stephanie-vilayphiou).

![forked theme](../images/week01/forked-theme.png "forked theme from Alexandre Leray"){ width=400 align=left}

![what it became after one day](../images/week01/customized.png "what it became after one day"){ width=400 align=left}


### Fonts

- [DINdong](https://typotheque.genderfluid.space/DINdong.html) from the super talented [Clara Sambot](https://www.instagram.com/clara_sambot). If you're French speaking, you can use this font to write in inclusive like: “les enseignant·es et les étudiant·es s’échangent leur savoir-faire”.
- Cutive Mono from Google Fonts


### Background patterns

I discovered this very cool CSS for repeating backgrounds: `repeating-conic-gradient`. You can find documentation about it and try it out on [Mozilla's resource website](https://developer.mozilla.org/en-US/docs/Web/CSS/gradient/repeating-conic-gradient).

On the [`repeating-linear-gradient` documentation page](https://developer.mozilla.org/en-US/docs/Web/CSS/gradient/repeating-linear-gradient), you can see how to repeat your pattern on a surface.

I wanted to have changing patterns when changing pages without being too heavy on the browser side. So I used the `random` template filter of the Jinja language that mkdocs uses. Here's what the code looks like (it has to be put in an `html` page as it uses a syntax specific to Jinja:

    <style type="text/css" media="all">
        body {
            background: repeating-conic-gradient(#DDD {{[71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82]|random}}deg 23%, white 0deg 25%);
            background-size: 40px 40px;
        }
        
    </style>

### Lightbox

I saw on mkdocs website [here](https://squidfunk.github.io/mkdocs-material/reference/images/#lightbox) that you can easily add a lightbox plugin (when you click on an image, it enlarges it on top of the whole website).


### figure & ficaption

I used the Markdown plugin [yafg](https://pypi.org/project/yafg/) to easily write `<figure>` and `<figvaption>`.

The markdown syntax for an image is as usual:

    ![Alternative text for accessibility](/path/to/image.png "This is the title/caption of the image.")

becomes this html code:

    <figure>
    <img alt="Alt text" src="/path/to/image.png" title="This is the title of the image." />
    <figcaption>This is the title of the image.</figcaption>
    </figure>
