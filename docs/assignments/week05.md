# 5. E-textiles

## References

### Gloves

[Irene Posch](https://ireneposch.net/early-winter-night-biking-gloves/)'s Early Winter Night Biking Gloves. I find those very useful! [Instructions here](https://ireneposch.net/diy-biking-glowes/)

![](https://ireneposch.net/wp-content/uploads/2016/07/ezgif.com-gif-maker.gif)


[Glow Glove](https://www.ylxdesign.com/my-work/itaa-glow-glove) is a glove made for recovery periods for people who got strokes.o

::: youtube 6eNgMB4g13Y



### Claire Williams


![](../images/week05/claire-williams-dsc_0208.jpg "Embroidered speaker.")

![](../images/week05/claire-williams-electro_mag.png "Electromagnetic walks. Knitted antennas to listen to electromagnetic fields in the urban environment.")

![](../images/week05/claire-williams-chants-magnetiques.jpg "Chants magnétiques.")



### Moon

![](../images/week05/moon.png "From *Knitting Technology. A comprehensive handbook and practical guide* by David J. Spencer. Warp-knitted fabric antenna on the moon (Photo credit NASA)")


### e-textile summer camp samplers

### Unstable Design Lab

Textile Animation

::: vimeo 244711586

* * *

[Magnetic Reverberations](https://github.com/UnstableDesign/Flappable)

* * *

::: instagram CxY7Z1svGjf


* * *

### Embroidered computer


In the 40 computer memories were made of intricate interweaving of copper wires. Women were the ones making them as they had the dexterity skills from sewing or other textile technique. Magaret Hamilton was the boss of the "rope mothers". The article ["That Time When Computer Memory Was Handwoven by Women"](https://www.amusingplanet.com/2020/02/that-time-when-computer-memory-was.html) retraces this history. My mother, who got employed at Hewlett Packard in the 80s to repair electronic cards, was hired because she was a seamstress even though she knew nothing about electronics.


![](https://3.bp.blogspot.com/-in6IscIcWO0/XjlD3NUj7CI/AAAAAAAAkoU/pS8FTfRA1hg3wPMhUUVj4fiQVYm_vIDHwCLcBGAsYHQ/s1600/core-memory-weaving-14.jpg "A core memory module with a capacity of 128 bytes. Photo: Konstantin Lanzet/Wikimedia Commons")

![](https://3.bp.blogspot.com/-TXwXjEYEhXo/XjlD3RvkYkI/AAAAAAAAkoY/4bvvS0X0gPU22-HyA_K6TIlA_lQ73A2bwCLcBGAsYHQ/s1600/core-memory-weaving-15.jpg "An 8-GB microSD card on top of 8-Bytes of magnetic-core memory. Photo: Daniel Sancho/Wikimedia Commons")

In 2016, e-textile artist [Irene Posch](https://ireneposch.net/the-embroidered-computer/) made a very beautiful version of a computer memory with embroidery and pearl-shaped magnets.

![](https://ireneposch.net/wp-content/uploads/2018/05/DSC_4931_grayer_w-1024x684.jpg "Irene Posch's Embroidered Computer.")


### Soft/Hard connections

I like the DIY battery holder in this sample:

![](https://media2.giphy.com/media/v1.Y2lkPTc5MGI3NjExMjhqb2Z0bmllYTVqd2VtaTlhNGYxNG43ZnRtZmw2d2xtaHhmcDN1NCZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/jj5Hcb3MXZrQV6SdeA/giphy.gif "Alice Sowa, Fabricademy 2023")


### Paper sensors

I really liked the paper sensors prototypes that [Ieva Marija Dautartaitė](https://class.textile-academy.org/2023/ieva-dautartaite/assignments/week05/) made for Fabricademy 2023.

::: vimeo 802614054


It reminded me of the Arduino paper piano project that I once made from [Instructables](https://www.instructables.com/Ultimate-Arduino-Paper-Piano/).

![](https://content.instructables.com/FF7/MIE9/JUGT8I0J/FF7MIE9JUGT8I0J.jpg?auto=webp&frame=1&width=933&height=1024&fit=bounds&md=c32c4cdf9534d31fa2dd7de1d1a589a6)



### Tooling

[Irene Posch's tooling](https://ireneposch.net/tooling/) page lists quite some nice tools to build yourself for e-textile.



## e-textile breadboards

### References

#### Pattern dot matrix

The [Pattern dot matrix](https://penelope.hypotheses.org/2232) is a tangible interface to program weaving patterns. It has been developed by Dave Griffiths within the Penelope research project.


![](https://penelope.hypotheses.org/files/2020/08/1.jpg "Prior version in wood.")

![](https://penelope.hypotheses.org/files/2020/08/21.jpg "Pattern dot matrix with conductive thread.")

In 2021, I got a subsidy to explore textile and code. I wanted to reproduce the textile version of the Pattern Dot Matrix, or make a textile breadboard. I've never had the time to do so. But now is the time! :)


#### [Feltbread](https://www.instructables.com/fabric-breadboard/)

![](https://img.youtube.com/vi/ux7pKWvxWXY/hqdefault.jpg)


#### [Snap buttons ](https://www.instructables.com/Shirt-Circuit-DIY-Wearable-Breadboard-Circuits/)

![](https://content.instructables.com/FGV/T4HW/ILCHFM6W/FGVT4HWILCHFM6W.jpg?auto=webp&frame=1&fit=bounds&md=fe01e5f8a91a936efed0f46e7a6cd216)











### Knitted breadboard

#### e-textile pin probes

Two weeks ago, I helped my artist friend [Wendy Van Wynsberghe](https://www.wvanw.space/) to build soft pin probes from [Irene Posch's tutorial](https://ireneposch.net/pinprobe-diy).Wendy's paracords were quite thick so we had to modify the 3D printing file to make the holes bigger. Also there was an error in the file, the cap was not mirrored so the holes were not in front of each other when you'd close the case. 

![](../images/week05/pinprobes-wendy.jpg "Wendy Van Wynsberghe's pin probes.")



![](../images/week05/pinprobe1.jpg "Conductive thread in machine knitted i-cord + sewing pin")

![](../images/week05/pinprobe2.jpg "Putting it into the pin probe.")

![](../images/week05/pinprobe3.jpg "")

[`.stl` file for DIY pin probe, remixed from Irene Posch's](https://www.thingiverse.com/thing:6279504).


<iframe title="Making an i-cord on a knitting machine" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/c7ac6df9-a4a1-4ef9-b547-197478b52083" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

![](../images/week05/pins.jpg "Sewing pins with silver conductive thread as a pin head to make contact with through-hole components.")


#### Prototypes on a failed knitted sock

A few weeks ago, I had already tried the different conductive threads/yarns that I had to check if a textile breadboard would work out. Only my conductive yarn from [Bart & Francis](https://bart-francis.be/nl/etextile-high-tech-garen-co/g-128) didn't work for the breadboard as it is not conductive enough.

![](../images/week05/sock1.jpg "e-hajo Silver thread.")

![](../images/week05/sock2.jpg "Adafruit stainless steel thread.")

![](../images/week05/golden-thread-greenfabric.jpg "a metallic thread found by chance in the second-hand notions shop of Green Fabric.")



Here is my unfinished breadboard. It works fine, I just need more time to chain-stitch all the lines!

![](../images/week05/textile-breadboard.jpg "Machine knitted full-needle rib in second-hand cotton + crochet chain stitches with silver thread.")












## Knitted sensors

I wanted to experiment the different conductive threads/yarns with my knitting machine. I could even explore techniques that I've never tried before: loops on single-bed, ottoman stitches, short rows ripples. Since those were my first attempts with these techniques, and with quite capricious threads, they were not very successful.

![](../images/week05/knitting-machine-dowel.jpg "Using a dowel to make big loops.")

![](../images/week05/analog-sensors.jpg "Different knitted swatches with conductive threads.")


### Digital sensor: push button



![](../images/week05/pushbutton-prototype.gif "Push button prototype with bought knitted conductive fabric.")


![](../images/week05/pushbutton-inside.jpg "Inside of the push button in felt. Holes made with hole puncher.")


![](../images/week05/pushbutton-knit.gif "Machine knitted conductive thread with the plating feeder.")


![](../images/week05/wearable-led.gif "Same circuit with wearable LED.")





### Analog

When Liza Stark said that conductivity can be increased by proximity or by adding more material, I immediately thought of a good purpose for my Bart & Francis not-so-conductive yarn. I thought that it would work well for stroke switches.



#### Folds

I wanted to make a stroke sensor with fringes, but in the end, I ended up with a pressure sensor. 



<iframe title="E-textile pressure sensor" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/cbade97a-0a9f-4624-b38f-258363b1f7cf" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>



#### Stretch

Stretch sensor made with a simple crochet chain with the not-so-conductive yarn from Bart & Francis.
I wanted to put the chain on a glove above my finger to activate an LED but I didn't have time.

<iframe title="E-textile stretch sensor" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/97abd2dc-aaf8-423b-9b6f-fd49b25e3054" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>



#### Blow


I wanted to make a blow sensor by putting a lot of tiny conductive threads that could be blown onto an LED to make contact.

First try with the LED on a breadboard:

<iframe title="E-textile blow sensor" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/e2de47a7-319c-44ab-bc8c-150162f50d01" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

Second try with the LED on a paracord:

<iframe title="e-textile blow sensor" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/a2e4a84f-b946-48a9-a2fd-7bd873e30eea" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>



??? Update
    - After reflection, I should have looked at the [Stroke sensor](https://www.kobakant.at/DIY/?p=792) on Kobakant to design the blow sensor to make it more stable.
    - ::: youtube NQLivM3RDd4

## Small project

I wanted to use the pressure sensor to control a motor with a fan on it; the fan would then blow the blow sensor to control an LED. Each part functions by itself, but I didn't have time to combine all experiments.


### Tinkercad

[Tinkercad](https://www.tinkercad.com/) is an online CAD tool mostly aimed for kids. It's well-known for simple 3D modelling (I use it all the time!) but it's circuit design tool is less known. 

![](../images/week05/tinkercad-new.png "Create a new circuit design.")

![](../images/week05/tinkercad-lemon.png "You can import components, even a lemon battery!")

![](../images/week05/tinkercad-schematic.png "My circuit schematics.")



### Arduino code

Here's the pressure sensor on the textile breadboard connected to Arduino to get the analog values when we press the knitted swatch. The video shows the analog pin on the LED, it should be placed on the knitted sensor instead. When I press the sensor, the value decreases but the LED goes brighter. I would have expected that the values would get higher actually as it is supposed to read Voltage values according to the [AnalogRead documentation](https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/).

<iframe title="Pressure analog sensor" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/a1bb44be-0784-411b-855e-27de9576729e" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>


!!! Update
    During Week 8, on wearable electronics, I took some time to refine my Arduino circuit to get an analog ouput from my sensor then controlling the LED with Arduino.
    

    ![](../images/week05/circuit-diagram.png)
    
    ![](../images/week05/sensor-input-output.jpg)

```
int ledPin = A0;
int sensorPin = A5;
int sensorValue = 0;
int newSensorValue = 0;

 
void setup() {
  Serial.begin(9600);
  pinMode(sensorPin, INPUT);  
  pinMode(ledPin, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
    sensorValue = analogRead(sensorPin);
    newSensorValue = map(sensorValue, 0, 1023, 0, 255);
    Serial.println(newSensorValue);
    analogWrite(ledPin, newSensorValue);
}
```



