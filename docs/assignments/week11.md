# 11. Textile Scaffold

## References

### Twist again, Dominique Vial
[Dominique Vial](https://class.textile-academy.org/2021/dominique.vial/MY%20PROJECT/MY%20PREZ/) (Fabricademy 2020-21) made a wonderful project on recycling jeans and making household objects.

![](https://textile-academy.org/content/images/2022/04/205080136_1425680251140614_5137997462116190701_n.jpg)

### Fab-Brick

[FabBrick](https://www.fab-brick.com/) is a French company which is selling bricks made out of textile waste since 2018. The patent is available in French [here](https://patentimages.storage.googleapis.com/64/df/56/7f56a36acdb6a2/FR3102194A1.pdf).



![](https://static.wixstatic.com/media/21e6f9_6fbff8675ff04cc19d27adf14db63472~mv2.jpg/v1/fill/w_1901,h_1216,al_t,q_90,usm_0.66_1.00_0.01,enc_auto/21e6f9_6fbff8675ff04cc19d27adf14db63472~mv2.jpg)

### Binishells

Nicolò Bini is exploring furthermore the experiments of his father Dante Bini who was pouring wet concrete on the ground and slowly inflated it to create architectures. Nicolò Bini is using inflatable structures to be able to have higher and custom shapes, then he sprays concrete on it.

:::youtube 1kYqSEt8Yug

## Crystals

SPOILER ALERT: It didn't work out… 

!!! Recipe
    - Boil Demineralized Water
    - Add your powder in the warm water until it doesn't dissolve anymore
    - Suspend an object on top of the container and submerge one part of it
    - Cover with a light fabric to avoid contamination


### Iron sulfate

Browsing [Crystalverse](https://crystalverse.com/iron-sulfate-crystal-ring/) blog, I saw that iraon sulfate was a quite fast process for crystallization. The green obtainted on that blog is really appealing. I had iron sulfate which was already partly crystallized in beautiful green chunks. I tried to use the white powder to keep the existing crystals. The water turned directly brownish when I added the powder because it oxydizes with the oxygen from the water. I kind of expected that. On Crystalverse, the author adds sulfuric acid to remove that oxydation. But I was ok to have orange/brownish crystals. I planned to make crystals growing on yarn that I had spun and hand-dyed with alder to see if there was an interesting color reaction. I thought at first that crystals were formed because I was seeing some cloudy particles around the fiber, but when I took them out of the solution after 3 days, there was nothing solid.

![](https://sf.ezoiccdn.com/ezoimgfmt/crystalverse.com/wp-content/uploads/2021/06/chart.jpg?ezimgfmt=ng:webp/ngcb1 "From Crystalverse.com.")



![](../images/week11/crystals/iron-sulfate.jpg "Ingredients: Iron sulfate green crystals, white powder.")

![](../images/week11/crystals/iron-sulfate-dust.jpg "After a couple of hours, some particles seem to have formed around the fibres.")

![](../images/week11/crystals/iron-sulfate-result.jpg "Result: there are no crystals but the yarn color reacted to the iron sulfate.")


### Calcium chloride

I tried to make Calcium Chloride crystals. According to this [website](https://sciencenotes.org/grow-calcium-chloride-crystals/), it is supposed to work. But it didn't work after 3 days, I kept the solution to see if there's still some evolution…

![](../images/week11/crystals/knit-lace.jpg "Machine knitted lace for crystals to grow onto.")

![](../images/week11/crystals/calcium-chloride.jpg "Yarns, LED, flexible LED dipped in calcium chloride solution.")

![](../images/week11/crystals/calcium-chloride-result.jpg "After 3 days, no crystals…")

![](../images/week11/crystals/calcium-chloride-result-liquid.jpg "There is some particles at the bottom though, I let it sit for a couple of days to see if there's some evolution.")





## Textile waste + white glue

!!! recipe
    - 50% water + 50% white glue (in weight)
    - shredded textile waste (should work with waste yarns too)

I made a solution of 50/50 weight ratio of water and white glue (wood glue) that I mixed with textile waste. The textile waste was shredded with a [Precious Plastic](https://www.preciousplastic.com/) machine that we have at Green Fabric to recycle sewing waste. I left it for the weekend at the lab, and it hardened when I came back. The big block has an interesting smooth and shiny finish, probably according to the type of material you use for the mold.  I am quite pleased with the result and it would be interesting to make bigger bricks. I think it could also work with tapioca glue (see next section).

![](../images/week11/white_glue/white-glue_water.jpg "Mixing 50/50 weight water and white glue.")

![](../images/week11/white_glue/textile-waste.jpg "Shredded textile.")

![](../images/week11/white_glue/mix.jpg "Adding shredded textile.")

* * * 

![](../images/week11/white_glue/drying.jpg "Drying the bricks on a heater.")

![](../images/week11/white_glue/result.jpg "Result (view from top).")

![](../images/week11/white_glue/result-back.jpg "Result (view from bottom).")

<iframe title="brick-small" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/70971ced-7ca7-4bc8-93c8-eeaba5d329d9" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

<iframe title="brick-big" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/3325397e-a13a-45dc-bf21-6f9d67596f30" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## Papier mâché with threads (tapioca glue)

!!! recipe
    - in a pan add:
        - 150g room temperature water
        - 15g tapioca flour
    - stir well to remove any lumps
    - put the pan on the stove on very low temperature
    - stir all the time to avoid lumps
    - stop cooking when it becomes transparent and thick
    - if it's too thick: 
        - put the pan out of the stove
        - add water and stir until you obtain a regular texture
        - put it back on the stove until the texture is to your taste


I wanted to try the "papier mâché" technique with textile waste. Instead of white glue, I tried a homemade glue with tapioca flour and water. My mother said her father used that to glue the packaging of cakes he would sell in Laos. I first put 15g of tapioca in 150g water, but she said it wasn't enough, so in total I put 30g tapioca for 150g water. In the end, it was too concentrated and I had to put water so that it would not be too thick. I think 10% tapioca of the weight of water should be enough.

I dipped yarn waste in the mixture and glued them on a balloon to make a bird house with my daughters. The texture was too gluey for my little one and she gave up. After one day, the top was dry but not the bottom so I couldn't break the balloon to see the result yet. But the top is quite hard so I think it is going to stay hard after the balloon pops out. 

![](../images/week11/tapioca/stove.jpg "Cooking tapioca + water.")

![](../images/week11/tapioca/yarn-waste.jpg "Yarn waste from knitting.")

![](../images/week11/tapioca/gluey1.png "Gluey texture.")

![gluey2](../images/week11/tapioca/balloon.gif "Balloon ready to dry. The green mark is a guide to leave a hole.")




## CNC course

[Camille Vierin](https://www.expologymakers.be/), a resident at Green Fabric, shares her CNC machine with the rest of the lab. She is using a [Mekanika](https://www.mekanika.io/) machine which are machines made in Belgium that you have to assemble yourself with a great documentation. Plans are open source for easy customization and repair. 

![](../images/week11/cnc/cnc.jpg "Mekanika CNC milling machine.")

Camille gave us a course on [Fusion 360](https://www.autodesk.com/products/fusion-360/overview) to prepare a file for the CNC machine. She gave us an existing .dxf file of a book holder from Mekanika to train us.

- [book holder .dxf file](../files/week11/formation CNC/bookholder.dxf)

??? "Manufacture tab"
	- Orientation: change plans
	- Stock/Brut: setup your material
		- relative size cube: to cut in a material which has already been cut before
		- stock top offset: put it back to 0, otherwise it adds 1mm on the top of the material
		- setup/origin: stock box point, select the point at the bottom left of the stock to only have positive coordinates
	- Operations: always start from the inside to the outside
		- Turning/2D pocket
			- for laminated wood: use a negative milling cutter (around 40€) or a compression milling cutter (both positive and negative, 50€), otherwise a positive cutter (15€) will rip the fibers out. 
		- Cutter (define your cutting tool)
			- flat end mill, 2 flutes, carbide (better than HSS)
			- diameter: 8mm
			- shaft diameter: 8mm
			- overall length: 70mm
			- length below holder: 30mm
			- shoulder length: 25mm
			- flute length: 22mm (pay attention to this one)
		- Cutting data (this is specific to each machine and the cutting tools)
			- spindle speed: 20000rpm
			- surface speed: empty
			- ramp spindle speed: 20000rpm
			- cutting feedrate: 2760mm/min.
			- the following settings should be filled automatically
			- check feed per tooth result according to material, size and type of tool ([feed per tooth tables](https://mae.ufl.edu/designlab/Advanced%20Manufacturing/Speeds%20and%20Feeds/Speeds%20and%20Feeds.htm))
			- Validate

??? "Pocket, Properties box on the right"
	- Tool
		- plung feedrate: 1000 mm/min.
	- Geometry: select bottom path of the pocket.
	- Height: a view to let you check all important heights (materials and movements)
	- Passes
		- [x] Both ways
		- [x] Multiple Depths (max half of diameter)
		- Max roughing stepdowns 3mm
		- [ ] Stock to leave
	- Linking
		- [x] Allowing retract
		- Safe distance: 4mm
	- Leads and transition
		- [ ] Lead-in (entry)
		- [ ] Lead-out (exit)
	- Ramp
		- Helical Ram Diameter: 7.6mm
		- Max ramp stepdown : 3mm

??? "2D Contour, Properties box on the right"
	- Geometry: select bottom outside paths
		- Tabs (to not cut fully the material so that it doesn't float in the void when cut)
			- Number of tags: 4
			- Tab width: 6mm
	- Passes
		- [x] Both ways
		- [x] Multiple depths
		- Max routing stepdown: 3mm
		- Safe distance: 4mm
	- Linking
		- [ ] Lead-in (entry)
		- [ ] Lead-out (exit)
		- [x] Ramp: 2deg/3mm/2.5mm 

??? "Going on the CNC machine"
	- Mekanika's CNC machine uses the PlanetCNC sofware
	- setup the CNC tool 
	- Home button
	- move origin with arrows to the bottom left corner of the desired cutting area
	- XY button: set the XY origin here
	- move by hand the head above the spoilboard
	- put the probe underneath the tool and clip its red plug onto the tool
	- Z button: setup the Z origin automatically with the probe
	- setup the spindle's rotation speed (20000rpm [5] for this file) 
	- setup the suction shoe
	- load the file on the panel
	- plug the spindle
	- turn on the spindle
	- check if weird noise
	- turn on the hoover
	- launch



Camille suggests to buy CNC tools on [Fraiser Bits](https://fraiserbits.com).

![](../images/week11/cnc/cnc-tools.jpg "The drilling tool we used for the course.")

![](../images/week11/cnc/cnc-probe.jpg "Probe to setup the Z axis.")

![](../images/week11/cnc/cnc-panel.jpg "Mekanika CNC milling machine panel.")

PICTURE RESULT
