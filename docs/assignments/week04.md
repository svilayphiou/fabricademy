
# 4. BioChromes

## Open Colours

Cecilia Raspanti quoted Johannes Itten twice during the Biochromes course:

> Color is life; for a world without color appears to us as dead. Colors are primordial ideas, the children of light.
> <footer>Johannes Itten</footer>

And with reason as colors are a fundamental aspect of our world. They are so natural that they can seem obvious, but they are not at all. And you realize that when you try to actually produce them. It is not that simple.

On computers, because we don't need to actually produce them, colors also seem obvious and accessible. But in 2022, Pantone Inc. who has the monopole of colors for printing, stopped its contract with Adobe. Since forever, Adobe had a contract to make Pantone colors available in all their software. In 2022, suddenly the colors would disappear. If you'd open a former document made with Pantone colors now, you would see black instead. If you want Pantone colors, you now need to buy subscriptions to Pantone directly. I've seen that they seem to be workarounds for that, but this shows how colors are actually not so obviously available. When you use only free/open source software you know that as there are no Pantone colors by default, you also need to apply workarounds. In 2010, artist ginger coons worked on making an open standard for colors to be free of Pantone, but to be able to apply those standards to many fields besides print: fabrics, paints, etc. Here is her talk [Libre Graphics Meeting 2010](https://libregraphicsmeeting.org/2010/). Sadly, it seems the project did not pursue afterwards.

::: youtube controls MOxUxipmcvM 


## Colors in general

Here are some inspirations about the use of colors in general.


### Kukka | Laura Luchtman

> Chromarama sprouted from my desire to make people aware of the overlooked role of visual limitations in design. My Chromarama series helps people with colour vision deficiency distinguish between colours. The work embodies colour and form studies, each with its own function for a particular type of colour blindness. Colours, shapes and textures are coordinated in such a way that the designs and colours are visible to the viewer.
> <footer>Laura Luchtman</footer>

::: instagram CwXsrtioAFq

### Raw color
[Raw Color](https://www.rawcolor.nl/) is a duo (Daniera ter Haar and Christoph Brach) working on colors through many different fields: graphic design, photography, product design. I find their global work very inspiring as they address colors through such different supports.

![](https://www.rawcolor.nl/project/CollectionPalette2/hl_kleuren27_800.jpg "Collection Palette from natural resources for Hollands Licht (art direction).")

![](https://www.rawcolor.nl/project/TempTextExpo/TempTex_Expo7.jpg "Temperature Blankets. Data visualization.")




## Recycled colors

I am deeply interested in how to obtain colors by using those who already exists through the recycling of materials. It has the advantage to obtain colors difficult to have with natural dyes and also to avoid the use of water.


### Manteco

[Manteco](https://manteco.com/) is a wool spinning factory in Prato, Italy. They recycle wool since 1943 (actually many wool factories in Prato do that). They describe [here](https://manteco.com/how-are-recycled-wool-colors-created-without-dyeing/) their process to reproduce the colors of their catalogue.

![](https://manteco.com/wp-content/uploads/2021/07/IMG_3477-min.jpg "Sorting colors.")

![](https://manteco.com/wp-content/uploads/2021/07/IMG_9576-min.jpg "Choosing colors.")

![](https://manteco.com/wp-content/uploads/2021/07/IMG_3656-min.jpg "Weighins percentages of colors.")


### i-did

[i-did](https://www.i-did.nl/) is a factory in the Netherlands which makes felt panels out of recycled uniforms. Their color palette comes from the uniforms of different companies. I love that you can precisely source the origins of the fabric.


![](https://static.wixstatic.com/media/21a845_2cbcd15ae40b4d8d93bc0b32206bcf93~mv2.png/v1/fill/w_300,h_180,al_c,q_80,usm_0.66_1.00_0.01/21a845_2cbcd15ae40b4d8d93bc0b32206bcf93~mv2.png "KLM uniforms")

![](https://static.wixstatic.com/media/8245b2_9d34f915cee144499d25b4b23d5fb071~mv2.png/v1/fit/w_279,h_168,q_75/8245b2_9d34f915cee144499d25b4b23d5fb071~mv2.png "Dutch Army uniforms")

![](https://static.wixstatic.com/media/21a845_5b0ad1fba7684cc6bbb7617f549f06e1~mv2.png/v1/fill/w_300,h_180,al_c,q_80,usm_0.66_1.00_0.01/21a845_5b0ad1fba7684cc6bbb7617f549f06e1~mv2.png "Efteling uniforms (amusement park)")





## Natural colors

### Sasha Duerr

[Sasha Duerr](https://sashaduerr.com)'s book *Natural Palettes: Inspiration from Plant-Based Color* (2022) is a superb book on natural dyes. I just love  

![](https://images.squarespace-cdn.com/content/v1/57453b337da24fdfe9e2b16e/1610586280574-0C62NED265937L052YUF/Screenshot+2021-01-13+at+2.49.49+PM.png?format=1500w "Book cover")

![](https://m.media-amazon.com/images/I/71ZaqJVT9rL._SL1500_.jpg "Acorns dye. Non-mordanted; Alum; Alum + Iron; Iron.")


### Slow Stitch Studio

[Slow Stitch Studio](https://slowstitchstudio.com/) is a duo (Ann & Serge) who met during a training in textile crafts in Japan. They are now based in Chiang Mai, Thailand, where they employ a team full-time to produce natural dyed clothing. I like the mix of traditional techniques with a more contemporary approach in the patterns.

::: instagram CpSqAvlSN-v

::: instagram ChuWKVVJQGv

::: instagram Ckc84VVpquv



### Laboratorium

[Laboratorium](http://laboratorium.bio/) is the experimental lab for art/design and biotechnology at KASK/School of Arts Ghent, Belgium. As a biolab in art and design it focuses on exploring different interactions between art, science and technology. María Boto is the responsible of the lab.
They started by exploring colors from micro-organisms (algaes, bacteria…). Now they are exploring structural colors, those you find on beetles, butterflies, which are not pigments but a particular nanostructure which reflects light in a way that produces colors. This new research is visible on the [Ecology of Colour Project](https://ecologyofcolour.be/).

::: instagram CUsUf9VNSRz

::: instagram Cxncfc0tT97

::: instagram Cf3e4UhKn8v



### Nuances de plantes

[Nuances de Plantes](https://nuancesdeplantes.wordpress.com/) is a non-profit organization which has a pedagogical role to preserve and diffuse how textiles were naturally dyed. The core team is composed of Sylvie Lechat, Charlotte Marembert and [Amandine Brun-Sauvant](https://www.instagram.com/atelier.brunsauvant/) who I share my atelier with at [Green Fabric](https://greenfabric.be). They have made the Kaleidogarden: a pedagogical garden of tinctorial plants that were used in the past in Brussels in the dyeing industry.

![](https://nuancesdeplantes.files.wordpress.com/2021/06/dsc09659.jpg "Map of Brussels locating the different industries in textile dyeing")

![](https://nuancesdeplantes.files.wordpress.com/2021/06/dsc09660.jpg "Cota tinctoria, Golden marguerite")

### The Hole Story

I am generally not a super fan of ecoprinting on fabric (except for Slow Stitch Studio who make marvelous things with this technique). Anne Schlüter, from [The Hole Story](https://www.instagram.com/theholestory.ch/) used this technique on yarn. I really love the variegated yarns it produces. We tried with Green Fabric to reproduce the process last year but our yarn was quite yellowish and not white, thus the results were not that nice. I would like to explore this technique furthermore but I haven't had the time this week.

::: instagram CPkrvXzj8bi

::: instagram CVm2VhTqGxr



### Alexandre D’Hubert

I met Alexandre in 2022 when he was finishing his Masters degree at [ÉSAD Valence](https://esad-gv.fr). He showed me his booklet silkscreened with inks he made by foraging plants and minerals during Covid in the North of France.

::: instagram CwfSz7_Mtyq

::: instagram Cl09PO1IqBc

::: instagram CmL1JhkLpRp

### Loes Bogers
Red cabbage dye is a well-known and easy dye to experiment with. The purple color changes to red when you add acid, and to blue (up to green) when you add basic. Loes' experiment on silk are so delicate, really inspiring.

<https://class.textile-academy.org/2020/loes.bogers/files/recipes/cabbagedye/>

![](https://class.textile-academy.org/2020/loes.bogers/images/finalpics-108.jpg)

![](https://class.textile-academy.org/2020/loes.bogers/images/finalpics-100.jpg)


## Mixing carded wools

??? "Tools"
    - handcards
    - electronic spinning wheel (or regular spinning wheel or spindle)
    - knitting machine / knitting needles



I like when colors vibrate especially when you can see the components of a color when you look at it very close. At Green Fabric, I had the chance to share my atelier with [Amandine Brun-Sauvant](https://www.instagram.com/atelier.brunsauvant/), a textile designer specialized in natural dyes. I used woolen fibers which she dyed two years ago and I made 50% mix of madder root & reseda, and then a 50% mix of campeche wood and golden rod.


![](../images/week04/mix/yellow-pink-01.jpg "Wool fibers dyed with madder root (pink) & reseda (yellow).")



![](../images/week04/mix/yellow-pink-02.jpg "Mixed madder root & reseda with handcards.")
















![](../images/week04/mix/purple-yellow-2-01.jpg "Wool fibers dyed with campeche wood (purple) & golden rod (yellow).")



![](../images/week04/mix/purple-yellow-2-0.jpg "Handcarding.")



![](../images/week04/mix/purple-yellow-2-03.jpg "Mixed campeche wood & golden rod.")



![](../images/week04/mix/purple-yellow-2-04.jpg "Fibers spun on the eew6.0 e-spinner.")





![](../images/week04/mix/hanks-rinse.jpg "After spinning, Iset the spin by immerging the hanks into hot water.")

![](../images/week04/mix/hanks2.jpg "Final hanks.")

![](../images/week04/mix/results.jpg "Machine-knitted samples from the handspun yarns.")

Whenever I try a new fiber or a new mix of colors, I like to spin half with a worsted style (remove air from the fiber) and half woolen style (let air in the fiber). Here we can clearly see how the spinning style changes the perception of colors. It looks like different fibers were used to achieve those two samples!

![](../images/week04/mix/purple-yellow-vs2.jpg "Comparing 2 ways of spinning: worsted (right) and woolen(left).")










# Indigo

[Amandine Brun-Sauvant](https://www.instagram.com/atelier.brunsauvant/) gave us (with my Fabricademy fellows [Eileen](https://class.textile-academy/2024/eileen-schreyers) and [Charlotte](https://class.textile-academy/2024/charlotte-jacob)) a one day course on making an indigo vat and dyeing with it.


![](../images/week04/indigo/team.jpeg "*The* team! You can see we were super happy about the workshop.")


## Making the vat

### Vat: 1-3-2 recipe

??? info "ingredients"

	- 4L water
	- 32g indigo extract (8g/L)
	- xxx g fructose
	- xxx g slaked lime

??? info "tools"
    - pestle/mortar
    - small bowls
    - big bucket
    - wooden stick
    - 2 bowls
    - spoon
    - scale


??? recipe "recipe"

	- measure ingredients
	- add
	- simmer
	- mix
	- remove
	- strain
	- repeat



![](../images/week04/indigo/ingredients.jpg "Ingredients: indigo powder,")

![](../images/week04/indigo/mortar.jpg "Pounding the indigo extract into a super fine powder (very long process).")

![](../images/week04/indigo/soup-blue.jpg "When the powder is fine enough, mix it in the water.")

![](../images/week04/indigo/soup-lime.jpg "Adding lime to the soup.")

![](../images/week04/indigo/soup-fructose.jpg "Adding the fructose to the soup.")

![](../images/week04/indigo/soup-whirlwind.jpg "Whirlwind in the soup.")

![](../images/week04/indigo/soup-miso.jpg "It precipitates. It looks like a miso soup. Yum!")

![](../images/week04/indigo/soup-copper.jpg "Soup is ready when it has a copper color.")

![](../images/week04/indigo/vat-preserve.jpg "If you'd like to preserve your indigo vat, you should put something on its surface so that it does not get oxydized.")




## Using the vat

![](../images/week04/indigo/vat-old.png "This is an indigo vat made beforehand. It always looks translucent with a copper color. To use again, it's good to warm it up and stir it.")

![](../images/week04/indigo/vat-giant.jpg "For dyeing, we used both the fresh small vat we made and this gigantic vat which Amandine uses for her work.")

![](../images/week04/indigo/img_5984.jpg "We didn't warm it up to go faster and it worked well.")

![](../images/week04/indigo/rinse.jpg "First, we have to rinse our textile so that they would attract the coloured water faster.")



## Dyeing yarns

![](../images/week04/indigo/strain.jpg "Always strain a maximum of liquid to avoid spoilage and right on top of the liquid level to avoid oxydation.")

![](../images/week04/indigo/yarn-bath1-out.jpg "First bath of handspun wool.")

![](../images/week04/indigo/yarn-rinse.jpg "Between each coloured bath, a rinsing bath to get clean.")

![](../images/week04/indigo/yarn-bath1.jpg "First bath of handspun wool, industrial cashmere, industrial hemp (left to right). Super interesting how the different fibres catch the colours differently.")

![](../images/week04/indigo/yarn-bath-2.jpg "Second bath.")

![](../images/week04/indigo/yarn-knit-results.jpg "After 3 coloured baths and rinses, I made samples with the knitting machine. All of them were a bit harsh to knit but the hemp was the tougher and would break  all the time. Afterwards Amandine told me I probably haven't rinse them enough as they should not feel so harsh and rough.")
![](../images/week04/indigo/yarn-knit-hemp.jpg "Broken hemp…")

![](../images/week04/indigo/yarn-knit-cashmere.jpg "Happy cashmere.")



## Dyeing fabrics


![](../images/week04/indigo/fold-iron.jpg "A chevron pattern folded with an iron.")

![](../images/week04/indigo/green.jpg "After each bath, you need to wait for the oxydation to happen. It turns from yellow-green to blue. You might want to open the folds to have oxydation happening in tiny corners.")



<iframe title="unfolding indigo shibori" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/97388713-dda1-4209-af89-4afbd2ca8c52" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>



![](../images/week04/indigo/folds-final.jpg "Results of my different folding tests. The one on the bottom left was tighten with a sewing thread by making running stitches.")

## Natural dye

I collect many colour sources from time to time in order to "one day" test a natural dye with them. But of course I never have the time… For this week experiments, I dug some alder cones that I collected in a park when I was having a break on my summer vacation in France. I used a handspun wool yarn that I made a couple of years ago.

I first boiled the alder cones in water for a couple of hours. Then I strained the liquid and put it back on the stove. I wet my yarn with water and added it to the alder infusion. I didn't mordant my yarn as I wanted to see what colours I would get without. Alder cones have a lot of tanins so it acts a bit like a mordant.




![](../images/week04/aulne/alder-water.jpg "Handspun wool in the alder infusion..")

![](../images/week04/aulne/alder-nowater.jpg "Don't let your water go away like me!.")

![](../images/week04/aulne/iron-sulfate.jpg "Iron sulfate solution at XX%. The greenish crystals on the left are dried iron sulfate.")

![](../images/week04/aulne/20231017_095641.jpg "Hank half-dipped in iron sulfate solution.")

![](../images/week04/aulne/20231017_110848.jpg "Handspun wool dyed with alder. Grey tones were obtained by dipping it in iron solution after dyeing.")

![](../images/week04/aulne/iron-wool.jpg "Handspun wool dyed with alder. Grey tones were obtained by dipping it in iron solution after dyeing.")

![](../images/week04/20231017_142132.jpg "Handspun yarn. Alder cone dye. indigo dye")





##Natural Ink



![](../images/week04/aulne/20231016_091159.jpg "Weighing handspun woolen yarn.")

![](../images/week04/aulne/20231016_092332.jpg "Weighing alder cones.")

 ![](../images/week04/aulne/20231017_092022.jpg "Weighing water.")

![](../images/week04/aulne/20231017_092022.jpg "Weighing water.")

![](../images/week04/aulne/filter2.jpg "Filtering with embroidery backing waste.")

### Guar gum

My first try was with gum guar.

![](../images/week04/aulne/20231017_093241.jpg "Filtered alder reduced infusion.")



![](../images/week04/aulne/ink-guargum.jpg "Adding guar gum.")



![](../images/week04/aulne/ink-guargum-fail.jpg "I added way too much guar gum. It is way too thick.")



![](../images/week04/aulne/ink-guargum-test.jpg "Testing the guar gum ink with a flat brush. The ink is a bit shiny and thick but not very homogenous.")





### Arabic gum

My second try was with arabic gum. The bath was more concentrated than the first one, so it is darker. 

![](../images/week04/aulne/20231017_093946.jpg "Weighing alder reduced infusion.")



![](../images/week04/aulne/20231017_094012.jpg "Adding the arabic gum.")



![](../images/week04/aulne/20231017_101438.jpg "Obtained ink.")

![](../images/week04/aulne/ink-arabic-test.jpg "Arabic gum ink test with a flat brush. This ink is much more homogenous than the gum guar ink. It's absorbed quicker by the paper.")

![](../images/week04/aulne/ink-tools.jpg "I was quite satisfied with this ink so I tried different tools. It does have a nice iridescent effect.")



