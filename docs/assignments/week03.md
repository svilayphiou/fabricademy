# 3. Circular Open Source Fashion

## F/LOSS

I've been working almost exclusively with Free/Libre and Open Source Software (F/LOSS) for 14 years now. 
I'm therefore quite concerned about the sharing of knowledge and licensing questions. I've been working with [Constant](https://constantvzw.org), an organization for art, media and cyberfeminism in Brussels. They are deeply involved in the making of F/LOSS licenses in Belgium. They've been part of the setup of Creative Commons in Belgium, they helped [Libre Objet](http://libreobjet.org/) to find/write a license for open source furniture design. Recently, they raised the political issues about the existing free and open licenses mainly regarding mis/appropriation. They propose a new license to prevent that: the Collective Conditions For Re-use license (CC4R). You can read it fully here in [English](https://constantvzw.org/wefts/cc4r.en.html), [French](https://constantvzw.org/wefts/cc4r.fr.html), [Dutch](https://constantvzw.org/wefts/cc4r.nl.html).
https://constantvzw.org/wefts/cc4r.en.html

> **Why use the CC4r?**
> 
> - To remind yourself and others that you do not own authored works;
> - To not allow copyright to hinder works to evolve, to be extended, to be transformed;
> - To allow materials to circulate as much as they need to;
> - Because the CC4r offers a legal framework to disallow mis-appropriation by insisting on inclusive attribution. Nobody can take hold of the work as one’s exclusive possession.


Some nice references on *libre* licenses:

- [Un Faible Degré d'Originalité](https://www.youtube.com/watch?v=kHPI3YhY8aU), a very funny theatre play by/with Antoine Defoort explaining the history of intellectual property. Full play is available online in French with English subtitles.
- [*Copy This Book, an Artist's Guide to Copyright*](https://copy-this-book.eu/) by Eric Schrijver is a guide on intellectual property especially focused on artistic practices. It is also available in Chinese and in French.




## Inspiration

<!--### Pleats-->

<!--::: instagram BlqQkdsgkJp nocaption-->

<!--::: instagram CPtCjlOAs96 nocaption-->

<!--foam mathematics-->

<!--::: instagram B_KMl_gohvQ nocaption-->

<!--::: instagram CxVE7w_rlhR nocaption-->



<!--### Tesselations-->
<!--tesselation: point de sortie commun aux quatre pièces-->

<!--cf. https://class.textile-academy.org/2023/lisa-boulton/assignments/week03/-->




### Patterns

![](https://www.mbal.ch/mbalwp/wp-content/uploads/fly-images/477/CROP-Anni-Albers-Second-Movement-II-1978.-Courtesy-Alan-Cristea-Gallery-1280x99999.jpg "Anni Albers")

![](https://dza2a2ql7zktf.cloudfront.net/binaries-cdn/dqzqcuqf9/image/fetch/q_auto,h_920,w_920,dpr_auto,c_fit,f_auto/https://d2u3kfwd92fzu7.cloudfront.net/catalog/artwork/gallery/1506/MOLNAR_Structure___partir_de_la_lettre_N_42x26cm_2016-1.jpg "Vera Molnár")


![](../images/week03/karel-martens.jpg "Karel Martens' combinatory patterns")

![](../images/week03/hella-jongerius.jpg "Hella Jongerius")

![](../images/week03/sigrid-calon.jpg "Sigrid Calon")

![](https://framemark.vam.ac.uk/collections/2006AG2390/full/735,/0/default.jpg "Koloman Moser")

::: instagram CEJkkkEHtBp nocaption
::: instagram CSoEQ-LKKQO nocaption

::: instagram CnEZeVDs3Kp nocaption

::: instagram Cwo_EfNqlKS nocaption
::: instagram Cn7w4sOp-Ky nocaption
::: instagram Cl0Z5GtOl6q nocaption

::: instagram BmJeiTMD-p5 nocaption



::: instagram CcTs9hGOIFB nocaption

::: instagram BsOpacpHDbr nocaption


<!--### kirigami-->
<!--::: instagram CXgy1JOIXcn nocaption-->

<!--Turtle stitch-->

<!--auxetic green-->


### OS Circular Fashion
::: instagram CUpW2fZNEGa nocaption

![](https://gitlab.fabcloud.org/academany/fabricademy/2019/students/analaura.sanroman/raw/master/docs/images/Weel%2003/circular_fashion_seamless_lunar_phases.png "Analaura Sanroman, Fabricademy 2019")

::: figure https://www.googleapis.com/download/storage/v1/b/fabricademy.appspot.com/o/library%2Fprojects%2FEBlU8Z1PFkNxRCXv35HdEPGCEEh2%2F1667983376108_1.jpg?generation=1667983376245480&alt=media
    [Marion Banon, Fabricademy 2022](https://oscircularfashion.com/project/-NGQ_DXSoVq4PV2SyNY8)

![](https://www.datocms-assets.com/53750/1632517058-postcoutureantwerp-medium.jpg?w=1600&fm=jpg "Post-Couture Collective")


## Project: Triangles

### Flatland

My favorite shape is the triangle ever since I read *Flatland: A Romance of Many Dimensions*, one of the earliest science-fiction novel (1884) by Edwin Abbott Abbott. It's a world where people are 2D geometrical shapes, the triangle being the most primitive shape as it's the first shape one can obtain outside of a line. Then the more sides/angles one has, the strongest it is in the society. Until they discover that a 3rd dimension exists…

::: figure https://gutenberg.org/cache/epub/97/images/cover.jpg
    Cover of *Flatland*, Edwin Abbott Abbott, 1884

Anyways, I think the triangle is an interesting shape because it's only 1 point more on top of a line, and that it can particular in many ways (isocele, equilateral, right) or just *any* triangle (*quelconque* in French)…



### Prototypes in 2021

In 2021, I participated in a 3 hours workshop around modular fashion given by Valentine Fruchart from Green Fabric. I knew directly I wanted to use a triangle as a module. I chose an equilateral one as I thought it would offer many possibilities in translation, rotation and it can make lozenges as well. It allowed me to make a "hermaphrodite" module having both notches and flaps so I'd need only one module to connect them altogether. Being equilateral makes it easy to have a rather zero-waste cut in the material.


::: figure ../images/week03/triangles-prototype.jpg
    Prototyping, from paper to leather

::: figure ../images/week03/triangles-cuir.jpg
    First leather modules


### Back to 2023

Since then, I've never had the time to actually play with my module. So for this assignment, I thought it would be nice to crash-test it in a bigger scale object. 


    


## Tools

The article [Wallpaper Group on Wikipedia](https://en.wikipedia.org/wiki/Wallpaper_group) explains rather well how wallpaper group patterns work. There are 17 possible tilings through the combinations of movements. Their names are based on cristallography notation.

### Wallpaper symmetry
<iframe class="website" src="https://math.hws.edu/eck/js/symmetry/wallpaper.html"></iframe>


### Inkscape

I used the `Tiling` path effect (through the Path Effect Editor tab).


![](../images/week03/tiling1.png "1. Select your object. Add `Tiling` --Pavage in French-- in the Path Effect Editor tab.")

![](../images/week03/tiling2.png "2. Result of the basic tiling parameters.")

![](../images/week03/tiling3.png "3. Parameter `rx+ry+cy`. Triangles are upside down.")

* * *

![](../images/week03/tiling4.png "4. Changing the Shifting in *x*.")

![](../images/week03/tiling5.png "5.")

![](../images/week03/tiling6.png "6.")


### FreeCAD

I realized that my triangle drawn in Inkscape was not completely accurate:

![](../images/week03/inkscape-triangle.png)

So I thought it was a good opportunity to finally learn a parametric design software. Fusion360 is quite slow on my computer either the online version or the desktop version on my virtual Windows. And I've always wanted to give a go at [FreeCAD](https://www.freecad.org/). It has the same parametric logic of constraints than Fusion360.

![](../images/week03/tiling2.png "2. Result of the basic tiling parameters.")




### Lasercutter Beambox Pro

- Force  35%
- Speed  20mm/s


![](../images/week03/triangles-skirt.png "Simulation to check how many modules are needed for a skirt.")

![](../images/week03/lasercut.jpg)


![](../images/week03/lab-sample.jpg "The sample given to the lab.")


### Assembly: tests

I first wanted to try out polyester fabrics which had patterns on it.

![](../images/week03/patterns1.jpg )

![](../images/week03/patterns2.jpg )

![](../images/week03/patterns3.jpg )

![](../images/week03/pixel.gif "The patterns of the white fabric is printed, therefore one side remains white. So I made a test of a “pixel” or sequin that you could flip/activate by using the flaps." )


Then I came back to just 2 plain colors, still in very fine polyester and chambray (cotton and polyamide). 

![](../images/week03/test.jpg)

![](../images/week03/skirt2-1.jpg)

### Assembly: 1st try

I wanted to challenge myself and do a garment to explore 3D shaping.  Working like this felt like scuplting. 

I folded the triangles horizontally to make straight edges. They could even welcome a string to tighten things up if necessary.

It was nice at first but then I quickly realized that I had to make too many overlaps to make curves. I was not so convinced by this process.


![](../images/week03/skirt1.jpg)

![](../images/week03/skirt2.jpg)

![](../images/week03/skirt3.jpg)



### Assembly: 2nd try

I started over by first making curved lines for the front and back hip curvature. Then I made vertical lines out of this base. I then connected the vertical lines together and added some modules where there was a gap. I folded the triangles vertically to make smaller columns and to add “pleats”. This is possible because the material is very thin.

I first started on this black mannequin, but because it's recovered of felt, the burnt polyester was always getting stuck on the mannequin.


![](../images/week03/skirt2-2.jpg)

![](../images/week03/skirt2-3.jpg)

![](../images/week03/skirt2-4.jpg)

![](../images/week03/skirt2-5.jpg)

![](../images/week03/skirt2-6.jpg)

![](../images/week03/skirt2-7.jpg)

![](../images/week03/skirt2-8.jpg)

![](../images/week03/skirt2-9.jpg)


### Tips

![](../images/week03/tweezers.gif "I used tweezers to pass the little flaps throught the little holes.")

![](../images/week03/extension.gif "I could make extensions by overlapping triangles.")




### Files

Source files available on [oscircularfashion.com](https://oscircularfashion.com/project/-NgT-rDdb1inkzK-sM8j).

