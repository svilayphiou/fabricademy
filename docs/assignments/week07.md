# 7. Computational Couture

## Inspiration

### [Arnaud Pfeiffer](https://www.instagram.com/arnaudpfef/)
This is not 3D printing, but very inspirational parametric designs.

::: instagram CjBpY2-sdnR

::: instagram Cgw2z6Bs-vX

::: instagram Cetv9toL4vi


### Amandine David + Esmé Hofman

[Amandine David](https://www.instagram.com/amandine__david/) and [Esmé Hofman](https://www.instagram.com/esmehofman/) have been collaborating for quite a while in mixing traditional crafts and contemporary technologies. The project Meshes is based on parametric lasercut modules designed in Grasshopper by Amandine David in Brussels. She sends those to Esmé Hofman in Eindhoven to weave them. Below are pictures from [Dutch Design Daily](http://dutchdesigndaily.com/complete-overview/amandine-david-esme-hofman/).

![](../images/week07/amandine-david+esme-hoffman_dutch-design-daily01.jpg "Amandine and Esmé.")

![](../images/week07/amandine-david+esme-hoffman_dutch-design-daily02.jpg "Grasshopper parametric lasercut modules by Amandine.")

![](../images/week07/amandine-david+esme-hoffman_dutch-design-daily03.jpg "Esmé Hofman weaving with the modules.")

### Studio Minimètre

[Studio Minimètre](https://www.instagram.com/studio.minimetre/) is a Brussels-based 3D printing studio. They are very accurate on their tools and eco-conception. They optimize their prints to save time and materials. For example, a classical mask holder would take between 105 and 150 minutes to print. Their design (see below) prints in only 10 minutes. They are researching eco-friendly material such as filaments made of food waste (coffee, mussels…) and 3D printing ceramics. They recycle their PLA waste into new filaments and recently they are grinding metal waste from furniture design into powder to insert it into their DIY-recycled PLA.

:: instagram CAGLobfBVzT

::: instagram CwkNHbTt7xa

### Full Control xyz

[Full Control XYZ](https://fullcontrol.xyz/) is a software to directly generate gcode designs. You can either make your own models in Python or you can alter the designs already existing and play with parameters. 

::: instagram ClFDwSNDTja


::: instagram CzO0QIwNduR

::: instagram CzV2rn7Nd8B


### Krizia

I am not a big fan of her aesthetics, but [Krizia](https://www.instagram.com/sewprinted/) has a lot of interesting 3D printing fabrics techniques.

::: instagram CrbUF8yAa4g

::: instagram CkOXEWRA4ko

::: instagram CbAy4UeA0KF


### Kate Reed
[Kate Reed](https://class.textile-academy.org/2021/kate.reed/assignments/week07/), Fabricademy 2021.

### Haneen Jaafreh
[Haneen Jaafreh](https://class.textile-academy.org/2022/haneen-jaafreh/assignments/week07/), Fabricademy 2022


### Chainmail knit
Didn't have time to print this, but this model looks very fun to play with! Could be added on the sides of any 3D printed fabrics to assemble pieces.

<https://www.thingiverse.com/thing:3619166>

![](https://img.thingiverse.com/cdn-cgi/image/fit=contain,quality=95/https://cdn.thingiverse.com/renders/1d/c2/d2/df/ee/864a9002ee377edf546479b013d51a57_display_large.jpg)


## Tools

!!! example ""

	- [OpenSCAD](https://openscad.org/)
	- [Prusa Slicer](https://www.prusa3d.com/page/prusaslicer_424/)
	- [StippleGen 2](https://www.evilmadscientist.com/2012/stipplegen2/)
	- [Grasshopper](https://www.grasshopper3d.com)
	- [Rhino3D](https://www.rhino3d.com)



## Tiling path effect in Inkscape
As a first and very quick & dirty prototype, I made a parametric pattern in Inkscape using the Tiling path effect. I used the randomizer to change randomly the size of the clones.

![](../images/week07/inkscape-triangles.gif "Different renderings of the randomized tiling path effect in Inkscape.")


Then I imported the .svg file in [TinkerCAD](https://www.tinkercad.com/) to extrude it and make an .stl file. Then I printed it with PLA on a lacey fabric. I like a lot the hard but flexible textile it produces. I block the fabric on the plate with magnets and clips, but with other prints, I figured it was not so stable, the fabric is sometimes not stretched enough. A better solution is to remove the plate, and tape the fabric underneath the plate. Also, you need to raise your Z-level as the fabric has a thickness. I used the "Live Z adjust" of the Prusa machine to do it live and not having to calibrate each time you change a fabric.

![](../images/week07/inkscape-triangles-prusa.jpg "3D print of randomized triangles with PLA on lacey fabric.")


![](../images/week07/inkscape-triangles.jpg "")


<iframe title="triangles-lace-mute" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/ac4f8376-3e33-467c-9ae8-35b80fbd30a0" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>


## Infill method

The 3D printer at [Green Fabric](https://greenfabric.be) is a Prusa Mini. So we usually use [Prusa Slicer](https://www.prusa3d.com/page/prusaslicer_424/) to translate .stl files into .gcode files. It is a very interesting software and you can also use it for other brands. They have a lot of presets for a lot of machines and filaments.

Krizia from Sew Printed shows in this [video](https://www.youtube.com/watch?v=BW_l6PvyC3c) how to use infill parameters to make 3D printed fabric.

I used this method to 3D print concentric circles on a fabric directly with Prusa Slicer.

??? Recipe
    - 1. Right-click on the canvas.
        - Add a cylinder.
        - Change its dimension on the right panel (in my case: X:150mm, Y:150mm, Z:1mm).
        - Go to the "Print Settings" tab.
    - 2. Layers and perimeters
        - Vertical shells/Perimeters: 0
        - Horizontal shells/Solid layers/Top: 0
        - Horizontal shells/Solid layers/Bottom: 0
    - 3. Infill
        - Infill/Fill density: 5% (change according to what you like)
        - Fill pattern: Concentric
    - 4. Skirt and brim
        - Skirt/Loops: 0
        - Brim/Brim type: No brim

![](../images/week07/infill-method1.png "Step 1. Cylinder.")

![](../images/week07/infill-method2.png "Step 2. Remove shells.")

![](../images/week07/infill-method3.png "Step 3. Concentric fill type.")

![](../images/week07/infill-method4.png "Step 4. Remove skirt and brim.")

![](../images/week07/infill-method5.png "Step 5. Slice.")

![](../images/week07/infill-method.jpg "3D print with PLA.")

![](../images/week07/infill-method2.jpg "3D print with TPU flex.")


## Print on transfer paper
After seeing [Paraprint](https://paraprint.io/)'s recitation on Wednesday, I wanted to try [her trick to print transparent filament onto a sublimation paper](https://www.youtube.com/watch?v=FYWYAf8dP7I). Sublimation is a physical process where something goes from solid state to gas state without passing through liquid state. 
We actually did not have that specific type of paper but only regular transfer paper from my Fabricademy fellow [Eileen](https://class.textile-academy.org/2024/eileen-schreyers). Both work the same way, you need to print your design on the paper with an inkjet printer. According to [this website](https://mug-thermoreactif.fr/blog/quelle-est-la-difference-entre-le-papier-de-sublimation-et-le-papier-de-transfer-n7#:~:text=Papier%20de%20sublimation%20versus%20papier%20de%20transfert%20thermique&text=Les%20deux%20types%20doivent%20utiliser,principalement%20pour%20les%20t%2Dshirts.), sublimation paper uses sublimation ink or a sublimated surface on the target object. We gave it a shot anyways with Eileen and it did work! I used a rectilinear fill type whereas Eileen used a Gyroid fill type; hers looks much better as it gives a kind of fabric texture. Go have a look at [her page](https://class.textile-academy.org/2024/eileen-schreyers/assignments/week07/) to see the result.




![](../images/week07/transfer-paper.jpg "3D print with transparent TPU flex on transfer paper.")

<iframe title="transfer-paper-mute" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/46e4a506-1e71-4504-9fa1-36efcb969006" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

### Update: ironing

I wanted to test ironing different sheets on a 3D printed object to see if they would stick to it.

![](../images/week07/iron.jpg "Ironing stuff on 3D print.")


![](../images/week07/iron+transfer-paper.jpg "Ironing transfer paper: FAIL.")

![](../images/week07/iron+vynil.jpg "Ironing heat transfer vynil: FAIL.")

![](../images/week07/iron+aluminium-foil-art.jpg "Ironing aluminium foil art: SUCCESS!")





## Grasshopper

??? Links
    - Text and images Grasshopper documentation (and no videos): [Parametric by design](https://parametricbydesign.com/grasshopper/how-tos/domains/)

I was very curious to give a try at Grasshopper but I got actually discouraged after a couple of hours trying to understand how it worked. I managed to draw a triangle directly from it after a couple of hours, and understood how to add an element drawn in Rhino into Grasshopper. But I eventually gave up as I use Rhino/Grasshopper on a Virtual Machine on my Linux OS. And because of this, my Windows window is quite small and I can't use an external screen to put Grasshopper on the side. Also, I feel like Grasshopper is not super fluid to work with compared to other nodal programming software like [Pure Data](https://puredata.info/). It is very difficult to understand what type of inputs and outputs a component asks for. It is important to me to build something frmo scratch to be able to understand how a software works, and I found it quite difficult to continue with Grasshopper…


## Open SCAD

So after Grasshopper, I gave a shot at Open SCAD. I have been curious and overwhelmed about it for a while. But in the end, it was super easy to dive into it, I was able to build a triangle in 5 minutes. 

Open SCAD is a 3D modelling software but only through programming. It has its own programming language, but it is the same paradigm as Python which I know well so it was easy to get to it. I find its language quite elegant and was quite amazed by its system to build a GUI (Graphical User Interface) to change your parameters. It also has a sort of command line prompt at the top (like the one in Rhino), which makes it quite convenient to code without knowing all methods by heart.


```
/////////////////////////////////
// FOR LOOP
/////////////////////////////////

// Parameters of 3 points of a triangle
A=[0,0];
B=[10,10];
C=[10,0];

for (i=[0:10])
    color([1, 0, i/10])
    linear_extrude(height=2)
    translate([i*11,0,0])
    polygon([A, B, C]);
/////////////////////////////////
```
![](../images/week07/openscad-forloop.png "A simple for loop.")
{: style="float: right;"}


```
/////////////////////////////////
// NESTED FOR LOOPS (notably to make a grid of elements)
/////////////////////////////////

// Parameters of 3 points of a triangle
A=[0,0];
B=[10,10];
C=[10,0];

for (x = [-20 : 10 : 20],
     y = [0:10])
    translate([x,y*10]){ 
        color("blue") 
        polygon([A, B, C]);
    }
/////////////////////////////////
```

![](../images/week07/openscad-nested-forloops.png "Nested for loops.")
{: style="float: right;"}

```
/////////////////////////////////
// LINES / SUN
/////////////////////////////////

// Change this number to choose how many lines you want, 
//the angle of the lines will be calculated automatically
LINES = 50;

for (i=[0:360/LINES:360])
    rotate(i)
    translate([0,10,0])
    color("pink")
    cube([0.5,i%2 * 15 + 15,1]);
/////////////////////////////////
```

![](../images/week07/sun.gif "Circle with alternating line lengths.")

![](../images/week07/sun-PLA.jpg "3D printed in PLA, 0.4mm nozzle.")

![](../images/week07/sun-PLA.gif "3D printed in PLA, 0.4mm nozzle.")

![](../images/week07/sun-TPU.jpg "3D printed in TPU flex, 1mm nozzle.")

![](../images/week07/sun-TPU.gif "3D printed in TPU flex, 1mm nozzle.")


## Stipples

I was amazed by Julia Koerner's [Setae project](https://www.juliakoerner.com/setae).

![](https://static.wixstatic.com/media/4d9771_fcd74049b7d4451eaea999edf5b0bdfe~mv2_d_2143_3000_s_2.jpg "Julia Koerner, Seta project. Photography: Ger Ger.")

I thought to give it a try with an image I made a few years ago: a portrait of Ada Lovelace already dithered with the software [Texturing](http://ivan-murit.fr/project.php?w=texturing&p=download.htm).
It's quite easy in OpenSCAD to import a .png image and extrude the pixels.

```
/////////////////////////////////
// EXTRUDE PNG
/////////////////////////////////
// replace "ada.png" by your image file
// change the scale values [X, Y, Z] according to your needs
scale([0.5, 0.5, 0.1])
surface(file = "ada.png", center = true, convexity = 1, invert=true);
```

![](../images/week07/ada-TexTuring-6235.png "Original image.")

![](../images/week07/ada4.png "Image extruded according to black values of pixels in OpenSCAD.")

![](../images/week07/ada1.png "Sliced model in Prusa Slicer. One day of printing!!!")


Because it was too long to plot, I used [StippleGen 2](https://www.evilmadscientist.com/2012/stipplegen2/) to have less dots but still having a legible image. StippleGen is a software to dither your images based on Voronoi diagrams. It was especially thought to plot images on eggs or other spherical objects. Using such a dithering does not deform the image when printing on a round object. 


![](../images/week07/stipplegen.gif "Different settings in StippleGen2.")

![](../images/week07/ada-stipples-sliced.png "Ada put through StippleGen2 and Prusa Slicer. 4h of print.")

I didn't have time to print it. And by looking at it now, I think this image does not work very well with this technique. I think it would work better with a grayscale images which would then give different heights instead of different thicknesses.


* * *

```
/////////////////////////////////
// EXTRUDE SVG
/////////////////////////////////
// replace "ada.svg" with your .svg file
// change height according to your need
linear_extrude(height = 20, center = true, convexity = 10)
import(file = "ada.svg", layer = "plate");
```

It is still quite long to print and didn't have the time to print it as we are three to share the 3D printer this week.


## Chainmail

I've always wondered how chainmail kind of 3D prints could actually be 3D printed and be flexible at the same time. To fully understand that, I wanted to create one myself in OpenSCAD. I found this [Blender tutorial](https://i.materialise.com/blog/en/how-to-design-and-3d-print-chainmail-and-other-interlocking-parts/) to understand the logic of it.

It took me quite a while to modelise the grid so that the triangles would not touch themselves. When I succeeded I made a 3D print which was very abstract! Because the grid was not dense enough.




![](../images/week07/chainmail1.png "First try at a triangular chainmail.")

![](../images/week07/chainmail-mess.jpg "Chainmail mess.")

Thanks to the parameters and the nice automatic GUI OpenSCAD makes out of your parameters, I managed to change the thickness of the triangles to make a denser grid.

![](../images/week07/chainmail-dense.png "Denser triangular chainmail by playing with the parameters on the right panel. The parameters are generated from the comments you add next to your parameters in your code (left panel).")

Prusa Slicer warns you when there might be issues in your print. 

![](../images/week07/chainmail-stability-issue.png "Prusa Slicer warning about stability issue.")

To prevent those stability issue, I added a support enforcer. It's a very nice feature in Prusa inbetween an automatic and manual support making. Don't forget to check the option in "Print Settings/Support Material/Generate Support Material" but without the "Auto-generated support".

![](../images/week07/chainmail-enforcer-add.png "Add a block as support enforcer.")

![](../images/week07/chainmail-enforcer-block.png "Resizing and placing support enforcer.")

![](../images/week07/chainmail-enforcer-generate-support.png "Selecting “Generate support material”.")

![](../images/week07/chainmail-enforcer-slice.png "Preview of slicing with support enforcer.")


I had issues to print it still as the first layer is only very small parts, they were not sticking to the bed. I decreased the first layer speed to 10mm/s and raised the first layer bed temperature to 70°C. It helped the first layers to print. But then, it got messy again when I was not looking and I didn't have time to launch the print again.

??? Code chainmail
    ```
    /* [TRIANGLE] */
    A=[0,0];
    B=[10,20];
    C=[20,0];


    /* [TILING] */
    rows= 5; //[0:10]
    columns= 4; //[0:10]


    /*[VARIABLES]*/
    rotation=13; //[0:40]
    SHIFTX=22; //[0:40]
    SHIFTX2=22; //[0:40]
    SHIFTY=14; //[0:40]
    SHIFTY2=14; //[0:40]
    Z=3; //[-10:10]


    // BUILD THE OUTLINE OF A 3D TRIANGLE
    module triangleOutline(){
        difference() {
             linear_extrude(2) offset(r = 2, delta=10, chamfer=false) {
                polygon([A, B, C]);
             }
             linear_extrude(2) offset(r = 0.2) {
                polygon([A, B, C]);
             }
        }
    }
    
    // RED GRID OF TRIANGLES
    for (i=[0:columns],
         j=[0:rows])
            color("red")
            translate([i*SHIFTX,j*SHIFTY,-Z/2])
            rotate([rotation,0,0])
            triangleOutline()
    ;
    
    // BLUE GRID OF TRIANGLES
    for (i=[0:columns],
         j=[0:rows])
            color("blue")
            translate([i*SHIFTX2+11,j*SHIFTY2+6, Z/2])
            rotate([-rotation,0,0])
            triangleOutline()
    ;
    
    ```






## 3D Models
- [Sun](https://www.thingiverse.com/thing:6302484)
- [Chainmail triangles](https://www.thingiverse.com/thing:6302477)
