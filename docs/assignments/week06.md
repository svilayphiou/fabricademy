# 6. BioFabricating Materials

## References



### Dutch Design Week 2022
Last year, I visited a small but incredible exhibition on biomaterials at Dutch Design Week 2022. Here are pictures that I took back then.

![](../images/week06/ddw22/feral-mind_your_breakfast.jpg "Feral by Mind Your Breakfast.")

![](../images/week06/ddw22/feral-mind_your_breakfast2.jpg)

![](../images/week06/ddw22/feral-mind_your_breakfast3.jpg)

* * *

![](../images/week06/ddw22/lionne_van_deursen-bacterial_cellulose.jpg "Lionne Van Deursen, bacterial cellulose.")

* * *
![](../images/week06/ddw22/mari_koppanen-amadou_mushroom.jpg "Mari Koppanen, amadou, a suede-like material derived from a polypore mushroom.")

![](../images/week06/ddw22/mari_koppanen-amadou_mushroom1.jpg)

![](../images/week06/ddw22/mari_koppanen-amadou_mushroom2.jpg)


* * *


![](../images/week06/ddw22/mari-koppanen_kombucha.jpg "Mari Koppanen, kombucha.")

![](../images/week06/ddw22/mari-koppanen_kombucha2.jpg)


### Lucile Haute 
[Lucile Olympe Haute](https://lucilehaute.com) is a friend that I met through graphic design practices. In parrallel, she's been exploring Kombucha as a cosmic talisman through artistic performances. She has led a stunning [workshop on Kombucha as a biomaterial](https://www.instagram.com/stories/highlights/18023732344461765/). She is now in a 10 months residency at Cité Internationale des Arts (Paris) to explore Kombucha furthermore.


::: instagram CvAWGwXN1hz

![](../images/week06/lucile-haute_kombucha-thread.jpg "Kombucha thread.")

![](../images/week06/lucile-haute_kombucha-laser.jpg "Kombucha lasercut")

![](../images/week06/lucile-haute_kombucha-tartan.jpg "Kombucha tartan with dyes.")

### Precious Peels
[Precious peels](https://www.instagram.com/precious_peels_/) is a project by Loumi Le Floch' where she explores eggplant skins as biomaterials. I really like the different colors she obtains by adding lemon to modify the colors.

::: instagram CqlD5c3jVv5

::: instagram Cn431vWNqvh

::: instagram ClL3GBejm5L

### Materiom
[Materiom](https://materiom.org/) is a contributive database of biomaterials recipes. You can create an account and add your recipes, fork/modify existing recipes, comment, etc.

![](https://materiom.org/images/22/screen_2.png)

![](https://materiom.org/images/spacerSquare.png)

### Hacking Silk
Marie Vihmar ([Hacking Silk](https://www.instagram.com/hacking_silk/)) is producing new materials out of silk.



::: instagram B0B6SBHhrF0

::: instagram B3kg6cEBHWs

::: instagram B4NuHAEhloU


### Kelp shoes by Jing-cai Liu

<https://www.futurematerialsbank.com/material/kelp/>

![](https://www.futurematerialsbank.com/wp-content/uploads/shoes-1000x665.jpg "Kelp shoes by  Jing-cai Liu")

### Lichens growing on knits

[Mirte Musje](https://www.instagram.com/mirtemusje/) made her final project at Swedish School of Textile on lichens growing on knits.


::: instagram Cd8Wz8YoOu9

::: instagram CdV60V6I-Sk


### Lichen bioplastic
[Lichen bioplastic](https://www.mat-wise.com/lichen-bioplastic)

![](https://static.wixstatic.com/media/63ae2d_9a9c19128ac74a6088295e1fa576d97b~mv2.png/v1/fill/w_795,h_795,al_c,q_90,usm_0.66_1.00_0.01,enc_auto/LinkedIn%20Post%201x1.png)

![](https://static.wixstatic.com/media/63ae2d_e318b6d4f2704e71ba0fa2dbb741da9c~mv2.jpg/v1/fit/w_2262,h_1272,q_90/63ae2d_e318b6d4f2704e71ba0fa2dbb741da9c~mv2.webp)

![](https://static.wixstatic.com/media/63ae2d_522025658aa64e76b2fc1fe8ce72bbc5~mv2.jpg/v1/crop/x_0,y_54,w_1536,h_1336/fill/w_600,h_522,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/P_%20Ca%C3%ADdo.jpg)

I love the warning when you arrive on the website:

> Lichen bioplastic was just a one-shot research.
> 
> It’s not sustainable to use it since the growth of the specie takes long and in very specific places. I used the leftovers of products that were already on the market so I would not interfere with the environment :)
>
> Nevertheless, if you have this under control then, I encourage you to do more research. 





### Paper yarn / *Kami-ito*

Paper has been used traditionnally in Japan to make clothes that are washable. It was a less expensive alternative to cotton. They generally use mulberry paper. 

[Sabrina Sachino Niebler](https://www.instagram.com/sabrinasachiko/) describes wonderfully her [process](https://www.sabrinasachiko.com/the-craft). She makes beautiful *shifu* (cloth made of *kami ito*) that she sometimes dye with natural ingredients.

::: instagram CAnRMXppOpY

::: instagram B2DhLLhp33N

* * *

Since I discovered this practice, I wanted to try it out myself in order to recycle paper waste, mostly tissue paper or patternmaking paper.
I made myself some tests in the past couple of years with only tissue paper for now. The first post are samples made with a spindle in 2020, the other one was made with an e-spinner in 2022. Both of those samples were intact after washing (of course, I did just a gentle wash for now).

::: instagram CZIWLDetkee

::: instagram ChhpeeYtNCG

* * *

Tests made with the Studio Hilo spinning machine:

![](../images/week06/before/paperyarn-hilo01.jpg "Paper yarn spun on the Studio Hilo automatic spinner.")

![](../images/week06/before/paperyarn-hilo02.jpg "Paperyarn ball, the yarn is lacking wraps. It does not look smooth.")

![](../images/week06/before/paperyarn-hilo03.jpg "Hand-knitted sample. Unwashed.")

![](../images/week06/before/paperyarn-hilo04.jpg "Hand-knitted sample. Washed. Gets broken when washing.")

### Wild textiles

I am deeply interested in using resources that we can find anywhere which are often underestimated. I bought Alice Fox's *Wild Textiles* book last year to have inspirations on many natural resources.

![](https://botanicalcolors.com/wp-content/uploads/2022/09/wildtextiles.webp "*Wild Textiles* book cover.")

Last year, I tried making yarn out of nettle. 

PICTURE

This summer, I found some Creeping Thistle (*cirsium arvense*, *chardons des champs*) at a pollination state on my way to the lab. I thought their little hairs were actually quite long compared to dandelions for example and that they were worth trying to spin. It was quite hell to spin so I only managed to spin that much, but it gives a very soft fabric!

![](../images/week06/before/chardon-des-champs.jpg "Creepint Thistle yarn. Hand carded, spun eew 6.0, hand-knitted.")



### Manioc bioplastic

*This was made before Fabricademy but thought it was interesting to share.*

I've been curious to experiment with biomaterials for quite a while now. My only experiment till now was at the beginning of the year when my partner wanted to do tapioca flour from scratch out of manioc. There were a lot of manioc fibres left out of the process (the tapioca flour was great!) and I thought to give it a random try. I cooked them in a pan with vinegar and with/without glycerol. The ones without glycerol were super hard and broke too easily when dried. The ones with the glycerol are still today quite supple (check video). Lasercut worked quite nicely on this material.

![](../images/week06/before/manioc.jpg "Manioc bioplastics.")


<iframe title="Bioplastic manioc" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/6a08b987-0713-4d6e-a5a1-5cd5b4ab1eb8" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

For preparing I Love Science festival (see below), I wanted to try bioplastics from tapioca flour directly. The two attempts I made ended up in super sticky mixtures which were too difficult to spread and never dried. **FAIL.**







# Gelatin bioplastic

Two weeks ago, I gave 30 minutes workshops during three days at the I Love Science Festival (this is why I am late for documenting week 4 Biochromes!). To prepare this, I've been testing some recipes every now and then for a month. I used Anastasia Pistofidou's tricks of using embroidery hoops as molds to have nicely shaped samples.


![](../images/week06/ilovescience/dryer-bobbinholder.jpg "Using a bobbin holder as a dry rack.")

![](../images/week06/ilovescience/dryer-knittingmachine.jpg "Using a knitting machine as a dry rack.")


I tested [recipes from Le Textile Lab](https://letextilelab.com/produit/cookbook-biomateriaux/) in Lyon:

- agar-agar 
- silicone gelatin
- resin gelatin
- composite gelatin

## Agar-agar bioplastic sheets

This recipe gives a rather soft and elastic sheet. It is still quite sticky. When you make a composite by adding a filler, it looks quite fragile.

??? Ingredients
    - 250mL water
    - 5g agar-agar
    - 15g glycerol

??? Recipe
    - weigh all ingredients in a pot.
    - simmer it on low heat until it thickens.
    - pour it in mold.

![](../images/week06/ilovescience/agar-agar.jpg "First attempts at agar agar.")

![](../images/week06/agar-agar.jpg "Sample sheet.")

!!! Tips/Remarks

    - Cook the mixture well so that it thickens in the pot before pouring it, otherwise it will never dry!
    - If you pour onto a fabric, put a plastic underneath.
    - It shrinks quite a lot and does not stay in the embroidery hoop. I suggest to make a big surface and then cut off the outline.



## Gelatin bioplastic sheets

??? Ingredients
    - 240mL water
    - 48g gelatin
    - (gelatin ratio) glycerol

According to the amount of glycerol, you obtain a plastic which is super soft and sticky (more glycerol) or a plastic which is hard and slick (less glycerol). They all have different usage, you need to find one ratio that suits your needs.

## Gelatin silicone

**Gelatin/Glycerol ratio: 1:1**

As its name suggests, this recipe mimicks silicone matter. It is very soft, squishy, elastic and sticky. It could be used to replace rubber usage. It is still sticky even when fully dried. This stickyness can be seen as a feature and can be used to replace rubber/silicone usage. I used them as spacers in [week 2 Digital Bodies](./week02.md). After a lot of manipulation, it can lose its stickiness and translucency, you can clean it with a damp towel and it will be good as new.

![](../images/week06/gelatine-silicone01.jpg "From top left to bottom right. 1. Flower seeds. 2. Coloured pencil dust. 3. eggshells. 4. coloured pencils. 5. paper confettis. 6. machine knit.")

![](../images/week06/gelatine-silicone02.jpg "From top left to bottom right. 1. On water-repellent fabric. 2. On “sandwich” bubble wrap. 3. Madder root dye waste. 4. Bubble wrap. 5. Purple chalk. 6. Green chalk.")

![](../images/week06/gelatine-silicone03.jpg "From top left to bottom right. 1. Curcuma. 2. Food coloring [got stolen]. 3. coffee grinds.")

![](../images/week06/coloured-pencils.jpg "Coloured pencil dust. One of my favorite.")

Bioplastic poured on a "sandwich" bubble wrap: a bubble wrap having a flat layer on each side. It makes nice facettes.

<iframe title="Bioplastic poured on &quot;sandwich&quot; bubble wrap" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/8fe11c81-aa32-49b7-bee3-1e34d637c21b" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

Bioplastic poured on machine knit sample.
<iframe title="Bioplastic on machine knit" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/0e73196c-ff03-4fd0-9168-f64c6bf4c4a1" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## Gelatin resin

**Gelatin/Glycerol ratio: 6:1**


![](../images/week06/gelatin-resin.jpg "From top left to bottom right. 1. natural. 2. “sandwich” bubble wrap. 3. bubble wrap. 4. coffee grinds. 5. paper confettis. 6. eggshells.")



## Gelatin composite
**Gelatin/Glycerol ratio: 3:1**

![](../images/week06/gelatin-composite.jpg "From top left to bottom right. 1. Natural. 2. chemical indigo spots. 3. bubble wrap. 4. chemical indigo. 5. yarn waste. 6. sewing thread waste.")


## Festival I Love Science

For the festival, I decided to use the Gelatin resin recipe as it gave a non-sticky result. Also because if kids would put too much glycerol, it would still give a nice result.

![](../images/week06/ilovescience/ilovescience-setup.jpg "Workshop setup.")

![](../images/week06/ilovescience/ilovescience-table.jpg "Workshop setup.")

![](../images/week06/ilovescience/ilovescience-kids.jpg "Young participants.")

![](../images/week06/ilovescience/ilovescience-wall.jpg "Drying the participants bioplastics.")


::: instagram Cyc1FSaNakL


* * *

## This week's experiments

### Lasercut

Beware of the smell of burnt gelatin, it is horrible…

![](../images/week06/lasercut01.jpg "A kid at I Love Science festival suggested to make key rings.")

![](../images/week06/lasercut02.jpg "I wanted to make one for my kid's birthday, but the edges were not very nice.")

![](../images/week06/lasercut03.jpg "Probably because the power/speed of the laser was too strong.")

![](../images/week06/lasercut04.jpg "So I made a matrix to spot the nicest settings as suggested by Charlotte.")

Engraving works super nicely and it doesn't even smell that horrible.

<iframe title="Laser engraved bioplastic" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/0dedd151-dd8f-49ca-8be8-9163e784c1ec" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>


### New fillers

I took a bit of time to test fillers I didn't have time to test before. This time I wanted something more suple than the gelatin resin recipe upper in the page.

**Gelatin/Glycerin ratio 2:1**


![](../images/week06/gelatin-glycerin_2to1-01.jpg "From top left to bottom right. 1. Coloured chalks. 2. Food colouring + repair. 3. Golden glitter. 4. Pink glitter. 5. Natural mica. 6. Fish scales.")

![](../images/week06/gelatin-glycerin_2to1-02.jpg "From top left to bottom right. 1. Chemical indigo. 2. Bubble wrap + chemical indigo. 3. Flower seeds. 4. Indigo + laser engraved. 5. Natural indigo waste.")


### Glitter

Who doesn't love glitter? I know they're not sustainable at all, but I did buy those golden and pink glitter at the store where we bought all Fabricademy products. I just couldn't resist… I do plan to use them only for unwashed objects though.


![](../images/week06/glitter-gold.jpg "Golden glitter.")

![](../images/week06/glitter-pink.jpg "Fluo pink glitter.")



### Mica
As an alternative to plastic glitter, I wanted to try natural glitter: mica. 

Here is a natural mica that I found this summer during my vacation in Auvergne mountains. It was actually quite difficult to turn it into a powder, the mica sheets are really like natural plastic! The mortar/pestle would not do much to it. I broke them with my fingers is small pieces. I think a blender would have worked better, but I just wanted to test a tiny amount for now and it was not sufficient for a blender.

![](../images/week06/mica02.jpg "Mica found in the mountains.")

![](../images/week06/mica03.jpg "Mica in bioplastic.")

<iframe title="Mica bioplastic" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/1e12854f-182e-4794-820e-23683499ed23" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

#### Fish scales


Sea bream scales.

<iframe title="Fish scales bioplastic" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/a08bd35f-6888-4835-83c7-75bbd33b4580" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>


#### Natural indigo waste

The natural dyer of Green Fabric, Amandine Brun-Sauvant often removes the top layer of her indigo vat. She kept it to see if it would make an interesting bioplastic. I made it into a powder again with a mortar/pestle but it did not dissolve very well in the bioplastic. Amandine suggests for next time to rehydrate it just like when you launch a vat, and use this coloured paste/water to make the bioplastic.

![](../images/week06/natural-indigo.jpg "Natural indigo waste.")

![](../images/week06/natural-indigo2.jpg "In mortar.")
 




### Repair bioplastic
I was wondering if you could repair bioplastics. I took a failed one which was too thin and broke when it dried. I added a few drops of sirupy bioplastic on the whole. The edges melted a bit when I did that. Maybe it's good to wait a little bit that the mixture gets colder to avoid that. But in the end, when dried, the repair is quite ok!

![](../images/week06/gelatin-repair.jpg "Gelatin bioplastic repair with new gelatin bioplastic mixture.")


### Foam gelatin

I made several attempts at making foam bioplastics but it always dephases. The foam is always on top, leaving a big chunk of translucent bioplastic underneath which gets moldy after a few days. I don't understand what's wrong with those…

![](../images/week06/foam2.jpg "Foam in silicone molds.")

![](../images/week06/foam.jpg "Foam on top of the bioplastic.")


### Scale it up

I wanted to make big sheets of bioplastic to be able to make small objects.

![](../images/week06/gelatin-sheet2.jpg "Spread on a oilcloth. Not a good idea! Not flat, not even.")

![](../images/week06/gelatin-sheet3.jpg "The oilcloth sheet broken and dried.")

![](../images/week06/gelatin-sheet.jpg "Spread on an oven metal tray. It's better but not necessarily even everywhere.")

![](../images/week06/gelatin-suminagashi.jpg "Suminagashi with chemical indigo.")

Some tests into making objects by sewing.

![](../images/week06/sewing01.jpg "Hand sewing with wool. Just a regular needle, the plastic is quite soft to sew.")

![](../images/week06/sewing02.jpg "")

![](../images/week06/sewing03.jpg "Hand sewing with cotton. With a flap this time to close the envelope.")


<iframe title="Sewn bioplastic envelope" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/4eac76e4-d850-4505-9bee-7948d484a983" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

A test by melting the bioplastic with a hot needle. This technique would be nice to make temporary sealings, for packaging for example. You can open it up again quite easily.

![](../images/week06/heatseal01.jpg "Sides were joined by sealing with heat.")

![](../images/week06/heatseal02.jpg "")


### Origami

[Dewi Brunet](https://www.instagram.com/dewibrunet/), a friend artist specialized in origami, has been exploring biomaterials as well under the name [Flowd.lab](https://www.instagram.com/flowd.lab/). We met on Monday to show ourselves with [Charlotte](https://class.textile-academy.org/2024/charlotte-jacob/) and [Eileen](https://class.textile-academy.org/2024/eileen-schreyers/) our respective experiments. I had a thin sheet of gelatin bioplastic which was holding the fold quite well and asked Dewi to fold it. He tried the bioplastic on the [open source pleating loom](https://github.com/DewiBrunet/Plissage/blob/master/Pleating%20loom.md) that he developed.

![](../images/week06/dewi-origami01.jpg "Dewi's gelatin bioplastic experiment. He says the folds don't stay after a while.")

![](../images/week06/dewi-origami02.jpg "Dewi making the creases on my bioplastic sheet using his open source pleating loom.")

![](../images/week06/dewi-origami03.jpg "Dewi folding the bioplastic.")

![](../images/week06/dewi-origami04.jpg "Dewi still folding while standing [he loves to make origamis in public transport!]")

![](../images/week06/dewi-origami.gif "Dewi still folding while standing [he loves to make origamis in public transport!]")

![](../images/week06/dewi-origami07.jpg "Folded bioplastic. The folds are quite resistant, but the plastic broke a bit at some locations. Probably a tiny bit more gelatin would prevent that.")



# Sodium Alginate

I am obsessed with yarns so I wanted to make sodium alginate yarn when I saw [Loes Bogers super strong yarn](https://class.textile-academy.org/2020/loes.bogers/files/recipes/alginatestring/).

??? ingredients
    - 6g sodium alginate
    - 10g glycerol
    - 200mL water
    - 5g olive oil
    - 20g calcium chloride
    - 200mL water

??? recipe
    - Mix all ingredients with a handheld mixer otherwise there are big lumps.
    - Let it sit overnight (in the fridge?).
    - Prepare your calcium chloride solution by dissolving it into water (10% concentration).
    - Put the sodium alginate mix into a syringe.
    - Extrude in the calcium chloride solution.

My first attempt was a fail as I had too much of the gelatin recipe in mind, and I was convinced that there would be more sodium alginate than glycerol, but it is the other way around… I also forgot the oil. I ended up with very chunky fatty strings. I figured that after half an hour, my mixture left inattended completely solidified.

Second try, I did the proportions good, but I ended up with quite fragile strings and pearls. Maybe my hole in my plastic bag (DIY syringe) was too small… I then noticed that you should normally let sit the mixture overnight for the bubbles to go away… 

Third try: good proportions, left it overnight on the table. When I came back in the morning, I found a cheese/tofu sort of thingie… I got discouraged by the sodium alginate so I abandoned this path for now. I did use the little bubbles as pearls. The "cheese" is still like a flan after 6 days, let's see how it evolves. I saw on another recipe that they let it sit overnight in the fridge. Does this make a difference?…


![](../images/week06/alginate/syringe.jpg "DIY syringe.")

![](../images/week06/alginate/alginate-vermicellis.jpg "Sodium alginate vermicellis.")

![](../images/week06/alginate/sodium-alginate-tofus.jpg "Sodium alginate tofus.")

![](../images/week06/alginate/alginate-pearls.jpg "Sodium alginate pearls.")

<iframe title="Sodium alginate mixture fail" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/72f49160-c5f1-4718-be5e-e18b6bc9249c" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>



# Algae

Alert: this is a totally unscientific and completely arbitrary process and experimentations… It went a bit all over the place, but it was fun!

??? Links
    - <https://www.fao.org/3/y4765e/y4765e08.htm>
    - <https://www.iaacblog.com/programs/amber-laminaria-seaweed-biocomposite-material/>
    - <https://class.textile-academy.org/2023/alberte-bojesen/SEAWEED%20DIALOGUES/01%20%7C%20SEAWEED%20DIALOGUES/>

I ate some oysters this week so I kept the algaes which were with it. I let them sit in water for a day to remove the salt, then a second bath of a couple of hours. I simmered them a few hours. 

## Drying

I wanted to use the algaes as materials itself. I wondered if I could find a way to make them more supple. I tried putting it in sodium carbonate, in vinegar, in both. 

![](../images/week06/algae/algae-boil.jpg "Simmering algaes (beware of the smell!.")

![](../images/week06/algae/algae-juice.jpg "Algae simmering water. After 5 days, it is a bit slippery and developed a slippery biofilm at the top.")

![](../images/week06/algae/algae-sodiumCarbonate_vinegar.jpg "In sodium carbonate and vinegar. It makes CO2 bubbles as a regular acid/basic reaction.")


In vinegar
:    Still hard. 
:    Gets a green blueish color.

In sodium carbonate
:    After a while, the algae becomes soft and disintegrates when you press it between your fingers


In Sodium Carbonate then vinegar
:    It fuzzes for a while (acid + alkalin makes this fuzzy reaction and delivers CO2).
:    After 5 days, the algae is not soft at all


## Precipitate

I tried to spray calcium chloride onto my different juices to see if I got a reaction. It did make some kind of whitish precipitation right away, without really solidifying ever. There was absolutely no reaction from the vinegary juices, so there is definitely a reaction with sodium carbonate going on. As it is not solidifying, I tried to filter those precipitates to see if I could exploit them afterwards. To be continued…

<iframe title="Algae precipitate" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/19f12272-83ba-49ed-b174-e3a824fbc36c" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

![](../images/week06/algae/precipitate.jpg "Precipitate with calcium chloride. 1: sodium carbonate [5 days old]. 2: algae juice + glycerol. 3: sodium carnonate. 4: sodium carbonate + glycerol. 5: vinegar [5 days old]. 6: sodium carbonate + vinegar [5 days old].")




![](../images/week06/algae/algae-drying.jpg "Drying algae leaves. 1: glycerol simmered + glycerol applied after. 2: vinegar bath + glycerol applied after. 3: vinegar bath. 4: precipitate filtered. 5: glycerol simmered.")


## Extracting colors

The green given by the vinegar is quite intense and I thought it would be nice to exploit it. I made a test of putting algaes in ethanol 96% to extract colors. 

![](../images/week06/algae/algae-ethanol.jpg "In pure ethanol to extract color. Left regular algae, right algae bathed in vinegar.")








# Fish leather

My mom wanted to eat some sushi this week-end and bought an enormous piece of salmon. I asked the fishmonger to remove the skin but to keep it. I removed the scales with a handmade tool that I bought a few years ago in Thailand or Laos. I then removed the few left scales and flesh with a chef knife. I didn't have ethanol at 96% at home, so I froze the skin to wait till Monday to do it at the lab. 

I tested the so-called "lazy" recipe (this is just perfect for me!) that Cecilia Raspantes gave during the course: one part glycerine, one part ethanol. Shake 5-20 times a day during 3 days. 


![](../images/week06/fishskin/fishskin.jpg "Salmon fish skin in ethanol and glycerol.")








# Grown materials

## Flowers

This is not really a grown material, but I wanted to check if my gelatin resin with flower seeds would actually make flowers. So I put one of my samples in water, to see how much time it would take to dissolve and germ.

After a day, the plastic was soft but still present!


![](../images/week06/flowers/flowers02.jpg "I put the bioplastic with flower seeds in water to better see the roots.")

![](../images/week06/flowers/flowers03.jpg "After a couple of hours, the bioplastic became soft.")


## Kombucha

We were given a Kombucha mother on Monday by [Dewi Brunet](https://www.instagram.com/dewibrunet/) who has been exploring with Kombucha leather for 4-5 years. We tested 2 recipes, one with failed homemade beer (white beer but with a brown color) from Fabricademy fellow [Charlotte](https://class.textile-academy.org/2024/charlotte-jacob/) and one with jasmine tea.

We made the new baths on Tuesday 31st October, let's this how this goes…


![](../images/week06/kombucha/kombucha-beer.jpg "Valentine putting the mother of kombucha in homemade failed beer from Charlotte.")



## Sponge blob

??? Links
    - <https://warwick.ac.uk/fac/sci/lifesci/outreach/slimemold/care/>

A blob is a mysterious unicellular thingie that one can find in the forest. It has no nervous systems but it can resolve networking problems nonetheless.
I knew that our friend Dewi Brunet had blobs. I asked him to give us one piece to see if it would make an interesting material…

::: youtube Dwcx6yCdDE8

Dewi says it likes oats from Quaker brand the most. So I bought its fav food and we gave him its first oat flakes on Tuesday 31st October. Let's see what happens…

![](../images/week06/blob/bob-le-blob.jpg "Welcoming Sponge blob to the lab.")

<!--![](../images/week06/blob/bob-le-blob.jpg "First meal for Sponge blob.")-->





<!--##### TEST SERIE BIO-PLASTIC-->

<!--| _Material pic_ | _Material name_ | _polymer_ | _plastifier_ | _filler_ | _emulsifier_ |-->
<!--| -------------- | --------------- | --------- | ------------ | -------- | ------------- |-->
<!--| ![](../images/home/sample-image.jpg){ width=100 } | bio-rainbow | biokelp powder 12 gr | glycerol 100 ml | rainbow dust 1 kg | green soap a drop |-->
<!--| ![](../images/home/sample-image.jpg){ width=100 } | bio-rainbow | biokelp powder 12 gr | glycerol 100 ml | rainbow dust 1 kg | green soap a drop |-->
<!--| ![](../images/home/sample-image.jpg){ width=100 } | bio-rainbow | biokelp powder 12 gr | glycerol 100 ml | rainbow dust 1 kg | green soap a drop |-->
<!--| ![](../images/home/sample-image.jpg){ width=100 } | bio-rainbow | biokelp powder 12 gr | glycerol 100 ml | rainbow dust 1 kg | green soap a drop |-->



