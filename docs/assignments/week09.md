# 9. Soft robotics

## References



### Inflatable bike airbag

![](https://www.yankodesign.com/images/design_news/2019/09/the-invisible-bike-helmet-that-can-take-a-blow/airbaghero.jpg "Yanko Design")





### Samuel Poincloux

Samuel Poincloux is a French researcher in physics. He studied the physical properties of the knit stitch and how it could be used in engineering. The video is in French but his papers are available on the web in English as well.

::: youtube R5nG-F9jP2U

### Saskia Helinska

[Saskia Helinska](https://class.textile-academy.org/2022/saskia-helinska/finalproject.html), Fabricademy 2022

::: youtube yxzaZYXA-3M

### Alberte Bojesen

[Alberte Bojesen’s inflatables](https://class.textile-academy.org/2023/alberte-bojesen/SEAWEED%20DIALOGUES/03%20%7C%20PREVIOUS%20SEAWEED%20RESEARCH/#inflatables), Fabricademy 2023.

![](https://media2.giphy.com/media/18gVDVv7BCAyxMlk6X/200w_s.gif "Alberte Bojesen's seaweed inflatable experiment")

### Elzelinde van Doleweerd

[Elzelinde van Doleweerd’s shape-changing edible material](https://www.linkedin.com/posts/elzelinde-van-doleweerd_i-am-happy-to-share-that-i-recently-graduated-activity-6833411766349824000-j-S-) with the restaurant Alchemist** in Copenhagen.

::: instagram CV6COcSIqhv

### Marion Pinaffo & Raphaël Pluvinage

This duo of artists/engineers is so inspiring. They conceive animation but in the physical space, inventing mechanisms to activate and animate shapes, sounds… I especially love that they replaced electric energy by non-electric energy like sand, wind…

::: instagram BWxcoBYh7_U

::: vimeo 619917586

### Jean-Simon Roch

Jean Simon Roch is an artist working on automated sceneries. I am in love with those earplugs dances:

::: instagram CTgv-D-gJvY

### Étienne Mineur
::: instagram CX06j7NP-iw

opensoftmachines.com mini-extensible-pneumatic

### 3D printed mechanics

::: instagram CxYRKrqPquV



::: instagram CwxvV2fP2NG



### KnitSkin

Machine Knitted Scaled Skin for Locomotion: <https://www.hybridbody.human.cornell.edu/knitskin>

::: youtube iO2jYlqSVoo



### Traditional ways of sealing plastic bags

::: youtube z0ocJ2Podis
::: youtube N4utVVGK3jc



### Aeromorph

<https://tangible.media.mit.edu/project/aeromorph/>

::: vimeo 194560714

### Ana Piñeyro

Ana Piñeyro explores the [kinetic potential of nylon monofilament](https://anapineyro.com/portfolio/animating-matter/). She also explores the [movements of wool](https://anapineyro.com/portfolio/active-wool/).



### Soft Circuit Toolkit

[Soft Circuit Toolkit](https://www.softmodbot.com/) is a breadboard, just like in electronics, but working with air. You can prototype easily air circuits. I find it mind-blowing as it is much easier to understand how a breadboard works with air rather than electronics. 

::: vimeo 833265909



### Open Soft Machines

[Open Soft Machines](https://opensoftmachines.com/) is a blog publishing small tutorials on soft machines. I love its aesthetics and the fun techniques they share.

::: youtube w-vUxVKuCZ4

<!– NOTES FROM CLASS

embroidery + casting

magnets
serpentin
electrolyse
lego pump
balloon


origami cube montessori avec fil qu'on tire 

sablier air/eau

CO2 avec vinaigre + bicarbonate
levure + sugar+ vinegar + beer

solenoid
air pumps
water valve

knit skin
https://www.hybridbody.human.cornell.edu/knitskin

bioplastic sealed with heat

2 feuilles de plastique → découpeuse laser

–>





what's a muscle? an elastic?
contraction+expansion

## Kirigami-inspired gripper

::: youtube zk3pFRu15kU

![](../images/week09/20231115_093855.jpg "I drew on top of a still from the previous video.")

![](../images/week09/20231115_193330.jpg "Paper prototype cut.")

This first gripper was working really great, but after playing with it too much, it broke quite fast. The little articulations are too thin. I made another version with a thicker paper, but as it is too thick (first test in the video below), it has a hard time to bend and grab stuff. I came back to an ordinary A4 paper and made the junctions larger and it works fine again.



<iframe title="soft-gripper-mute" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/85f6f281-1afd-44d9-82fc-9007057ec990" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>



Source file to reproduce: [Kirigami gripper SVG](../files/week09/kirigami-gripper.svg) 



## Plastic inflatables



Inflatable made with an plastic sleeve for paper and hand-stitching. It kind of worked, but probably the material was too stiff and the air could go out too much.



![](../images/week09/20231115_182555.jpg "I drew on top of a still from the previous video.")

<iframe title="farde-stitching" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/21eeb3c8-c4ae-48b4-9cc3-52833a3cfa32" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>



Inflatable made with cellophane (food plastic wrap) and hand-stitching. Here the space between the two sheets is bigger and the material is less stiff, so it worked much better than the previous experiment.


<iframe title="cellophane-stitching" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/b3f935ad-ef6c-4c3d-8381-5d2af989f7ce" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>



Inflatable made with an plastic sleeve for paper and an iron (either for soldering or to iron clothes). Use a parchment paper to protect your tool. 



![](../images/week09/20231120_110156.jpg "I drew on top of a still from the previous video.")



![](../images/week09/20231120_110334.jpg "I drew on top of a still from the previous video.")

<iframe title="farde-iron" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/5019fc9a-6a3a-4830-afd0-1e7b1c584012" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>



## Yarn Mc Kibben muscle

Testing the stretchness of two yarn samples. The top one is knitted with a larger tension than the bottom one and therefore stretches much more.

<iframe title="stretch yarn" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/a699b240-a85b-4a1f-8415-d537a43414f6" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

Attempt at making a muscle out of knits and air pocket. It is not working. Probably the difference in tension between the two layers is not important enough and the muscle did not bend. Thus I added a woven fabric inside to have more tension in the muscle.

![](../images/week09/20231120_110219.jpg "Woven fabric sewn inside the muscle.")



![](../images/week09/20231120_110237.jpg "Woven fabric sewn inside the muscle.")



It is still not very convincing…

<iframe title="knit-muscle-fail" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/72d59ab7-1e32-4152-b77a-aaf556b2fa8a" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>



## Nylon muscle

As I love spinning fibers, I was really curious to experiment with nylon monofilament and overtwists it to change its physical properties. Sadly I didn’t have much time to experiment with it. It was rather difficult to find a good method. So far [those are the more helpful explanations](https://www.sciencefriday.com/articles/how-to-make-an-artificial-muscle-out-of-fishing-line/) about the process. 

!!! info "Process"
    - cut a length of nylon monofilament 
    - attach it to the starter of the spinning wheel
    - attach the other end to a stick for easie manipulation
    - start spinning
    - keep the starter thread in front of the wheel
    - you can turn the speed high at the beginning
    - after a short while, lower the speed
    - once you feel it's pulling towards the wheel, slowly go forward with your stick
        - if you don't go fast enough, there will be too much tension and the thread will break
        - if you go too fast, it will do "pigtails"

The documentation I consulted say that once we get our overtwisted thread, we need to "set it", to let the thread memorize its new shape in a way. As they use a drill and a weight, they recommend to add 50% of weight, or to untwist a little but not too much. I tried the latter, but it's difficult to know what's enough and what's "not too much". 

The overtwisted thread didn't really work as a muscle to lift up weight, but it does come back to its overtwisted state when elongated and heated up afterwards. So it's still quite interesting and deserve more tests.

![](../images/week09/2024-05-01-182052_grim.png "Spinning the nylon monofilament with a spinning wheel and a stick to hold it.")


<iframe title="shrink-nylon" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/c3d96497-5735-4122-b4e3-122734a3e620" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>


## Vynil sheets

I made an inflatable with baking paper sandwiched in 2 layers of vynil sticker sheets. I cut the parchment paper by folding it in 4 and cutting triangles. The baking paper is the area where your air will flow.



![](../images/week09/20231120_151804.jpg "Baking paper, folded and cut.")

![](../images/week09/20231120_155306.jpg "Baking paper on top of a vynil sheet, on a heat press, before adding the second vyvil sheet.")



![](../images/week09/diagram-air.jpg "Airflow path.")

<iframe title="vynil-pink" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/278b8c32-1f24-4075-93f6-24334ca0a774" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

<iframe title="vynil-blue" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/03afd788-ecf4-4dc5-9ec1-d5ff0adb2ff9" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
