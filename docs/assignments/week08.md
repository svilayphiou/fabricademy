# 8. Wearables


<!--A speaker converts electrical signals to sound pressure signals.-->
<!--A transducer converts sound pressure signals to electrical signals.-->


## References & Inspirations

### Wendy Van Wynsberghe

[Wendy Van Wynsberghe](http://wvw.collectifs.net) is a Flemish artist and friend. She's working mostly on e-textile and sound for interactive installations. Her piece *Kissing protocols* explores the way people great themselves in different cultures: visitors of the piece could wear masks, gloves, etc. and interact with each other. Her piece [*Ultrasonic*](https://www.wvanw.space/ultrasonic/) is a batman cape to hear ultrasounds from bats.

![](https://kunstenpunt-datagids-data.imgix.net/uploaded_files/files/000/018/424/original/14174.jpg?w=1096&h=832&fit=max&auto=format&q=60&bg=000000 "Kissing protocol.")

![](https://www.wvanw.space/wp-content/uploads/2017/01/Cape-Wendy-2-768x576.jpg "Ultrasonic cape.")


### Fractal antennae by Afroditi Psarra

[Fractal antennae](https://afroditipsarra.com/work/fractal-antennae)

![](https://images.squarespace-cdn.com/content/v1/603e5c69e5ece05692d9e43b/f5c61bf5-d2c4-4f4f-b039-06caab696720/fractalKimono_2.JPG?format=1500w)

![](https://images.squarespace-cdn.com/content/v1/603e5c69e5ece05692d9e43b/ebdf31ff-6ca0-4e7e-a588-dc8b27634f41/EST_7784.jpeg?format=1500w)

![](https://images.squarespace-cdn.com/content/v1/603e5c69e5ece05692d9e43b/dfc4d965-6383-4872-99da-a1daa69f06e9/IMG_4439.jpeg?format=1500w)



### The knitted radio by Ebru Kurbak
"[The Knitted Radio](https://ebrukurbak.net/the-knitted-radio/) is a sweater that is also an FM radio transmitter allowing a multiplicity of voices to be disseminated in the public space." It is proposed as a solution for protests in places with autocratic authorities.

![](https://ebrukurbak.net/wp-content/uploads/2014/06/knittedRadio_web_Page_19_s-1024x686.jpg "Knitted radio.")

Ebru Kurbak made really helpful knit diagrams to reproduce electronic components with knitted structures:

![](https://ebrukurbak.net/wp-content/uploads/2014/06/knittedRadio_web_Page_13-1024x685.jpg "Resistor.")

![](https://ebrukurbak.net/wp-content/uploads/2014/06/knittedRadio_web_Page_14_s-1024x685.jpg "Capacitor.")

![](https://ebrukurbak.net/wp-content/uploads/2014/06/knittedRadio_web_Page_15_s-1024x685.jpg "Coil.")


## Flip dots
[Flip-dot fabrics on Kobakant](https://www.kobakant.at/DIY/?p=5878)

![](https://farm4.staticflickr.com/3919/14928470922_baa04465a9_b.jpg "Igne Oyasi Motor")



### DIY magnetic pearl

[Jessica Stanley](https://class.textile-academy.org/2019/jessica.stanley/assignments/week09/#making-magnetic-beads-a-second-more-successfull-attempt) made magnetic pearls out of a neodymium magnet and hardened foam.

![](https://gitlab.fabcloud.org/academany/fabricademy/2019/students/jessica.stanley/raw/master/docs/images/week9/making_magnetic_ball.jpg "DIY magnetic bead by Jessica Stanley.")







## Mosfet circuit

Part of the assignment was to build a transistor (preferably mosfet N-channel) to always have it readily-available. If you're using an NPN transistor, you might need to add an extra resistor. To know what type of transistor you have, you have to type its number on a search engine and look at its datasheet.

A transistor acts like a resistor according to how much voltage you put it. It is useful when you want to use an electric component asking for a lot of current (such a component is called a power load). An arduino board outputs 20mA current and 5V voltage. Here we plug an additional battery at the top of the circuit, and the power load at the bottom. Make sure to avoid contact between the two lines crossing under the transistor to avoid short circuit. The Arduino code used here is just the basic LED blinking example.

As I didn't have copper tape, I made it on a plain ribbing sample with silver thread. I thought it would then be easier to use for a potential wearable project.

![](../images/week08/mosfet-diagram.png "Transistor diagram from Emma Pareschi.")

![](../images/week08/mosfet-circuit.jpg "Mosfet circuit on machine knitted fabric.")

<iframe title="transistor-vibration-motor" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/c71129b7-3d9a-4649-94fa-93901b7815cb" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>



## Actuator 1: flip dot

??? Refs
    - [Motor on a string](https://www.kobakant.at/DIY/?p=5185)
    - (Flapping wings](https://www.kobakant.at/DIY/?p=8058) + [DIY](https://www.kobakant.at/DIY/?p=5900)

I find the magnetic beads flip dots really beautiful and was curious to make my own coil or motor. I tried with what I had at the lab, and I already knew it would not work as the thread is way too thick and I was able to do only very few circles with it.

![](../images/week08/coil-bigfail.jpg "Failed coil because the thread thickness is too big.")

I tried again with enameled copper wire stolen from my daughter's pearls kit. I did 60 rounds and it worked! Although, to be honest, it is not working every time, I'm not sure why. My 9V battery is now at 7V, maybe I don't have enough voltage? Note that I don't have two-ended crocodile jumpers, so I use a breadboard, but you can connect directly the battery to the coil.

<iframe title="coil-magnet-flip-mute" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/431954a7-5db2-4749-adc2-c0fc5f6d3f39" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>


<iframe title="transistor-magnet-mute" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/138b11ac-13b7-404a-b2a1-58563c9cfa54" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>


Then I thought it would be nice to have the Arduino code to make it flip over and over like the blinking LED, but it is not as easy as it sounds. I figured I'd need to build an [H-bridge](https://www.kobakant.at/DIY/?p=8059) to be able to pass the current from point A to B, then from point B to A. I put the documentation here for later.

![](https://live.staticflickr.com/65535/49346675701_24714d4948.jpg "Kobakant's H-bridge for flicking flip dots.")

![](https://live.staticflickr.com/65535/49346718556_1e8d489251.jpg "Kobakant's H-bridge electric diagram.")

![](https://www.instructables.com/H-Bridge-Motor-Driver-for-Arduino-Using-Transistor/ "H-bridge electric diagram connected to Arduino from Instructables. https://www.instructables.com/H-Bridge-Motor-Driver-for-Arduino-Using-Transistor/")


## Actuator 2 + wearable: Air piano/flute

??? Refs
    - [Breathing belt](https://www.kobakant.at/DIY/?p=8171)
    - [Touch each finger with the thumb to play a note.](https://www.instructables.com/Piano-Glove/)
    - [Musical glove where you turn your hand to play music](https://learn.adafruit.com/cpx-musical-glove?view=all)


    ![](https://content.instructables.com/FC7/ZRFV/LD20H19W/FC7ZRFVLD20H19W.png?auto=webp&frame=1&width=565&height=1024&fit=bounds&md=1f05148fad30cb8d91a77843801cc194)


!!! example "Tools"

	- [Arduino UNO](http://class.textile-academy.org)
	- [Arduino IDE](http://class.textile-academy.org)
    - Not-so-conductive yarn
    - conductive thread
    - crochet hook
    - glove
    - Adafruit Flora
    - powerbank (or battery holder)
    - sewing needle
    - sewing thread
    - speaker

I wanted to use the stretch sensor that I made during [week 2 around e-textile](./week02.md). I first thought of making a sensor to check if one was sitting with a straight back as my partner and I often have back pain. But I thought it would be quite complicated to know exactly where to put the sensors without medical education… 

So I thought to use the sensors on the hand, like I already thought during week 2. I wanted to make an air piano: to play piano notes while just tapping virtual keys in the air.

The first prototype using a refurbished speaker on a breadboard was quite straight-forward and I didn't take pictures of that… I first tested the sketch from this [tutorial](https://www.hackster.io/blackpanda856/play-music-using-arduino-uno-and-a-speaker-b94e4a). Then I removed the melody from the original code and retrieved [here the frequencies of piano keys](https://learn.digilentinc.com/Documents/400).

![](https://learn.digilentinc.com/Documents/Topics/cK-programming/Music-theory/OctaveFreqKeys.svg "Musical notes frequencies.")

![](../images/week08/glove-sewing.jpg "Glove sewing directly on my hand.")

![](../images/week08/glove-inside-magnet.jpg "Sewing magnets inside the glove to hold the speaker.")



### Using Adafruit Flora

??? Install the Adafruit boards in Arduino IDE
    - Go to File/Preferences
    - Add this "https://adafruit.github.io/arduino-board-index/package_adafruit_index.json" in the field "Additional Boards Manager URLs"
    - Go to Tools/Boards Manager
    - Search for "AVR" in the search field
    - Install "Arduino AVR Boards" and "Adafruit AVR boards"
    - On Linux, you might need to do this is the terminal: `sudo chmod 777 /dev/ttyACM0` or `sudo chmod 777 /dev/ttyUSB0`
    - extra: on (Arch) Linux, if you still have an error ressembling `Permission denied on "/dev/ttyACM0"`, you might need to install the `adafruit-boards-udev` package with your package manager.

The Flora board has much less pinouts than the classical Arduino Uno. Here's a picture to show the correspondances:

![](https://cdn-learn.adafruit.com/assets/assets/000/002/845/original/flora_pinout.png?1396787706 "Flora and Arduino pinouts correspondances.")


I need one pin for the speaker, one pin to trigger the sensors, so there are only two pins left for the stretch sensors, meaning I can only connect 2 fingers. For a prototype that's fine, but if we want to use all fingers and both hands, we would need more pins… As for battery, I only found a 5V powerbank which is quite big…

![](../images/week08/glove-prototyping.jpg "Prototyping the glove with a Flora board.")

<iframe title="glove-computer" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/ecdc21fe-f6bb-495f-895f-7d34acfd2904" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

<iframe title="glove-powerbank" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/b70dbbb0-4791-4e0d-8d97-5bfe7cef2bd0" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>


Here, [Eileen](https://class.textile-academy.org/2024/eileen-schreyers/) my fabricademy fellow, is testing the glove:

<iframe title="glove-eileen" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/bb82665d-42da-4cc4-9e5a-025e7ddfe743" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

??? Air piano code

As I use only 2 fingers, I have commented lines concerning the other 3 fingers.

    ```
    // PIN NUMBERS VARIABLES
    int pinSpeaker = 10;
    //int fingerThumb = A1;
    int fingerIndex = A11;
    int fingerMiddle = A7;
    //int fingerRing = A3;
    //int fingerPinky = A4;
    
    int trigPin = 9;
    
    
    // INIT STRETCH VALUES FOR EACH FINGER
    int stretch = 0;
    int stretch1 = 0;
    int stretch2 = 0;
    int stretch3 = 0;
    int stretch4 = 0;
    
    
    // NOTES IN HERTZ
    float DO = 261.63;
    float RE = 293.66;
    float MI = 329.63;
    float FA = 349.23;
    float SOL = 392.0;
    float LA = 440.0;
    float SI = 493.88;
    float DO2 = 523.25;
    
    
    void setup() {
      // INIT SERIAL COMMUNICATION SPEED WITH THE BOARD
      Serial.begin(9600);
    
      // INIT INPUT PINS
      pinMode(fingerIndex, INPUT_PULLUP);
      //pinMode(fingerThumb, INPUT_PULLUP);
      pinMode(fingerMiddle, INPUT_PULLUP);
      //pinMode(fingerRing, INPUT_PULLUP);
      //pinMode(fingerPinky, INPUT_PULLUP);
    
      // init Output pins
      pinMode(trigPin, OUTPUT);
    
      pinMode(pinSpeaker, OUTPUT);
    
      tone(pinSpeaker, DO, 1000); // pinNumber, tone, duration;
      delay(1000);
      tone(pinSpeaker, RE, 1000); // pinNumber, tone, duration;
      delay(1000);
      tone(pinSpeaker, DO, 1000); // pinNumber, tone, duration;
    
    
    }
    
    // CUSTOM FUNCTION TO REPEAT A PART OF CODE WITH CHANGING PARAMETERS
    //
    // DON'T FORGET TO SPECIFY THE TYPE OF PARAMETERS IN THE FUNCTION DECLARATION
    // OTHERWISE THERE'S A "variable or field declared void" ERROR
    //
    void finger(int stretch, float note) {
        if(stretch > 2) {
          tone(pinSpeaker, note, 1000); // pinNumber, tone, duration;
    
      }
    }
    
    
    
    
    void loop() {
      digitalWrite(trigPin, LOW);
      delayMicroseconds(5);
      digitalWrite(trigPin, HIGH);
      delayMicroseconds(10);
      digitalWrite(trigPin, LOW);
    
      // DETECTS WHEN THE SENSOR IS BEING ACTIVATED
      // RETURNS THE DURATION WHEN GOING FROM HIGH TO LOW
      stretch = pulseIn(fingerIndex, HIGH);
      //stretch1 = pulseIn(fingerThumb, HIGH);
      stretch2 = pulseIn(fingerMiddle, HIGH);
      //stretch3 = pulseIn(fingerRing, HIGH);
      //stretch4 = pulseIn(fingerPinky, HIGH);
    
      // CALLING CUSTOM FUNCTION NAMED "finger"
      finger(stretch, DO);
    
      //finger(stretch1, RE);
      finger(stretch2, MI);
      //finger(stretch3, FA);
      //finger(stretch4, SOL);
    
      // CHECK VALUES IN SERIAL MONITOR
      Serial.println("index");
      Serial.println(stretch);
      Serial.println("stretch middle");
      Serial.println(stretch2);
    
    }
    ```








