# 13. Implications and applications

<!--start-->

## Digital surveillance

The Media Design master I followed at [Piet Zwart Institute](https://pzwart.nl) (now called [Experimental Publishing](https://https://xpub.nl/) was focused on using new media/technologies to observe, criticize our world under socio-economical and political aspects, especially the impacts of digital technologies. During my graphic design practice, I was working a lot with [Constant vzw](https://constantvzw.org), an organization for art & media, cyberfeminism in Brussels. I was working on making visible art projects close to the ones from my Masters degree. I would like to come back to this practice I left too long ago… 

During my master at PZI, [!Mediengruppe Bitnik](https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/), an artist duo, came to lead a workshop on CCTVs. I did not attend that workshop but I've heard about it from fellow students: they built a device which allowed them to walk around the city and see through the eyes of the CCTVs around them. 

![](../images/week13/bitnik-howtohackcctv.png "How to view CCTV's around, from Bitnik CCTV-A Trail of Images project, Fanzine London")

Extending that idea, I would like to make a device to not necessarily see what the cameras seen but rather feel where they are with a discrete device.  


<!--
> Studies of the way CCTV operators identify potential threat or problems have shown that they actively discriminate against races and sexes, targetting and scrutinizing young black men or particular subcultural groups. This discriminatory practice was later integrated in CCTV systems enhanced with facial recognition techniques, to target and track specific individuals.
>
> <footer><https://unpleasant.pravi.me/cctv-enhanced/#more-180></footer>
-->

A garment or wearable accessory that would vibrate in the direction where a CCTV is detected. The walker can then decide to turn his·her face on the opposite side to avoid being seen. It is not a product that I want to sell, it’s a device meant to raise awareness of the massive presence of CCTVs. Therefore its circulation would be mostly through the web and artistic contexts.

The device is mostly thought for walking in the city, but it could actually also be used when going into private spaces (airbnb, hotels…) to detect if a CCTV is hidden somewhere…

who 
:    general audience, probably more art-oriented

what
:    a device to detect CCTVs around

when
:    whenever

where
:    in the city, in private/rental spaces

why
:    to raise awareness of digital surveillance

how
:    a wearable and discrete device with multiple vibration motors which will vibrate in the direction of a CCTV

![](../images/week13/miro.png "Miro board, loopholes exercise")

### Ideas to extend the project 

Insecam feature
:    On top of sensing CCTV, with the help of a GPS, the device could also tell the walker that the CCTV is available on Insecam, a website giving public access to unprotected CCTVs.

Performance
:    If several persons walking together are wearing the CCTV sensing device, maybe a form of choreography would emerge from it. 

Mapping
:    If I have time, the detected CCTV position could be added to a map available publicly on the web. 

Camouflage
:    If I have time, I would like to work on patterns for knitted garments/accessories to be invisible to human recognition systems (visuals, heat…) 

<!-- un textile comme les trams recouverts de pub: on peut voir de l'intérieur mais on il y a une image de l'autre côté -->




??? "Resources and tools"
    - [Face detection browser with openCV](https://github.com/fleshgordo/face-detection-browser-opencv) by Gordan Savičić
    - [insecam](http://insecam.org/): a website where one can watch unprotected CCTVs around the world
    - [autoknit](https://github.com/textiles-lab/autoknit): software to convert a 3D object into 3D knit instructions for industrial knitting machines, including Kniterate
    - [Bitnik, CCTV-A Trail of Images, Fanzine London, 2009](https://2016.bitnik.org/media/c/fanzine_london.pdf)





## References around CCTVs


### !Mediengruppe Bitnik 

[**CCTV—A Trail of Images**, 2008—2014](https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/c)

> From 2008 to 2014, !Mediengruppe Bitnik conducted walks through the sceneries of invisible cities: equipped with self-built CCTV video signal receivers and video recording devices they organise dérives in search of hidden — and usually invisible — surveillance camera signals in public space. Surveillance becomes sousveillance: the self-built tools provide access to “surveillance from above” by capturing and displaying CCTV signals, thus making them visible and recordable.
>
> <footer><https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/c></footer>

![](https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/media/pages/projects/c/230d6c22e6-1688050534/derive-frankfurt-2012-01-1500x.webp "CCTV-A Trail of Images, !Mediengruppe Bitnik. Here we see on the portable screen the images that CCTVs around are capturing.")



[**Militärstrasse 105**, Hijacking Police Cameras, 2009](https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/m)

> Zurich. In 2009, !Mediengruppe Bitnik hijacked the signals from two police surveillance cameras and rebroadcast them to the exhibition space.
>
> <footer><https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/m></footer>

![](https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/media/pages/projects/m/7a19d300fe-1690899324/substitu-001-1600-1400x1400-q70.webp "Militärstrasse 105, !Mediengruppe Bitnik. ")

[**Surveillance Chess**, Hijacking CCTV Cameras in London, 2012](https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/s)

> London. 2012. On the brink of the Olympic Games. A tube station in one of the most surveilled public spaces in the world. !Mediengruppe Bitnik intercepts the signal of a surveillance camera: business people making their way to the Underground, a man in a suit looking for the right exit. From the left, a woman with a yellow suitcase walks into the frame of the surveillance camera. She opens her suitcase and activates a switch.
> This is the moment when Bitnik takes over. The surveillance image drops out, a chess board appears on the surveillance monitor and a voice from the loudspeakers says: ‘I control your surveillance camera now. I am the one with the yellow suitcase.’ The image jumps back to the woman with the yellow suitcase. Then the image switches to the chess board. ‘How about a game of chess?’, the voice asks. ‘You are white. I am black. Call me or text me to make your move. This is my number: 07582460851.’
>
> <footer><https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/s></footer>

Video: <https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/media/pages/projects/s/cbfc0c8e68-1696412864/surveillance_chess_720p.mp4>

<video class="plyr" crossorigin="" data-poster="https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/media/pages/projects/s/637018e59c-1688052805/surveillance_chess_london_still_1.jpg" src="https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/media/pages/projects/s/cbfc0c8e68-1696412864/surveillance_chess_720p.mp4"><source src="https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/media/pages/projects/s/0ac5430833-1696412782/surveillance_chess_480p.mp4" type="video/mp4" size="480"><source src="https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/media/pages/projects/s/88b251d7e9-1696412810/surveillance_chess_480p.webm" type="video/webm" size="480"><source src="https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/media/pages/projects/s/cbfc0c8e68-1696412864/surveillance_chess_720p.mp4" type="video/mp4" size="720"><source src="https://wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww.bitnik.org/media/pages/projects/s/4ec9941b5e-1696412919/surveillance_chess_720p.webm" type="video/webm" size="720">    </video>


### SpY, Cameras, 2013 

Spanish artist [SpY](https://spy-urbanart.com/) made an art installation in Madrid covering a whole wall with fake CCTVs. I couldn't find back another art installation that I've heard about: in an art museum, a wall of CCTVs actually following the visitor when s·he walks across the room.

![](https://spy-urbanart.com/wp-content/uploads/24-SpY-cameras-crop-2880x1920.jpg "SpY, Cameras, 2013.")



### "Laboratoire de surveillance - football & supporterisme", Théo Hennequin, 2023

![](https://www.theohennequin.com/img/PDCsite_01.png "Laboratoire de surveillance - football & supporterisme, Théo Hennequin")

> The book "Laboratoire de surveillance - football & supporterisme" explores the mechanisms of surveillance, repression and control to which football supporters, Ultras, fervent supporters and protesters are subjected. These measures, which have been tested in stadiums, include policing techniques, facial recognition, group disbanding, administrative bans, proactive judicial laws, arbitrary convictions without trial and police violence. Football stadiums are being used as a surveillance laboratory to experiment with these devices, with repercussions for activism and demonstrations. The case analysis presented in the book focuses on the Parc des Princes in Paris, but the devices are similar in many other stadiums. The visual and textual treatment of the case study is the result of research carried out by a graphic designer. The book highlights the liberticidal risks of these devices in a context of growing social tensions, marked by increasing inequality and discrimination.
> 
> <footer><https://www.theohennequin.com/></footer>




## References around camouflage

### Adam Harvey

[**CV Dazzle**](https://adam.harvey.studio/cvdazzle/)

Make-up and hairstyle based on asymmetry to bypass facial recognition. According to the article «‘Dazzle’ makeup won’t trick facial recognition. Here’s what experts say will», this technique no longer works since 2020. But Harvey stipulates that the same reverse-engineering technique can be applied to find new shapes to bypass current systems. Although this solution makes you invisible for machines, it makes you over-visible for humans.

![](https://adam.harvey.studio/docs/archive/cvdazzle/cvd-look1.jpg "Adam Harvey, CV Dazzle")

[**Hyperface**](https://adam.harvey.studio/hyperface/)

Later on, Harvey worked a textile pattern which could be recognized as faces, thus hiding the wearer's face from the image recognition software. This solution is more efficient than the latter as it makes you less over-visible for humans. 

![](https://adam.harvey.studio/docs/archive/hyperface/hyperface_adam_harvey_hyphen_labs_01.jpg  "Adam Harvey, Hyperface")


[**Camoflash**](https://adam.harvey.studio/camoflash/)

A Clutch that emits a super strong light when a camera flash is detected in order to overexpose the photo.

![](https://adam.harvey.studio/docs/archive/camoflash/anti-paparazzi-clutch.gif "Adam Harvey, Camoflash.")

[**Stealth Wear**](https://adam.harvey.studio/stealth-wear/)

Garments made of silver-plated fabric to avoid thermal recognition from drones.

![](https://adam.harvey.studio/docs/archive/stealth-wear/stealth-wear-scarf1.jpg "Adam Harvey, Stealth Wear.")

### Faking Animals
[Cap_able](https://www.capable.design/) is an Italian design studio working on high-tech products that «open the debate on issues of our present that will shape our future». I find these designs really interesting as they are eccentric but they don't look too weird either, therefore you are not overvisible by humans. 

![](https://www.thisiscolossal.com/wp-content/uploads/2023/01/capable-5.webp "Cap_able, 2023")

### Car prototypes camo

![](../images/week13/car-prototypes-camo.png "My Fabricademy fellow Charlotte Jacob told me about these camouflage for car prototypes to remove as much information possible when they get photo-shot.")

### Addie Wagenknecht, Anonymity, 2007

Artist [Addie Wagenknecht](https://www.placesiveneverbeen.com/) has distributed "anonymity" glasses to people in a city center: these glasses are just simple sunglasses in a rectangular shape. This type of performance is really inspiring for my project here. I really like the simple but yet super efficient strategy.

::: vimeo 89726697





## References around translating digital into tangible

### Gordan Savičić, Constraint City, The pain of everyday life, 2007

Austrian artist [Gordan Savičić](https://pain.yugo.at/) built a strapping vest which would tighten when encountering encrypted wifi signals. The stronger the signal, the tighter the vest.

![](https://www.yugo.at/processing/archive/img/constraint02.jpg "Gordan Savičić, Constraint City.")

<!--end-->

[Click here to see the next steps of the project.](../project/index.md)



## Feedback from zoom session

> "Bring it to your personal movitation, fears in relation to surveilance capitalism. https://dl.acm.org/doi/pdf/10.1145/3461778.3461999
> Explore the possibilities of expression. 
> https://dl.acm.org/doi/10.1145/3563657.3596091
> Being critical and other oportunities"	
>
> 
>
> "Interesting approach to privacy! I think to generate awareness and a performing action - you could explore also community map making, multiple users signaling where these cameras are - this creates a sense of community. (you went towards this at the end of the presentation! ahha:) A challenge with the map - could be that your location also becomes a point of data. Which you may not want.
> I was thinking also about beyond CCTV - do things such as tesla cars for example, google cars or parkign controller cars, as well as google doorbells for opening to the postman at distance etc etc,  all track you :)
> There was an action in Amsterdam, were a large group of people met in the red light district, to see how many people are necessary to activate police action :) 
> the cameras were meant to trigger a response from police when a large group (protest, hooligans, etc) 
> Long time ago, intern made fantastic collection of glasses > iris de vries - this as a general product, there are many of these examples, but this to say - what is your wish? awareness that they are there? also tool creation? empower? give a choice? 
> not sure if you have this kind or maps also there: https://data.amsterdam.nl/data/?center=52.3759464%2C4.8917284&embed=true&lagen=veilov-oovctg%3A1&modus=kaart&pk_vid=65e1c87d9a34096317029971911159d5
>
> "	i was not able to trace back a nice project i saw in https://www.cccb.org/en/projects/file/the-influencers/40706 this festival, about cctv. heat cams are easily accessible and you can get very nice results for knitting patterns. https://www.cccb.org/en/activities/file/the-influencers-2019/232028 also > https://3dprint.com/142948/disney-research-3d-knitting/			
> I definitely think you should go forward with 'The Thingy' as the name for your project. Awareness of surveillance cameras using a vibration motor seems a bit too mundane. Sounds like something that could be bought from Amazon. Your background and idea is compelling and I just feel that a more 'artistic' expression or manifestation of 'awareness' could be arrived at. For example, and maybe this is too extreme...but instead of vibration, maybe the device could pinch the wearer...so the passive surveillance is transformed into active physical pain...not just a tickle from a small motor.  To make the invasiveness of the surveillance into something visceral and shocking.	
>
> "Have you started thinking what form the wearable would take? Is it a scarf, and accessory, how is it integrated? 
>
> I like the camouflage idea, or the fact that it would be integrated directly in the pattern.. Would love to see something with the Kniterate :D"			Very curious about the result	I think it would be super interesting to see it as a performance or as an artistic experiment you do personally or include some community in!
>
> Very excited by the idea of Camouflague! Interesting idea of being 'hidden in plane sight'. I have always found dazzle ships very inspiring which is the idea of confusing the attacker instead of dissapearing 	could be interesting to explore the dialogue between constantly updating softwares and the need from that to constantly change methods - e.g. dazzle makeup doesnt work on current FR systems anymore 	
>
> So so excited about this concept! https://memory.is/all-light-everywhere you might like this documentary about surveillence. 	

