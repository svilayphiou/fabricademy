

# Concept | name of your project

## **5 Ws** who, what, when, where, why
_how is what you will start defining in your process pages_

### _who_
"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."

### _what_
"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."

### _when_

### _where_

### _why_

![](../images/home/sample-image.jpg)


![Image description](../images/home/sample-image.jpg){ width=500 }

Photo by <a href="https://class.textile-academy.org/2020/bela.rofe/">Bela Rofe</a> on <a href="https://class.textile-academy.org/2020/bela.rofe/">Fabricademy 2019-20</a>

??? tip

	check out these project *concepts pages* from Fabricademy participants of previous years.






## **References** projects, research papers, expos, performances etc

_Previous participants form a large body of work. Get inspired by both the way they documented, did research and went in search for personal values, aesthetics and language.
Everything made by anyone can be of reference, any project, research paper, exhibition or showcase, performance, system, service or product._

* Image reference


<figure markdown>
  ![](../images/home/sample-image.jpg)
  <figcaption>Image credits</figcaption>
</figure>

* Download reference

Links to reference files, PDF, booklets,


??? tip

	Remember to credit/reference all your images to their authors. Open source helps us create change faster toegther, but we all deserve recognition for what we make, design, think, develop.





## **Moodboard**

_Collect images to give insight in your vision: colors, shapes, sounds, images, words.._

<figure markdown>
  ![](../images/home/sample-image.jpg)
  <figcaption>Inspiration images left-right by xxx, xxx, xxx and xxx</figcaption>
</figure>


??? tip

	remember to resize and optimize all your images. You will run out of space and the more data, the more servers, the more cooling systems and energy wasted :) make a choice at every image :)
