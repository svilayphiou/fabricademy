# Process

## **Ideation & sketches**

_Think of uploading your hand-drawings, "frankenstein prototypes", pictures with notes, everything that shows your thinking process and initial development of your personal project. Giving inside in your process, will help the mentors to assist you further in your development._


![](../images/home/sample-image.jpg){ width=200 }
![](../images/home/sample-image.jpg){ width=200 }
![](../images/home/sample-image.jpg){ width=200 }


_research papers can also be linked through an online link or when optimised, PDFs of research papers can be added to your repository and credited to the writer or researcher_



## **Design & Fabrication**

_once you start designing and fabricating your first tests, you can link both at the bottom of the page with footnotes_

![](../images/home/sample-image.jpg){ align=right width=400 }
<br/>
"This step of the process was important because i learnt to draft my own pattern digitally. The first tests of this can be seen here on the right, find half- or test-fabrication files here[^1]"


<p style="clear: right;"></p>
<br/>



## **Prototypes**

_prototypes are your first step towards shaping your final piece, product, material et cetera_

<br/>

![](../images/home/sample-image.jpg)

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.




## **Mentoring notes**

_Mentors in all sessions may share with you their comments, notes, advise, projects and technical equipment to check out. This is good place to share those, so that you can find them later on when you need them the most!_


## Half-fabrication files

[^1]: Test file: 3d modelling test
[^2]: Test file: Laser cut test sheets
[^3]: Test file: additional test models
