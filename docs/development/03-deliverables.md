# Deliverables


## **GANTT**

_the in detail planning of the 3 month project development phase as you plan it to be.
You can use various planning tools to set your timeline and better foresee what you need to do next_


"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."

* [Monday](https://monday.com)
* [Excel]()
* [Trello](https://trello.com)




## **BoM** bill of materials

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."


### Materials

| Qty |  Description    |  Price  |           Link           | Notes      |
|-----|-----------------|---------|--------------------------|------------|
| 1   | Material one    |  22.00 $| http://amazon.com/test   | order soon |
| 2   | Material two    |  22.00 $| http://amazon.com/test   |            |
| 1   | Material three  |  22.00 $| http://amazon.com/test   | packs of 3 |
| 1   | Material five   |  22.00 $| http://amazon.com/test   |   timing   |
| 5   | Material eight  |  22.00 $| http://amazon.com/test   |            |
| 10  | Material twelve |  22.00 $| http://amazon.com/test   |            |
| 10  | Material eleven |  22.00 $| http://amazon.com/test   |            |

### Useful links

- [electronics](http://class.textile-academy.org)
- [biomaterial inventory](http://class.textile-academy.org)
- [fibers & farm](https://class.textile-academy.org)




## **Slide show**

Embed your presentation

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRSc1vc06BuUKVe_7rGqD4DUeuuLkDXcP1y0QnfE18M_lS1qjl61QJVpOfw84-DS2mc2jq1ij0kayVS/embed?start=false&loop=true&delayms=5000" frameborder="0" width="1000" height="460" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>





## **Story telling script**

My sketches for the story telling session tell a story about my concept and lead to the final outcome in..

![](./../images/home/sample-image.jpg){ width=300 align=left }
![](./../images/home/sample-image.jpg){ width=300 align=right }


<p style="clear: left;"></p>
<br/>

- _scene 1_ the scene opens with a room full of flowers
- _scene 2_ we zoom into the lady wearing a plant based dress


<p style="clear: left;"></p>
<br/>

![](./../images/home/sample-image.jpg){ width=200 align=left }
![](./../images/home/sample-image.jpg){ width=200 align=right }

<br/>
- _scene 3_ she comes out from behind the plants

<br/>
- _scene 4_ ... whats next?


<p style="clear: left;"></p>
<br/>


_A good exaple of story telling sketches are from ...Florencia Moyano https://class.textile-academy.org/2022/florencia-moyano/finalproject/prefinal03/_

see her storytelling script in a google slides presentation below.

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQywWLoCx1nUKORB-T-eh0dPICgxi6ZI7ijL30RlgbJhFxc1ACpq6EeCeuG67w2H-r5sajECGn7eaYz/embed?start=false&loop=false&delayms=3000" frameborder="0" width="1000" height="460" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>









## **FABRICATION FILES**

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

This model [^1] was obtained by.. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
The laser cut nesting [^2] was created using..


??? tip "footnote fabrication files"

	_Fabrication files_ are a necessary element for evaluation. You can add the _fabrication files_ at the bottom of the page and simply link them as a footnote. This was your work stays organised and files will be all together at the bottom of the page. Footnotes are created using [ ^ 1 ] (without spaces, and referenced as you see at the last chapter of this page) You can reference the fabrication files to multiple places on your page as you see for footnote nr. 2 also present in the Gallery.




## **Code Example**

_Use the three backticks to separate code._

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```


## **How-Tos & Tutorials**

_Upload templates or tutorials you created for your personal project, it can also be links to instructables when the project is educational, protocols when working with growing materials and so on.._

---
---

## _Fabrication files_

[^1]: File: 3d modelling of mannequin
[^2]: File: Laser cut sheets
[^3]: File: additional models

