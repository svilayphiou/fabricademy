# My documentation

This is the website gathering all my documentation for [Fabricademy](https://textile-academy.org) 2023–2024 course that I follow at the node [Green Fabric](https://greenfabric.be) in Brussels.

## Final project: Strip-p-ed out

«Strip-p-ed Out» is a project researching patterns which can bypass human recognition of Computer Vision. What does a CCTV see? Does it really see humans or…? Can we fool its eye? Look at the camera which is looking at you looking at it. I eye I eye I eye I…




- [Draft](./assignments/week13.md)
- [Planning](./project/index.md)
- [Quick & Dirty prototypes](./project/quick-prototypes.md)
- [More prototypes](./project/prototypes.md)
- [Kniterate experiments](./project/kniterate.md) (semi-industrial knitting machine)
- [Final proposals](./project/final.md)

* * *

- [Exhibition](./project/exhibition.md)
- [Storytelling](./project/storytelling.md)
- [Readings/References](./project/readings.md)

[A booklet of this research is available here](./project/booklet.md) (print it from the browser when you land on that page).




## Assignments


[![](./images/week01/pattern.gif "Project management")](./assignments/week01.md)

[![](./images/week02/final/anim3-small.gif "Digital bodies")](./assignments/week02.md)

[![](./images/week03/pixel-small.gif  "Circular fashion")](./assignments/week03.md)

[![](./images/week04/indigo/indigo.gif  "Biochromes"){: style="height: 175px;"}](./assignments/week04.md)

[![](./images/week05/pushbutton-knit-small.gif  "E-textiles"){: style="height: 175px;"}](./assignments/week05.md)

[![](./images/week06/fishscales.gif  "Biofabricating")](./assignments/week06.md)

[![](./images/week07/inkscape.gif  "Computational couture")](./assignments/week07.md)

[![](./images/week08/glove.gif  "Wearable")](./assignments/week08.md)

[![](./images/week09/blue.gif  "SoftRobotics"){: style="height: 175px;"}](./assignments/week09.md)

[![](./images/week10/csm.gif  "Open-source hardware"){: style="height: 175px;"}](./assignments/week10.md)

[![](./images/week11/tapioca/balloon.gif  "Textile scaffold"){: style="height: 175px;"}](./assignments/week11.md)

[![](./images/week12/led.gif  "Skin electronics"){: style="height: 175px;"}](./assignments/week12.md)|


* * *


## About me

![portrait](./images/home/VilayphiouS-small.jpg "Photo by Ludi Loiseau"){ width=400}


Trained as a graphic designer and media designer, I worked for ten years with [Open Source Publishing](http://osp.kitchen) making websites and visual identities using only Free and Open Source Software. I also teach Digital Design at [École de Recherche Graphique](http://erg.be) where I guide students into experimenting free software for graphics, hybrid publishing (web2print).
Since 2020, I joined [Green Fabric](https://greenfabric.be), a textile fablab in Brussels. There I like to jump from machine to machine, but I focus mainly on machine knitting.

- [Instagram](https://instagram.com/vvvvvilay/)
- [Gitlab](https://gitlab.com/svilayphiou/)
- [Green Fabric](https://greenfabric.be/)

## Previous work


### My Mother Was a Computer

A series of knitted tapestries of women who played a major role in the history of computing.



![My Mother Was a Computer](./images/home/mother.jpg "My Mother Was a Computer")

![Ada Lovelace](./images/home/ada.jpg "Ada Lovelace, knitted tapestry")

### DIY Jacquard loom

![Grace Hopper](./images/home/grace-loom.jpg "Grace Hopper, woven on knitting machine")

![Ada Dietz](./images/home/ada-dietz-loom.jpg "Ada Dietz, woven on knitting machine")

### Generated patterns

![Ada Dietz](./images/home/ada-dietz2.jpg "Pattern generated with Ada Dietz algorithm")

![AdaCAD](./images/home/adacad.jpg "Pattern generated with the software AdaCAD")

![Itertools](./images/home/itertools.jpg "Pattern generated with the Python library Itertools")

<p class="clear"></p>




<style>
    figure {
        margin: 2em;
        min-width: 200px;
        min-height: 200px;
    }
</style>
