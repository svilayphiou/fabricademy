# Prototypes

![](graph.png)

## More tests on «in the wild» images

I ran more tests on different categories of images to understand more how the machine recognizes humans. 

### **Faces**

![](prototypes/face-masks.png "Since Covid, we wear a lot of face masks. It looks like black face masks succeed in bypassing the algorithm, probably because it removes the volume/shadows of the face.")

![](prototypes/haarface.jpg "Viola-Jones Haarface algorithms looks for contrasts in specific zones of the face. (Don't use Lena's picture for testing purposes! <https://losinglena.com>). *(image by [GREG BORENSTEIN](https://www.flickr.com/photos/unavoidablegrain/), shared under a [CC BY-NC-SA 2.0 LICENSE](https://creativecommons.org/licenses/by-nc-sa/2.0/))*")

### **Environment**

![](prototypes/camo.png "A camouflage pattern works only in a specific environment.")



![](prototypes/night.png "The algorithm works quite good with night pictures. Frightening…")

### **Human or biped recognition?**



![](prototypes/4pattes.png "A human on all fours is not often recognized by the algorithm, therefore it recognizes bipeds rather than «humans».")

![](prototypes/sitting.png "A human sitting is not often recognized by the algorithm, therefore it recognizes bipeds rather than «humans».")

![](prototypes/mascots.png "Humans disguised in animals are also recognized. Is it because they're standing?")

![](prototypes/bears.png "Standing animals can also be recognized as «humans».")

### **Changing the silhouette drastically might work but not all the time.**

![](prototypes/balenciaga.png  "Test on Balenciaga known for crazy structures/silhouette.")

![](prototypes/iris-van-herpen.png   "Test on Iris Van Herpen known for crazy and asymmetrical structures.")

![](prototypes/weird-silhouettes.png  "Test on the keyword «catwalk weird silhouette».")

### **Patterns**



![](prototypes/test-gimping.gif   "Test drawing or deforming the body.")

![](prototypes/clo3D.gif  "It looks like geometrical patterns bypass the algorithm.")

![](prototypes/geometrical-patterns.png  "Test on different geometrical patterns.")

![](prototypes/catwalk-stripes1.png  "Horizontal stripes covering the shoulders seem to work better.")

![](prototypes/catwalk-stripes2.png  "Horizontal stripes covering the shoulders seem to work better.")

![](prototypes/catwalk-stripes3.png  "Horizontal stripes covering the shoulders seem to work better.")

## Feedback from midterm presentation with Ricardo O'Nascimento

> Love how you showed us your research today - makes it very clear, and very relatable, yet to the point.

> This would be a great part in your video - its super communicative.
> What do you expect to be the final outcome? if it is a full garment - what are your first suspicious of which elements will work best? do you have already preliminary outcomes - hints of what you will use to make the design choices? 
> I am a big fan of your concept :) if you are passing by amsterdam any time soon - i have a number of colleagues that i think you will have great conversations with about both privacy, survaillance, encrypting messages into clothing, etc etc :D"	

> The project concept is great, yet, the presentation should be more organized and formal.	

> The project is very challenging, yet most of the work it is done. 
> Congratulations again on the studies with camera and facial recognition. Deciding what to knit and why is your next focus, I like the suggestions of photochromic inks and the idea of embedding data visualization. Stripes generated from surveillance data is a valid point. I like the kimono ideas! 	
> I came across this recently...I think you might find it interesting. Dazzle Camoflage <https://www.slashgear.com/947328/what-is-dazzle-camouflage/>. Great idea to mark up images to see its impact on Image Recognition algorithm.																			



## Small-scale prototypes

I started prototyping in small scale with my daughter’s Barbie dolls (I’ve never bought any and they don’t like Barbies anyways, so it’s perfect, haha…). I made this small very simple rectangular structure to make a kimono-like vest which is easy to construct and to put on. 

![](prototypes/manuchao.png  "Test with wasted fabrics.")

![](prototypes/manuchao-hair.png  "Test with wasted fabrics. The hair is hiding the pattern and the doll is recognized.")

![](prototypes/dotted.png  "Test with incomplete lines.")

![](prototypes/dotted-complete.png  "Test with incomplete lines completed with a marker.")



video

![](prototypes/without-hatches-cam.jpg  "Inkscape hatches to quickly prototypes «handmade» stripes.")

![](prototypes/kimono-pattern.png  "Kimono-like pattern on Inkscape. Goes beyond an A4, but it's ok for prototyping purposes.")

![](prototypes/textile-printer.jpg  "Test for countershadowing. Textile inkjet printing.")







## Next steps




- Use datasets around camera surveillance to generate stripes which have meaning. Colors, thicknesses can be used to translate data. Where to find free datasets:<https://careerfoundry.com/en/blog/data-analytics/where-to-find-free-datasets/>
- Collective CCTV maps: <https://sunders.uber.space/> or <https://osmcamera.dihe.de/>
- Top CCTVs: 

    - by country:  <https://upcomingsecurity.co.uk/security-guides/cctv-camera-guides/cctv-by-country/>
    - per person/surface: <https://www.comparitech.com/vpn-privacy/the-worlds-most-surveilled-cities/>

- Cryptography researcher Seda Gürses claims that the more one is hiding from digital surveillance (talking about web surveillance in this case), the more that person is actually identified. Adam Harvey also claims that the more we fight surveillance systems, the stronger it gets. So instead of hiding, an attack could actually to pollute captured data with false data, either wrong or too many. How to achieve that within a garment?
- pick up second-hand/torn clothes from thrift shops for people in the exhibition to draw onto an anti-recognition pattern and take away their cloth → use big brushes with natural inks



