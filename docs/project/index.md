# Strip-p-ed Out

## Proposal

«Strip-p-ed Out» is a project researching patterns which can bypass human recognition of Computer Vision. What does a CCTV see? Does it really see humans or…? Can we fool its eye? Look at the camera which is looking at you looking at it. I eye I eye I eye I…

To read the first proposals, please refer to [Week 13](../assignments/week13.md).

<!–

??? "Notes from zoom session 09/01/2024:"

    - during covid, masks to avoid facial recognition
    - node red software (node programming): <https://nodered.org/>
    - smile recognition software by Claudia Simonelli: <https://fibasile.github.io/whytomake2017>

–>


## Planning

To plan my final project, I used the online tool [Plant UML](https://plantuml.com/fr/gantt-diagram) to generate a Gantt diagram from text. I find it easier/faster to edit than a graphical interface.

![](../images/final-project/gantt-planning.png "Final project planning")

Here's the source code generating the gantt chart:

```
@startgantt

Project starts 2024/01/09
saturday are closed
sunday are closed


<style>
ganttDiagram {
	task {
		FontName Sazanami Mincho
		FontColor blue
                FontStyle Bold
		FontSize 16
		BackGroundColor mediumspringgreen
		LineColor blue
	}
	milestone {
                FontName Sazanami Mincho
		FontColor blue
                FontStyle Bold
		FontSize 18
		FontStyle italic
		BackGroundColor deeppink
		LineColor red
	}
	note {
		FontColor Olive
		FontSize 10
                FontStyle Bold
		LineColor OrangeRed
	}
	arrow {
		FontName Helvetica
		FontColor red
		FontSize 18
		LineColor DeepPink
                LineThickness 1
	}
	separator {
                FontName Sazanami Mincho
		LineColor Violet
                FontStyle Bold
		FontSize 16
		FontColor DarkViolet
	}
	timeline {
            FontName Sazanami Mincho
            FontSize 16
	    BackgroundColor Bisque
	}
	closed {
		BackgroundColor GhostWhite
		FontColor gray
	}
}
</style>


[Final project] requires 77 days

-- F a b r i c a d e m y    Z o o m    S e s s i o n s --

2024/01/09 is colored in MistyRose
2024/01/16 is colored in MistyRose
2024/01/30 is colored in MistyRose
2024/02/12 is colored in MistyRose
2024/02/13 is colored in MistyRose
2024/02/14 is colored in MistyRose
2024/02/27 is colored in MistyRose
2024/03/12 is colored in MistyRose
2024/03/25 is colored in MistyRose
2024/03/26 is colored in MistyRose
2024/03/27 is colored in MistyRose

[Quick and dirty prototypes] happens at 2024/01/16
[Focus group - mentoring sessions] happens at 2024/01/30
[Mid-term presentations] happens at 2024/02/12
[Mentoring sessions] happens at 2024/02/27
[Review on storytelling] happens at 2024/03/12
[Final presentations] happens at 2024/03/25


-- P h a s e   1 --

[Prototyping: knit the collar] starts at 2024/01/10 and requires 1 day
[Order materials] starts at 2024/01/10
[Receive materials] starts at [Order materials]'s end
then [Test CCTV sensor input] requires 2 days
then [Test CCTV sensor output] requires 2 days
[Read: articles/books on digital surveillance] starts at 2024/01/10 and ends at [Mid-term presentations]'s end


-- P h a s e   2 --

[Prototyping: image recognition] starts at 2024/01/16 and requires 14 days
[Prototyping: CCTV sensor wearable] starts at 2024/01/16 and requires 14 days


-- P h a s e   3 --

[Prototyping: making choices and reiterate] starts at 2024/01/30 and requires 14 days
[Network: contact other artists close to the subject] requires 1 day and starts at 2024/01/30


-- P h a s e   4 --

[Final fabrication: Making it real] starts at [Mid-term presentations]'s end and requires 14 days
[Debugging: Let others test the devices] starts at [Mentoring sessions]'s end and requires 14 days
[Mediation: think of how to exhibit the piece, if any document is needed alongside] starts at [Mentoring sessions]'s end and requires 14 days


-- P h a s e   5 --

[Documentation: fine-tuning] requires 2 days

[Storytelling] requires 5 days and ends at [Review on storytelling]'s end

[Documentation: fine-tuning]  ends at [Storytelling]'s start



-- P h a s e  6   --

then [Storytelling: fine tuning] requires 5 days

[Global fine tuning] requires 7 days and ends at [Final presentations]'s end


@endgantt
```

## Quick & dirty prototypes

[Go to prototypes.](prototypes.md)
