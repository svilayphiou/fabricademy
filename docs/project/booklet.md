<div>
<link rel="stylesheet" href="../../css/interface.css" media="screen">
<link rel="stylesheet" href="../../css/booklet.css" media="screen">
</div>



<div id="cover" markdown>
# Strip<span style="text-decoration: underline; text-decoration-skip-ink: none; text-underline-offset: -13px; text-decoration-thickness: 4px; text-decoration-style: double; text-decoration-color: var(--color-3); ; border: 0; padding: 0;">p</span>ed out
Stéphanie Vilayphiou  
Fabricademy 2024
</div>



## Strip-p-ed Out

«Strip-p-ed Out» is a project researching patterns which can bypass human recognition of Computer Vision. What does a CCTV see? Does it really see humans or…? Can we fool its eye? Look at the camera which is looking at you looking at it. I eye I eye I eye I…

## First ideas

{% include-markdown "../assignments/week13.md" start="<!--start-->"      end="<!--end-->"  %}

{% include-markdown "./quick-prototypes.md"   %}

{% include-markdown "./kniterate.md" %}

{% include-markdown "./prototypes.md" %}


{% include-markdown "./final.md" %} 

{% include-markdown "./readings.md" %} 


<div id="colophon" markdown>


## Colophon
CONCEPT & RESEARCH
:   Stéphanie Vilayphiou

PROJECT MENTORING
:   Claudia Simonelli
:   Valentine Fruchart

ASSOCIATED FABLAB
:   Green Fabric, Brussels

VIDEO PRODUCTION
:   Stéphanie Vilayphiou

MODEL
:   Élodie Goldberg

THANKS
:   Valentine Fruchart
:   Claudia Simonelli
:   Troy Nachtigall
:   Ricardo O'Nascimento
:   Fabricademy team
:   Élodie Goldberg
:   Alexandre Leray

TOOLS
:   OpenCV
:   Kniterate
:   Inkscape 
:   Gimp
:   Krita

FONTS {: style="padding-top: var(--line-height);"}
:   Terminal Grotesque by Raphaël Bastide

BOOKLET
:   Web documentation written with mkdocs.
:   Layout in web2print with paged.js.

LICENSED UNDER CC4R
:   <https://constantvzw.org/wefts/cc4r.en.html>

PROJECT FILES
:   <https://class.textile-academy.org/2024/stephanie-vilayphiou/>
:   <https://gitlab.com/svilayphiou/fabricademy>

</div>

# <br>

# <br>

<script src="../../js/paged.polyfill.min.js">
</script>



<script>
// DEPLOY ALL DETAILS/SUMMARY TAGS
const D = document.querySelectorAll("details");

D.forEach((d) => {
  d.setAttribute("open", "True");
});

</script>

