# Storytelling

<!--start-->

Voiceover: text-to-speech

<div class="slide">Title</div>

<div class="slide"> wall of emoji eyes</div>

<div class="slide">What</div>

<div class="slide">Video capture of<br>tracked humans</div>

<div class="slide">Why</div>

<div class="slide">Data numbers</div>

<div class="slide">How</div>

<div class="slide">Existing garments<br>on CCTV bypass</div>

<div class="slide">Adam Harvey's<br>projects</div>

<div class="slide">Capable.design</div>

<div class="slide">How to be invisible<br>to both humans and machines</div>

<div class="slide">slideshow openCV<br>"in the wild" images</div>

<div class="slide">openCV video on dolls</div>

<div class="slide">Kniterate</div>

<div class="slide">Élodie Goldberg<br>wearing kimonos<br>in front of a CCTV…</div>

<div class="slide">…+ computer with live openCV</div>

<div class="slide">People painting on shirts<br>their own camouflage garment</div>

<div class="slide">People trying on<br>their own camouflage shirt</div>

<div class="slide">Credits</div>

<style>
.slide {
    border: 3px solid black;
    border-radius: 10px;
    padding: 1.5em;
    margin: 0.5em;
    width: 9em;
    height: 112px;
    background-color: white;
    display: flex;
    text-align: center;
    align-items: center;
    justify-content: center;
    float: left;
    font-size: 0.8em;
    line-height: 1.2em;
}
</style>

<!--end-->

<!--Video from Rico: A Computer Scans A Crowded Subway Tunnel Using Facial Recognition To Show Each Persons Personal Data In A Floating Display That Follows The Individual 00:13-->





<!--nebulullaby -->
<!--tracks 09-06-04-->
