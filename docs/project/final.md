# Final proposals

## Dolls

For the exhibition in May, I would like to exhibit the small scale prototypes I've made. Besides, I will invite the audience to make their own prototype and test it with a live OpenCV. They can later bring their prototype home and upscale their pattern on a real garment.
To ease the prototyping, I made a lasercut template to assemble the garment with tabs and notches rather than sewing.

![](./kimonos/kimono.svg "Lasercut template.")

Hand-drawn tests with Posca and textile markers:

![](./finals/lines.png "Hand-drawn lines")

![](./finals/curves.png "Hand-drawn curves")

![](./finals/dazzle.png "Hand-drawn dazzle pattern")

![](./finals/fat-lines.png "Hand-drawn fat lines")

Tests with OpenCV of different patterns:

![](./finals/barbies/20240325_150851.jpg)

![](./finals/barbies/20240325_152317.jpg)

![](./finals/barbies/20240325_152437.jpg)

![](./finals/barbies/20240325_152455.jpg)

![](./finals/barbies/20240325_152539.jpg)

![](./finals/barbies/20240325_152839.jpg)

![](./finals/barbies/20240325_152850.jpg)

![](./finals/barbies/20240325_152916.jpg)

![](./finals/barbies/20240325_152940.jpg)

![](./finals/barbies/20240325_153055.jpg)

![](./finals/barbies/20240325_153206.jpg)

![](./finals/barbies/20240325_153247.jpg)



## Vest 1: Readymade
Some patterns might already be available for a good camouflage. You must pay attention to the orientation though, it looks like horizontal patterns work best. Here are some examples with fabric found at Green Fabric:

![](./finals/mercerie-moderne/20240306_131124.jpg "Works so-so. Probably the lines are not big enough.")

![](./finals/mercerie-moderne/20240306_131136.jpg "Works fine.")

![](./finals/mercerie-moderne/20240306_131145.jpg "Works fine.")

![](./finals/mercerie-moderne/20240306_131511.jpg "Works so-so. Probably because it's squared.")

![](./finals/mercerie-moderne/20240306_131837.jpg "Works so-so. Probably because they are closed shapes and not lines")

![](./finals/mercerie-moderne/20240306_131847.jpg "Does not work. Shows that the orientation has an impact.")

I used one the pink and yellow striped fabric to make a very fast test of a scale 1 vest. It is an assembly of rectangles, it could use better shaping, but it's good for a prototype.



![](./finals/readymade.jpg )

![](./finals/Stephanie_Vilayphiou-Strip-p-ed_out01.jpg )

![](./finals/Stephanie_Vilayphiou-Strip-p-ed_out02.jpg )

## Vest 2: Scribbling

I used the "Hatches" effect for objects in Inkscape to make scribbling on the panels. It was quite useful to manage the thicknesses and orientaion of the lines. 

![](./finals/hatches.jpg "Prototype made with a textile printer.")

It's the first time that I made a big and finished object with the Kniterate! It was quite exciting. I had a lot of holes that I had to repair because the yarns were too fine/fragile.

??? info "Source files"
    - [Front 1](./input-kniterate/front1-2.png)
    - [Front 2](./input-kniterate/front2-2.png)
    - [Back](./input-kniterate/body2.png)
    - [Sleeve 1](./input-kniterate/sleeve1-2.png)
    - [Sleeve 2](./input-kniterate/sleeve2-2.png)
    - [Kniterate Back](./input-kniterate/fabricademy-back.json)
    - [Kniterate Front](./input-kniterate/fabricademy-front.json)
    - [Kniterate Sleeve](./input-kniterate/fabricademy-sleeve.json)




![](./finals/kniterate/20240320_095203.jpg "2 colors, inverted. Before ironing.")

![](./finals/kniterate/20240320_095758.jpg "2 colors, inverted. After ironing.")

![](./finals/scribbles.jpg )



![](./finals/Stephanie_Vilayphiou-Strip-p-ed_out03.jpg )

![](./finals/Stephanie_Vilayphiou-Strip-p-ed_out04.jpg )

## Vest 3: Dataviz



The thicknesses of the stripes of this vest are translations of the amound of CCTVs in 5 European countries.  
Data is from 2023, retrieved on this website: <https://www.comparitech.com/vpn-privacy/the-worlds-most-surveilled-cities/>. It is a top 150 of the most surveilled cities in the world, there are only 5 European ones in this list! Those are the ones I kept for the dataviz.


![](./storytelling/video_preview/strip-p-ed_out89.jpg "Barcelona")

![](./storytelling/video_preview/strip-p-ed_out90.jpg "Rome")

![](./storytelling/video_preview/strip-p-ed_out91.jpg "Madrid")

![](./storytelling/video_preview/strip-p-ed_out92.jpg "Paris")

![](./storytelling/video_preview/strip-p-ed_out93.jpg "London")


The captions of the dataviz have been lasercut:

![](./storytelling/finals/dataviz/20240320_153732.jpg" "")



??? info "Source files"
    - [Front 1: people/CCTV](./input-kniterate/front1-2.png)
    - [Front 2: surface/CCTV](./input-kniterate/surface.png)
    - [Back: crime index/CCTV](./input-kniterate/crime-index.png)
    - [Sleeve](./input-kniterate/dataviz-sleeve.png)
    - [Captions](./input-kniterate/belt-dataviz.svg)
    - [Kniterate Back](./input-kniterate/fabricademy-back.json)
    - [Kniterate Front](./input-kniterate/fabricademy-front.json)
    - [Kniterate Sleeve](./input-kniterate/fabricademy-sleeve.json)




Final vest:

![](./finals/dataviz.jpg )

![](./finals/Stephanie_Vilayphiou-Strip-p-ed_out05.jpg )

![](./finals/Stephanie_Vilayphiou-Strip-p-ed_out06.jpg )

## Video
Video available at <https://videos.domainepublic.net/w/56Epknx9rmjKCh6d9QCX1f>

<iframe title="Strip-p-ed out" width="560" height="315" src="https://videos.domainepublic.net/videos/embed/212ef093-bf93-4d65-a624-e4c4ff3e33ea" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

### Draft storyboard
{% include-markdown "./storytelling.md" start="<!--start-->"      end="<!--end-->" %} 


### Screenshots
![](./storytelling/video_preview/strip-p-ed_out11.jpg)

![](./storytelling/video_preview/strip-p-ed_out12.jpg)

![](./storytelling/video_preview/strip-p-ed_out13.jpg)

![](./storytelling/video_preview/strip-p-ed_out14.jpg)

![](./storytelling/video_preview/strip-p-ed_out15.jpg)

![](./storytelling/video_preview/strip-p-ed_out39.jpg)

![](./storytelling/video_preview/strip-p-ed_out40.jpg)

![](./storytelling/video_preview/strip-p-ed_out41.jpg)

![](./storytelling/video_preview/strip-p-ed_out42.jpg)

![](./storytelling/video_preview/strip-p-ed_out43.jpg)

![](./storytelling/video_preview/strip-p-ed_out44.jpg)

![](./storytelling/video_preview/strip-p-ed_out45.jpg)

![](./storytelling/video_preview/strip-p-ed_out46.jpg)

![](./storytelling/video_preview/strip-p-ed_out47.jpg)

![](./storytelling/video_preview/strip-p-ed_out48.jpg)

![](./storytelling/video_preview/strip-p-ed_out49.jpg)

![](./storytelling/video_preview/strip-p-ed_out50.jpg)

![](./storytelling/video_preview/strip-p-ed_out51.jpg)

![](./storytelling/video_preview/strip-p-ed_out52.jpg)

![](./storytelling/video_preview/strip-p-ed_out53.jpg)

![](./storytelling/video_preview/strip-p-ed_out54.jpg)

![](./storytelling/video_preview/strip-p-ed_out55.jpg)

![](./storytelling/video_preview/strip-p-ed_out56.jpg)

![](./storytelling/video_preview/strip-p-ed_out57.jpg)

![](./storytelling/video_preview/strip-p-ed_out58.jpg)

![](./storytelling/video_preview/strip-p-ed_out59.jpg)

![](./storytelling/video_preview/strip-p-ed_out60.jpg)

![](./storytelling/video_preview/strip-p-ed_out61.jpg)

![](./storytelling/video_preview/strip-p-ed_out62.jpg)

![](./storytelling/video_preview/strip-p-ed_out63.jpg)

![](./storytelling/video_preview/strip-p-ed_out64.jpg)

![](./storytelling/video_preview/strip-p-ed_out65.jpg)

![](./storytelling/video_preview/strip-p-ed_out66.jpg)

![](./storytelling/video_preview/strip-p-ed_out67.jpg)

![](./storytelling/video_preview/strip-p-ed_out68.jpg)

![](./storytelling/video_preview/strip-p-ed_out69.jpg)

![](./storytelling/video_preview/strip-p-ed_out70.jpg)

![](./storytelling/video_preview/strip-p-ed_out71.jpg)

![](./storytelling/video_preview/strip-p-ed_out72.jpg)

![](./storytelling/video_preview/strip-p-ed_out73.jpg)

![](./storytelling/video_preview/strip-p-ed_out74.jpg)

![](./storytelling/video_preview/strip-p-ed_out75.jpg)

![](./storytelling/video_preview/strip-p-ed_out76.jpg)

![](./storytelling/video_preview/strip-p-ed_out77.jpg)

![](./storytelling/video_preview/strip-p-ed_out78.jpg)

![](./storytelling/video_preview/strip-p-ed_out79.jpg)

![](./storytelling/video_preview/strip-p-ed_out80.jpg)

![](./storytelling/video_preview/strip-p-ed_out81.jpg)

![](./storytelling/video_preview/strip-p-ed_out82.jpg)

![](./storytelling/video_preview/strip-p-ed_out83.jpg)

![](./storytelling/video_preview/strip-p-ed_out84.jpg)

![](./storytelling/video_preview/strip-p-ed_out85.jpg)

![](./storytelling/video_preview/strip-p-ed_out86.jpg)

![](./storytelling/video_preview/strip-p-ed_out87.jpg)

![](./storytelling/video_preview/strip-p-ed_out89.jpg)

![](./storytelling/video_preview/strip-p-ed_out90.jpg)

![](./storytelling/video_preview/strip-p-ed_out91.jpg)

![](./storytelling/video_preview/strip-p-ed_out92.jpg)

![](./storytelling/video_preview/strip-p-ed_out93.jpg)

![](./storytelling/video_preview/strip-p-ed_out94.jpg)

![](./storytelling/video_preview/strip-p-ed_out95.jpg)

![](./storytelling/video_preview/strip-p-ed_out96.jpg)

![](./storytelling/video_preview/strip-p-ed_out97.jpg)

![](./storytelling/video_preview/strip-p-ed_out98.jpg)

![](./storytelling/video_preview/strip-p-ed_out99.jpg)

![](./storytelling/video_preview/strip-p-ed_out100.jpg)

![](./storytelling/video_preview/strip-p-ed_out101.jpg)

![](./storytelling/video_preview/strip-p-ed_out102.jpg)

![](./storytelling/video_preview/strip-p-ed_out103.jpg)

![](./storytelling/video_preview/strip-p-ed_out104.jpg)

![](./storytelling/video_preview/strip-p-ed_out105.jpg)

![](./storytelling/video_preview/strip-p-ed_out106.jpg)

![](./storytelling/video_preview/strip-p-ed_out118.jpg)

