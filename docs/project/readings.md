# Reading notes






## Contributors of Wikipedia, [«Camouflage»](https://en.wikipedia.org/wiki/Camouflage), retrieved on March 2024

> The majority of camouflage methods aim for crypsis, often through a general resemblance to the background, high contrast disruptive coloration, eliminating shadow, and countershading.



## Steven Porter, [«Can this clothing defeat face recognition software? Tech-savvy artists experiment»](https://www.csmonitor.com/Technology/2017/0108/Can-this-clothing-defeat-face-recognition-software-Tech-savvy-artists-experiment), *The Christian Science Monitor*, 2017

> «That’s a problem,» Dr. Sellinger and Dr. Hartzog wrote. «The government should not use people’s faces as a way of tagging them with life-altering labels. The technology isn’t even accurate. Faception’s own estimate for certain traits is a 20 error rate. Even if those optimistic numbers hold, that means that for every 100 people, the best-case scenario is that 20 get wrongly branded as a terrorist.»

* * *

> […] «I think camouflage is often misunderstood as a Harry Potter invisibly cloak, when camouflage actually is about optimizing the way you appear and reducing visibility.»





## Kate Mothes, [«Trick Facial Recognition Software into Thinking You’re a Zebra or Giraffe with These Pyschedelic Garments»](https://www.thisiscolossal.com/2023/02/capable-facial-recognition-textiles/), *Colossal*, 2023

> «Choosing what to wear is the first act of communication we perform every day. (It’s) a choice that can be the vehicles of our values, » says co-founder and CEO Rachel Didero. Likening the commodification of data to that of oil and its ability to be sold and traded by corporations for enormous sums—often without our knowledge—Didero describes mission of Cap_able as «opening the discussion on the importance of protecting against the misuse of biometric recognition cameras.» When a person dons a sweater, dress, or trousers woven with an adversial image, their face is no longer detectable, and it tricks the software into categorizing them as an animal rather than a human. 

## Adam Harvey, [«On computer vision»](https://adam.harvey.studio/on-computer-vision/), * UMBAU: Political Bodies*, 2021

> Photography has become a nineteenth-century way of looking at a twenty-first century world. In its place emerged a new optical regime: computer vision.
* * *
> Computer vision, unlike photography, does not mirror reality but instead interprets and misinterprets it, overlaying statistical assumptions of meaning. **There is no truth in the output of computer vision algorithms, only statistical probabilities clipped into Boolean states masquerading as truthy outcomes with meaning added in post-production.**
* * *
> Face detection algorithms, for example, do not actually detect faces, though they claim to. Face detection merely detects face-like regions, assigning each with a confident score.
* * *
> Algorithms are rule sets, and these rules are limited by the perceptual capacities of sensing technologies. This creates «perceptual topologies» that reflect how technology can or cannot see the world. In the first widely used face detection algorithm, developed in 2001 by Viola and Jones, the definition of a face relied on the available imagery of the time for training data. This comprised blurry, low resolution, grayscale CCTV imagery. The Viola-Jones face detection algorithm mirrored back the perceptual biases of low-resolution CCTV systems from the early 2000’s by encoding a blurry, noisy, grayscale definition of the human face. Understanding this perceptual topology can also help discover perceptual vulnerabilities. In my research for CV Dazzle (2010) and HyperFace (2016) I showed that the Viola-Jones Haar Cascade algorithm is vulnerable to presentation attacks using low-cost makeup and hair hacks that obscure the expected low resolution face features, primarily the nose-bridge area. By simply inverting the blocky features of their Haar Cascade algorithm with long hair or bold makeup patterns, faces could effectively disappear from security systems. Another vulnerability of the Haar Cascade algorithm is its reliance on open-source face detection profiles, which can be reverse-engineered to produce the most face-like face. In 2016, I exploited this vulnerability for the HyperFace project to fool (now outdated) face detection systems into thinking dozens of human faces existed in a pink, pixellated graphic on a fashion accessory.
* * *
> In Paglen’s ImageNet Roulette he excavates the flawed taxonomies that persisted in the WordNet labeling system that was used to label ImageNet, then purposefully trained a flawed image classification algorithm to demonstrate the dangers of racist and misogynistic classification structures.
* * *
> Becoming training data is political, especially when that data is biometric. But resistance to militarized face recognition and citywide mass surveillance can only happen at a collective level. At a personal level, the dynamics and attacks that were once possible to defeat the Viola-Jones Haar Cascade algorithm are no longer relevant. Neural networks are anti-fragile. Attacking makes them stronger. So-called adversarial attacks are rarely adversarial in nature. Most often they are used to fortify a neural network. In the new optical regime of computer vision every image is a weight, every face is a bias, and every body is a commodity in a global information supply chain.



## Adam Harvey, [«Origins and endpoints of image training datasets created  “in the wild”»](https://adam.harvey.studio/origins-and-endpoints/), 2020

> **The new logic is not better algorithms; it is better data, and more data.**
* * *
> In 2016, a researcher at Duke University in North Carolina created a dataset of student images called Duke MTMC, or multi-targeted multi-camera. The Duke MTMC dataset contains over 14 hours of synchronized surveillance video from 8 cameras at 1080p and 60FPS, with over 2 million frames of 2,000 students walking to and from classes. The 8 surveillance cameras deployed on campus were specifically setup to capture students «during periods between lectures, when pedestrian traffic is heavy». The dataset became widely popular and over 100 publicly available research papers were discovered that used the dataset. These papers were analyzed according to methodology described earlier to understand endpoints: who is using the dataset, and how it is being used. The results show that the Duke MTMC dataset spread far beyond its origins and intentions in academic research projects at Duke University. Since its publication in 2016, more than twice as many research citations originated in China as in the United States. Among these citations were papers linked to the Chinese military and several companies known to provide Chinese authorities with the oppressive surveillance technology used to monitor millions of Uighur Muslims.
* * *
> From one perspective, «in the wild» is an ideal characteristic for training data because it can provide a closer match to an unknown deployment environment. Theoretically, this can improve real-world performance by reducing disparity and bias. In reality, data collected from sources «in the wild» inherit new problems including the systemic inequalities within society and are never «natural» or «wild». Representing datasets as unconstrained or «wild» simplifies complexities in the real world where nothing is free from bias. Further, collecting data without consent forces people to unknowingly participate in experiments which may violate human rights.
* * *
> **It is advisable to stop using Creative Commons for all images containing people.**



## Adam Harvey, [«What is a Face?»](https://adam.harvey.studio/what-is-a-face/), 2021

> Computer vision requires strict definitions. Face detection algorithms define faces with exactness, although each algorithm may define these parameters in different ways. For example, in 2001, Paul Viola and Michael Jones introduced the first widely-used face detection algorithm that defined a frontal face within a square region using a 24 × 24 pixel grayscale definition. The next widely used face detection algorithm, based on Dalal and Trigg’s Histogram of Oriented Gradients (HoG) algorithm, was later implemented in dlib and looked for faces at 80 × 80 opixels in grayscale. Though in both cases images could be upscaled or downscaled, neither performed well at resolutions below 40 × 40 pixels. Recently, convolutional neural network research has redefined the technical meaning of face. Algorithms can now reliably detect faces smaller than 20 pixels in height, while new face recognition datasets, such as TinyFace, aim to develop low-resolution face recognition algorithm that can recognize an individual at around 20 × 16 pixels.
* * *
> As an image resolution decreases so too does the dimensionality of identity.











## Quentin Noirfalisse, [«Courtrai, reconnaissance faciale dans le viseur ? S’équiper pour surveiller. Épisode 4»](https://medor.coop/hypersurveillance-belgique-surveillance-privacy/police-justice-bng/episodes/courtrai-reconnaissance-faciale-dans-le-viseur-camera-criminalite-videosurveillance-briefcam-biometrie/), *in* «Hypersurveillance policière», *Médor web*, 23/12/2021

> Briefcam possède un outil assez pratique pour des policiers débordés : le Vidéo Synopsis. Il peut vous résumer des heures de vidéos en quelques minutes, en agglomérant des « objets » (individus, véhicules, par exemples) qui sont passés à différents moments sous l’oeil des caméras. En 2019, Vincent Van Quickenborne en faisait une présentation on ne peut plus enthousiaste aux médias. *« Le logiciel va aller rechercher tous les gens qui portent un sac à dos, des véhicules de couleur rouge ou qui contiennent un chien. La direction, la taille et la vitesse de l’objet peuvent être évalués. »*

* * *

> Là est tout l’enjeu posé par l’arrivée d’une solution comme Briefcam dans l’arsenal policier en Belgique. La reconnaissance faciale est interdite par la loi belge. Nous sommes un des deux pays européens, avec l’Espagne, à ne pas le permettre. Pourtant, Briefcam, de l’aveu même de la police courtraisienne à l’époque, et selon ses propres plaquettes publicitaires, dispose d’une telle fonction.
>
> La société RTS, qui détient une licence d’importateur pour Briefcam et l’a installé à Courtrai, a dû, à l’époque, désactiver les droits d’utilisateur pour la reconnaissance faciale. L’option est automatiquement disponible. RTS se justifie : leurs fournisseurs « supposent que tout le monde veut faire usage de la reconnaissance faciale ».

* * *

> Un seul exemple : comme le montre le projet Gendershades du Massachussets Institute of Technology, si on est une femme ou une personne à la peau foncée, on a plus de chances d’être victime d’une erreur d’identification qu’un bon vieux mâle blanc.





## Olivier Bailly, [«Mais où est Johan ? BNG, la base non-gérée (4/5)»](https://medor.coop/hypersurveillance-belgique-surveillance-privacy/police-justice-bng/episodes/bng-la-base-non-geree-45-fichage-covid-securite-manifestation-terrorisme-extremisme/), *in* «Hypersurveillance policière»,  28/04/2021

> Johan s’est vu refuser son accès à une activité professionnelle pour ça (extrait de la réponse de l’Autorité nationale de Sécurité) :
>
> ![](https://medor.coop/media/images/Capture_decran_2021-04-27_a_10.11.35.width-720.png)

* * *


> **Sophie**
>
> Elle est militante écolo, de gauche, tantôt radicale, tantôt consensuelle. C’est une chercheuse aussi. En 2019, elle est engagée à l’Agence fédérale de contrôle nucléaire (AFCN).
>
> Elle a à peine signé son contrat que la voilà licenciée. Elle n’a pas obtenu son habilitation de sécurité. *« J’aurais été virée pour mon passé militant, j’aurais trouvé cela dégueulasse mais bon, j’aurais compris. Mais ce n’est pas cela que j’ai trouvé dans mon dossier »*.
>
> Car Sophie a été en recours. Et quand elle a pu compulser les pages la concernant, elle a d’abord trouvé ce à quoi elle s’attendait : la participation à des manifestations et événements, certains où elle était présente, d’autres pas. On y signale trois arrestations administratives lors de manifestations. Sophie ne conteste pas.
>
> Mais aucune poursuite judiciaire n’a été menée et, dans son argumentaire, son avocat précise qu’*« un grand nombre de personnes ont été arrêtées en même temps que ma cliente »*.
>
> Accessoirement, les manifestations datent de 2014. Elles auraient du être archivées en 2017.
>
> Mais surtout, Sophie a appris son appartenance à un groupe extrémiste qui véhicule son lot de haine et que Sophie n’a jamais fréquenté.


## Olivier Bailly, [«Le grand quizz de la BNG»](https://medor.coop/hypersurveillance-belgique-surveillance-privacy/police-justice-bng/episodes/le-grand-quizz-de-la-bng/), *in* «Hypersurveillance policière», 04/05/2021

> En 2017, un événement étonnant (Tomorrowland) sera passé au crible de la BNG. 50 000 personnes. C’était du côté de la Côte.
> 
> Le but de ce screening massif était de contrôler si les festivaliers étaient connus de la police pour certains faits commis dans une période déterminée. C’est au final 29 186 identités de visiteurs (et 21 433 identités de collaborateurs) qui ont été analysées ! 10 % étaient connues de la BNG, soit 2 077 visiteurs et 1 912 collaborateurs.
>
> Si on comptait 31 000 enquêtes encodées fin 2006, il y en avait 270 000 en 2019, soit neuf fois plus d’enquêtes encodées. De quoi inventer le néologisme « factobésité » ! Et encore, c’était en 2019. Depuis lors, le coronavirus et sa cohorte de PV sanitaires sont venus s’ajouter au mégalodon.

* * *

> Nous avons donc appris que la BNG a fonctionné sans loi claire et précise pendant 12 ans, que plus de trois millions de suspects y étaient recensés en 2019 et que parmi eux, des mineurs de 14 ans pouvaient se retrouver encodés sans autorisation de magistrat de la Jeunesse. Prêts pour la suite ?
> 
> Lors des auditions pour confectionner la loi de 2014, quand la BNG ne pesait alors « que » 1,7 millions de personnes enregistrées, les mineurs de 14 à 18 ans représentaient environ 15 % du nombre de personnes identifiées. Les moins de quatorze ans, 1,6 %.

* * *

> « 52 % des dossiers traités en 2019 se sont soldés par un effacement complet ou partiel des enregistrements effectués par la police dans la BNG ». 
> 
> 52 % ! Conclure que la moitié des infos dans la BNG est fausse, ce serait un raccourci excessif. Les personnes qui ont demandé une vérification suspectaient peut-être que leur dossier contenait des erreurs. Cependant, la conclusion du COC ne laissait pas beaucoup de doutes : « (…) la BNG contient encore de nombreuses inexactitudes et/ou erreurs. »

* * *

> Sans archivage, on commence à être vraiment serrés dans la BNG. Surtout avec la croissance exponentielle de PV liés au coronavirus. C’est d’autant plus énorme que début mars 2021, le gouvernement a envisagé une mesure toute particulière pour les personnes de retour de voyage à l’étranger.
>
> Les personnes recevraient un SMS les enjoignant de faire un test Covid ou une mise en quarantaine. Si elles ne s’exécutent pas, direction une base de données policières !

## [«Pour l’interdiction de la reconnaissance faciale à Bruxelles»](https://democratie.brussels/initiatives/i-155), petition, March 2023

> **La reconnaissance faciale menace nos libertés**
> 
> L’usage de cette technologie dans nos rues nous rendrait identifiables et surveillé·es en permanence. Cela revient à donner aux autorités le pouvoir d’identifier l’intégralité de sa population dans l’espace public, ce qui constitue une atteinte à la vie privée et au droit à l’anonymat des citoyen·nes. La surveillance musèle la liberté d’expression et limite les possibilités de se rassembler, par exemple lors de manifestations. La reconnaissance faciale impactera surtout les groupes sociaux particulièrement affectés et marginalisés : personnes migrantes, communauté LGBTQI+, minorités raciales, personnes sans-abri, etc.

* * *

> - **risques quant au stockage des données :**   
> les risques de piratages informatiques visant ces données biométriques très sensibles sont importants et l’actualité belge a, de nombreuses fois, montré que les données récoltées par les autorités publiques n’étaient pas à l’abri de ces piratages ;
> - **risques d'erreurs et de discriminations accrues :**   
> les études montrent que cette technologie reproduit les discriminations sexistes ou racistes induites par les conceptions sociales dominantes et des institutions qui les vendent et qui les utilisent ;
> - **risques de normalisation et de glissement vers la surveillance de masse :**   
> le déploiement des technologies de surveillance avance à coups de projets pilotes qui précèdent les cadres légaux, puis sont ensuite régularisés, souvent sans débat démocratique.


## [Examen de la pétition contre l’usage de la reconnaissance faciale en Région de Bruxelles-Capitale](http://weblex.brussels/data/crb/doc/2022-23/148087/images.pdf#page=), 13 juin 2023

> **Que dit la loi ?**   
> En Belgique, aucune loi ne réglemente l’usage de la technologie de reconnaissance faciale. Comme je viens de le dire, il s’agit pourtant de données biométriques, des données uniques, propres à chacun – notre visage, nos empreintes digitales, nos iris, par exemple. C’est ce qui rend ces données si intéressantes pour l’identification des personnes, mais également si dangereuses lorsqu’elles sont utilisées à mauvais escient. Le traitement des données biométriques a un impact majeur sur notre vie privée.


* * *


> **Des risques de fuites et de piratage informatiques**   
> Au Royaume-Uni, 28 millions d’enregistrements représentant un total de plus de 23 gigaoctets ont été publiés sur internet après l’exploitation d’une faille d’une solution de l’entreprise Suprema, dont les clients sont notamment la Metropolitan Police, des entreprises de défense et des banques dans 83 pays. Outre des noms d’utilisateur et des mots de passe non cryptés, des registres d’accès aux installations, des niveaux de sécurité et des habilitations, les données exposées concernaient aussi les empreintes digitales et les enregistrements de reconnaissance faciale de millions de personnes. En plus du risque de manipulation des systèmes de contrôle d’accès de sites sécurisés, les observateurs ont souligné que le problème le plus grave résidait dans l’accès à des données biométriques qui ne peuvent par nature être modifiées. 

* * *

> Pour ceux qui pensent que l’intelligence artificielle concerne un futur très très lointain, il explique qu’aujourd’hui, en Chine, il existe déjà un système de contrôle social à points, qui a des conséquences directes pour la vie de milliards de citoyens à qui il est interdit de voyager, qui voient leur image affichée en public, qui reçoivent des appels intempestifs parce qu’ils n’auraient pas honoré une dette, pas payé une amende ou pas traversé sur un passage pour piétons. Ce n’est certainement pas le type de système qu’il souhaite pour Bruxelles.

* * *

> Enfin, une étude européenne sur le sujet a déjà démontré qu’onze pays européen l’utilisent déjà de façon régulière. Les orateurs disposent-ils d’informations quant à une réglementation européenne en la matière ?

* * *

> L’oratrice se dit choquée d’apprendre que toute caméra de surveillance peut être équipée d’un logiciel de reconnaissance faciale. C’est une chose dont on n’a pas assez conscience aujourd’hui. Elle demande si les caméras privées peuvent également en être équipées.


## Other references

- [ ] [Smita Kheria, Daithi Mac Sithigh, Judith Rauhofer, Burkhard Schafer, «'CCTV Sniffing': Copyright and Data Protection Implications»](https://www.research.ed.ac.uk/en/publications/cctv-sniffing-copyright-and-data-protection-implications) 
- [ ] [!Mediengruppe Bitnik, «Surveillance Chess: Hacking into Closed-Circuit Surveillance—Municipal Surveillance as a Subject of Artistic Fieldwork»](https://ojs.library.queensu.ca/index.php/surveillance-and-society/article/view/chess/chesss)
- [x] 
- [ ] [de Vries Patricia & Schinkel William. «Algorithmic anxiety: Masks and camouflage in artistic imaginaries of facial recognition algorithms», *Big Data & Society*, *6*(1), 2019](https://journals.sagepub.com/doi/10.1177/2053951719851532)
- [x] [Contributors of Wikipedia, «Dazzle Camouflage»](https://en.wikipedia.org/wiki/Dazzle_camouflage)
- [ ] [Tangible Cloud: artistic practices, counter-narratives to the mainstream vision of digital: *cloud computing*.](https://archives.tangible-cloud.be/)
- [ ] [Nicolas Malevé, «The exhibitionary complex of machine vision»](https://www.uib.no/en/machinevision/158889/ai-art-and-machine-vision-nicolas-malev%C3%A9-audrey-samson-and-jill-walker-rettberg)
- [x] [Amrita Khalid, «‘Dazzle’ makeup won’t trick facial recognition. Here’s what experts say will, 2020»](https://www.digitaltrends.com/news/cv-dazzle-makeup-facial-recognition-protests/)
- [x] Hanna Rose Shell, *Ni vu, ni connu*, Zones Sensibles, 2014
- [x] <https://www.wired.com/story/eye-mouth-eye/>
- [x] <https://technopolice.be/>
- [x] [«Hypersurveillance policière», dossier d’articles, *Médor web*](https://medor.coop/hypersurveillance-belgique-surveillance-privacy/police-justice-bng/), 2021—22
- [ ] [*Unpleasant Design*](https://unpleasant.pravi.me/) (2013), by Selena Savić and Gordan Savičić is a website and 2 books on listing designs which are thought to be unpleasant: for example benches for avoid people staying too long or sleeping on it, anti-climb paintings…
- [ ] Evan Selinger and Woodrow Hartzog, [«What Happens When Employers Can Read Your Facial Expressions?»](https://www.nytimes.com/2019/10/17/opinion/facial-recognition-ban.html), in *New York Times*, Oct. 17, 2019
- [ ] Woodrow Hartzog and Evan Selinger, [«Why You Can No Longer Get Lost in the Crowd»](https://www.nytimes.com/2019/04/17/opinion/data-privacy.html), in *New York Times*, April 17, 2019
- [x] Josh Ye, [«China drafts rules for using facial recognition technology»]( https://www.reuters.com/technology/china-drafts-rules-using-facial-recognition-technology-2023-08-08/), Reuters, August 8 2023