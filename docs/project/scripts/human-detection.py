#! python

import cv2
import numpy as np
import sys, os
import pathlib

# initialize the HOG descriptor/person detector
hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

# loads face classifier tool
face_classifier = cv2.CascadeClassifier(
    cv2.data.haarcascades + "haarcascade_frontalface_default.xml"
)

# get input and output paths from the command line
# ./human-detection.py "../input/" "../output/"
inputPath_str = sys.argv[1]
inputPath = pathlib.Path(inputPath_str)


# Sets variables according if the input is a file or a folder
if inputPath.is_dir():
    folder = True
    images = os.listdir(r'%s' % inputPath)
else:
    folder = False

outputPath = '%s' % sys.argv[2]


# Main function
def analyseImage(image, filename):
    img = cv2.imread(image)
    img.shape

    # converts image to grayscale image for faster processing
    gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # detect people in the image
    boxes, weights = hog.detectMultiScale(gray_image, winStride=(8,8) )

    # returns the bounding boxes for the detected objects
    boxes = np.array([[x, y, x + w, y + h] for (x, y, w, h) in boxes])
    
    for (xA, yA, xB, yB) in boxes:
        # display the detected boxes in the colour picture
        cv2.rectangle(img, (xA, yA), (xB, yB), (0, 255, 255), 2)
    
    # detects faces in the image
    face = face_classifier.detectMultiScale(
        gray_image, scaleFactor=1.1, minNeighbors=5, minSize=(40, 40)
    )
    
    for (x, y, w, h) in face:
        # display the detected boxes in the colour picture
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
    
    img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    
    # Saves output images in outputPath folder
    if folder and outputPath:
        print(filename)
        cv2.imwrite(filename, img)

    # Shows output in a new window
    else:
        import matplotlib.pyplot as plt
        plt.figure(figsize=(20,10))
        plt.imshow(img_rgb)
        plt.axis('off')
        plt.show()

# If the input path is a folder
# applies script to all images
if folder:
    for img in images:
        analyseImage(inputPath_str + img, outputPath + img)

# Otherwise just treat one image and shows it 
else:
    analyseImage(inputPath_str, outputPath + inputPath_str)
