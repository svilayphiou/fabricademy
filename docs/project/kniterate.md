# Kniterate

In parallel to [computer vision tests](prototypes.md), I made some samples on the [Kniterate](https://kniterate.com), a semi-industrial knitting machine as I plan to knit my digital camouflages with this machine.

## Yarns

Kniterate is a 7 gauge machine (7 needles per inch) and welcomes yarns from Nm6 to Nm8. 

The most difficult thing to tackle with the Kniterate are yarns which will go flawlessly in the machine, either for the waste yarn (a yarn you don’t want to put too much money into) and the knit yarn. For the waste yarn, we used PLA yarns made out of corn from [Noosa Fibers](https://noosafiber.com/). After removing the waste yarn, we could potentially send them back to the company for recycling.

## Birds-eye jacquard, inverted colors (full-needle rib)

![](output-kniterate/balls-design.png "Preparation file on the Kniterate software.")

![](output-kniterate/balls-waste.jpg "First custom double-bed jacquard, with waste yarn")

![](output-kniterate/balls-recto.jpg "First custom double-bed jacquard, front")

![](output-kniterate/balls-verso.jpg "First custom double-bed jacquard, back")



## Birds-eye jacquard, 3 colours, default options

![](output-kniterate/tof-design.png "Preparation file on the Kniterate software.")

![](output-kniterate/tof-recto.jpg "These settings make «pockets» between the front and back.")

![](output-kniterate/tof-verso.jpg "These settings make «pockets» between the front and back.")

## Birds-eye jacquard, 3 colours, Full-needle rib

::: instagram C3ihuHwNPsn



## dithering tests

In the idea of making countershadows, I tried different dithering software/settings to make a radial gradient with the Kniterate. 

![](output-kniterate/gradient.png "Input image.")

![](output-kniterate/gradient-gimp.png "Gimp Floyd-Steinberg.")

![](output-kniterate/gradient_TexTuring-6291.png "Texturing app: http://ivan-murit.fr/texturing.htm")

![](output-kniterate/gradient-addither.png "Dithermark, addither filter.")

![](output-kniterate/gradient-atkinson.png "Dithermark. Atkinson filter.")

![](output-kniterate/gradient-bayer.png "Dithermark, Bayer filter.")

![](output-kniterate/gradient-bayer4x4.png "Dithermark, Bayer 4x4 filter.")

![](output-kniterate/gradient-front.jpg "Radial gradient with different dithering tools. Front.")

![](output-kniterate/gradient-back.jpg "Radial gradient with different dithering tools. Back.")

![](output-kniterate/gradient-out.jpg "Knits coming out the Kniterate, separated by waste yarn.")



